package my.threesector.android.utils;

public class ErrorConstants {

    public static final String LOGIN_ERROR = "Unable to Sign In User";
    public static final String REGISTER_SUCCESS = "Registration Successfully Done,Login to Continue";
    public static final String REGISTER_ERROR = "Unable to Register User";
    public static final String CATEGORIES_LISTING_ERROR = "Categories listing details not found";
    public static final String PROJECT_LISTING_ERROR = "Project listing details not found";
    public static final String FAVOURITES_LISTING_ERROR = "NO MORE FAVOURITES";
    public static final String MY_LISTING_ERROR = "YOUR LISTING DETAILS NOT FOUND";
    public static final String UPDATE_PASSWORD_ERROR = "UNABLE TO UPDATE PASSWORD";
    public static final String UPDATE_PASSWORD_SUCCESS = "Password updated successfully";

    public static final String CATEGORY_HOME_ERROR = "UNABLE TO GET COMPANIES LISTING";
    public static final String NEARBY_LISTING_ERROR = "NEARBY LISTING DETAILS NOT FOUND";

    public static final String ERROR_REVIEW_DETAIL = "REVIEWS DETAILS NOT FOUND";

    public static final String FAVOURITE_ADD_ERROR = "UNABLE TO ADD TO FAVOURITE ITEM";
    public static final String FAVOURITE_REMOVE_ERROR = "UNABLE TO REMOVE FROM FAVOURITE ITEM";

    public static final String ADD_REVIEW_ERROR = "UNABLE TO ADD REVIEW";
    public static final String ADD_REVIEW_SUCCESS = "Review Added Successfully";

    public static final String BECOME_AUTHOR_SUCCESS = "You have successfully became an Author";
    public static final String BECOME_AUTHOR_ERROR = "You are unable to become a Author";

    public static final String ERROR_NO_CHAT_USERS = "Chat details not found";
    public static final String ERROR_SEND_MESSAGE = "UNABLE TO SEND MESSAGE";

    public static final String BROKEN_INTERNET = "INTERNET CONNECTION BROKEN";
    public static final String TOKEN_EXPIRED = "SESSION EXPIRED LOGIN AGAIN";


}
