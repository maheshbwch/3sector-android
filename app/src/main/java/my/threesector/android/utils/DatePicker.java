package my.threesector.android.utils;

import android.content.DialogInterface;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import my.threesector.android.interfaces.DatePickerInterface;

public class DatePicker {

    private DatePickerDialog datePickerDialog = null;

    public DatePicker(String type, final DatePickerInterface datePickerInterface) {

        Calendar calendar = Calendar.getInstance();


        datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                            @Override
                                                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                                monthOfYear = ++monthOfYear;
                                                                datePickerInterface.onDateSelected(dayOfMonth, monthOfYear, year);
                                                            }
                                                        },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );

        if (datePickerDialog != null) {

            if (datePickerDialog.getVersion() == DatePickerDialog.Version.VERSION_1) {
                datePickerDialog.setScrollOrientation(DatePickerDialog.ScrollOrientation.HORIZONTAL);
            } else {
                datePickerDialog.setScrollOrientation(DatePickerDialog.ScrollOrientation.VERTICAL);
            }

            datePickerDialog.setAccentColor(Color.parseColor("#683894"));

            if (type.equalsIgnoreCase(Constants.DATE_FUTURE) || type.equalsIgnoreCase(Constants.DATE_BIRTH_DATE)) {
                datePickerDialog.setMaxDate(calendar);
            } else if (type.equalsIgnoreCase(Constants.DATE_PAST)){
                datePickerDialog.setMinDate(calendar);
            }

            if (type.equalsIgnoreCase(Constants.DATE_BIRTH_DATE)) {
                //calendar.set(1990, 01, 01);
                datePickerDialog.showYearPickerFirst(true);
            }


            datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    datePickerInterface.onDialogDismiss();
                }
            });


        }
    }

    public void showDatePicker(AppCompatActivity appCompatActivity) {
        datePickerDialog.show(appCompatActivity.getSupportFragmentManager(), "Datepickerdialog");
    }

    public void showDatePicker(Fragment fragment) {
        datePickerDialog.show(fragment.getChildFragmentManager(), "Datepickerdialog");
    }


}
