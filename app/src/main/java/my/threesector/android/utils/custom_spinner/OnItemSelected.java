package my.threesector.android.utils.custom_spinner;

public interface OnItemSelected {
    void onItemSelected(int position);
}
