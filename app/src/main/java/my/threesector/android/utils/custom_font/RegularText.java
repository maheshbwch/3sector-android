package my.threesector.android.utils.custom_font;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class RegularText extends TextView {

    /*
     * Caches typefaces based on their file path and name, so that they don't have to be created every time when they are referenced.
     */
    private static Typeface mTypeface;

    public RegularText(final Context context) {
        this(context, null);
    }

    public RegularText(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RegularText(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        if (mTypeface == null) {
            mTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/Kastelov-Axiforma-Regular.otf");
        }
        setTypeface(mTypeface);
    }

}


