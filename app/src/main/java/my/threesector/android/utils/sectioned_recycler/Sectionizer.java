package my.threesector.android.utils.sectioned_recycler;

public interface Sectionizer<T>{
    public String getSectionTitle(T object);
}