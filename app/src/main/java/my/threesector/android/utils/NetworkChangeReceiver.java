package my.threesector.android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import my.threesector.android.interfaces.ConnectivityInterface;


public class NetworkChangeReceiver extends BroadcastReceiver {

    private ConnectivityInterface connectivityInterface = null;

    public NetworkChangeReceiver() {
    }

    public NetworkChangeReceiver(ConnectivityInterface connectivityInterface) {
        this.connectivityInterface = connectivityInterface;
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        if (connectivityInterface != null) {
            if (isOnline(context)) {
                connectivityInterface.onConnected();
            } else {
                connectivityInterface.onNotConnected();
            }
        }
    }

    private boolean isOnline(Context context) {
        return MyUtils.isConnected(context);
    }
}