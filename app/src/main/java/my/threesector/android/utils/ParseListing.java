package my.threesector.android.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import my.threesector.android.models.CategoryListing;

public class ParseListing {


    public ParseListing() {
    }


    public static ArrayList<CategoryListing> getCategoryListingArrayList(JSONArray jsonArray) {
        ArrayList<CategoryListing> categoryListingArrayList = new ArrayList<>();
        try {
            if (jsonArray != null && jsonArray.length() > 0) {

                for (int i = 0; i < jsonArray.length(); i++) {
                    String urlType = null;
                    String iconUrl = null;
                    String address = null;
                    String latitude = null;
                    String longitude = null;
                    String businessStatus = null;
                    int mode = 0;
                    int average = 0;
                    String categoryName = null;

                    String listingId = jsonArray.optJSONObject(i).optString("ID");
                    String postTitle = jsonArray.optJSONObject(i).optString("postTitle");
                    String tagLine = jsonArray.optJSONObject(i).optString("tagLine");
                    String phone = jsonArray.optJSONObject(i).optString("phone");
                    String logo = jsonArray.optJSONObject(i).optString("logo");
                    String coverImg = jsonArray.optJSONObject(i).optString("coverImg");

                    JSONObject addressObject = jsonArray.optJSONObject(i).optJSONObject("oAddress");
                    if (addressObject != null) {
                        address = addressObject.optString("address");
                        latitude = addressObject.optString("lat");
                        longitude = addressObject.optString("lng");
                    }


                    JSONObject businessStatusObject = jsonArray.optJSONObject(i).optJSONObject("businessStatus");
                    if (businessStatusObject != null) {
                        businessStatus = businessStatusObject.optString("text");
                    }

                    JSONObject reviewObject = jsonArray.optJSONObject(i).optJSONObject("oReview");
                    if (reviewObject != null) {
                        mode = reviewObject.optInt("mode");
                        average = reviewObject.optInt("average");
                    }

                    JSONObject termObject = jsonArray.optJSONObject(i).optJSONObject("oTerm");
                    if (termObject != null) {
                        categoryName = termObject.optString("name");
                    }


                    JSONObject iconObject = jsonArray.optJSONObject(i).optJSONObject("oIcon");
                    if (iconObject != null) {
                        urlType = iconObject.optString("type");
                        iconUrl = iconObject.optString("url");
                    }

                    categoryListingArrayList.add(new CategoryListing(listingId, postTitle, tagLine,phone, logo, coverImg, address,latitude,longitude,businessStatus, mode, average, categoryName, urlType, iconUrl));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return categoryListingArrayList;
    }


}
