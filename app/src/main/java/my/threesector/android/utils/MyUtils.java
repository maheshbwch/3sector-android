package my.threesector.android.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import org.joda.time.LocalDate;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import my.threesector.android.BuildConfig;
import my.threesector.android.R;

public class MyUtils {

    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static void openOverrideAnimation(boolean isOpen, Activity activity) {
        activity.overridePendingTransition(isOpen ? R.anim.slide_from_right : R.anim.slide_from_left, isOpen ? R.anim.slide_to_left : R.anim.slide_to_right);
    }

    public static Typeface getBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Kastelov-Axiforma-Bold.otf");
    }

    public static Typeface getMedBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Kastelov-Axiforma-Medium.otf");
    }

    public static Typeface getRegularFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/Kastelov-Axiforma-Regular.otf");
    }

    public static boolean checkStringValue(String value) {
        return value != null && !value.equalsIgnoreCase("null") && !value.isEmpty();
    }


    public static void showLongToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    public static ProgressDialog showProgressLoader(Context context, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }

    public static void dismissProgressLoader(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void intentToImageSelection(Activity activity, int code) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), code);
    }

    public static String intentToCameraApp(Activity activity, int code) {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile(activity);
            } catch (IOException ex) {
                //Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                activity.startActivityForResult(pictureIntent, code);

                return photoFile.getAbsolutePath();
            }
        }
        return null;
    }

    public static File createImageFile(Activity activity) throws IOException {
       /* File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
        file.createNewFile();
        return  file;*/
        File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(getImageName(), ".jpg", storageDir);
    }

    public static String getImageName() {
        return Constants.APP_NAME_UNDER_SCORE + getCurrentTimeStamp();
    }

    public static String getCurrentTimeStamp() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static boolean isValidEmail(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }


    public static void openWhatsApp(Context context, String number, String message) {
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + number + "&text=" + URLEncoder.encode(message, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
            } else {
                Toast.makeText(context, "Whatsapp not installed in your device", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e("ERROR WHATSAPP", e.toString());
            Toast.makeText(context, "Whatsapp not installed in your device", Toast.LENGTH_LONG).show();
        }
    }

    public static boolean isInternetConnected(Context mContext) {
        boolean outcome = false;
        try {
            if (mContext != null) {
                ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
                for (NetworkInfo tempNetworkInfo : networkInfos) {
                    if (tempNetworkInfo.isConnected()) {
                        outcome = true;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outcome;
    }

    public static boolean isOreoOrAbove() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public static boolean appInstalledOrNot(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


    public static String getFormattedDate(int dayOfMonth, int monthOfYear, int year) {

        String day = "";
        String month = "";

        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
        } else {
            day = "" + dayOfMonth;
        }

        if (monthOfYear < 10) {
            month = "0" + monthOfYear;
        } else {
            month = "" + monthOfYear;
        }


        //String date = "" + day + "-" + month + "-" + year;
        String date = "" + year + "-" + month + "-" + day;
        Log.e("date", "selected:" + date);
        return date;
    }

    public static String getCurrentDate(String format) {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String date = df.format(c);
        return date;
    }

    public static String getCurrentMonthDate(String format) {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(format);
        String date = df.format(c);
        return date;
    }

    public static String getCurrentMonthName(String format) {
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(c.getTime());// NOW
    }

    public static String getNextMonthName(String format) {
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        c.add(Calendar.MONTH, +1);// One month ago
        return sdf.format(c.getTime());
    }

    public static Date getDateFromLocalDate(LocalDate localDate) {
        int day = localDate.getDayOfMonth();
        int month = localDate.getMonthOfYear();
        int year = localDate.getYear();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DATE, day);
        return calendar.getTime();
    }


    public static String getDateWithDay(Date date) {
        return getDayWithDateFormat().format(date);
    }

    public static SimpleDateFormat getDayWithDateFormat() {
        return new SimpleDateFormat(Constants.DAY_WITH_DATE_FORMAT);
    }

    public static String getDateForApi(Date date) {
        return getDateFormatForApi().format(date);
    }

    public static SimpleDateFormat getDateFormatForApi() {
        return new SimpleDateFormat(Constants.DATE_FORMAT_REVERSE);
    }

    public static String getStatus(String statusCode) {
        switch (statusCode) {
            case Constants.REQUESTED:
                return "Requested";
            case Constants.ACCEPTED:
                return "Accepted";
            case Constants.DECLINED:
                return "Declined";
            case Constants.COMPLETED:
                return "Completed";
            case Constants.WORKING:
                return "Working";
            default:
                return "";
        }
    }

    public static String getTimeFromHoursMinutes(int hr, int min) {
        return "" + (hr < 10 ? ("0" + hr) : ("" + hr)) + ":" + (min < 10 ? ("0" + min) : ("" + min)) + " " + ((hr < 12) ? "AM" : "PM");
    }

    public static String getDateWithDayName(int day, int month, int year, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DATE, day);
        Date date = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }


    public static void viewHtmlTxt(String text, TextView textView){
        if (MyUtils.checkStringValue(text)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                textView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
            } else {
                textView.setText(Html.fromHtml(text));
            }
        } else {
            textView.setText("-");
        }
    }

    public static void openCallIntent(Context context, String number) {
        Log.e("number", "" + number);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            //Creating intents for making a call
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            context.startActivity(callIntent);
        } else {
            Toast.makeText(context, "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }

    public static String getDateFromTimeStamp(long timeStamp) {
        try {
            Timestamp ts = new Timestamp(timeStamp);
            Date date = new Date(ts.getTime());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
            String dateAndTime = simpleDateFormat.format(date);
            return "" + dateAndTime;
        } catch (Exception e) {
            Log.e("exp:", ":" + e.getMessage());
            return "";
        }
    }


}
