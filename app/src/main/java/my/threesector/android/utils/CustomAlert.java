package my.threesector.android.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Button;

import my.threesector.android.R;
import my.threesector.android.interfaces.CustomAlertInterface;

public class CustomAlert extends AlertDialog.Builder {

    private Context context = null;

    public CustomAlert(final Context context,String message, String positiveButtonTxt, String negativeButtonTxt, final CustomAlertInterface customAlertInterface) {
        super(context);
        this.context = context;
        this.setTitle(Constants.APP_NAME);
        this.setIcon(R.mipmap.app_logo);
        this.setMessage(message);

        this.setPositiveButton(positiveButtonTxt, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
                customAlertInterface.onPositiveButtonClicked();

            }
        });
        if (MyUtils.checkStringValue(negativeButtonTxt)) {
            this.setNegativeButton(negativeButtonTxt, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    customAlertInterface.onNegativeButtonClicked();
                }
            });
        }
        this.setCancelable(false);

    }


    public void showDialog(int positiveButtonColor,int negativeButtonColor) {
        AlertDialog dialog = this.create();
        dialog.show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        positiveButton.setTextColor(context.getResources().getColor(positiveButtonColor));
        negativeButton.setTextColor(context.getResources().getColor(negativeButtonColor));


       /* positiveButton.setTextColor(context.getResources().getColor(R.color.blue));
        negativeButton.setTextColor(context.getResources().getColor(R.color.blue));*/
    }

    /*public AlertDialog getDialog() {
        AlertDialog dialog = this.create();
        dialog.show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        positiveButton.setTextColor(context.getResources().getColor(R.color.blue));
        negativeButton.setTextColor(context.getResources().getColor(R.color.blue));
        return dialog;
    }*/


}
