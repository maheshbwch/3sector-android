package my.threesector.android.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import my.threesector.android.R;
import my.threesector.android.interfaces.UploadAlertListener;

public class UploadAlert {

    private Activity activity;
    private UploadAlertListener uploadAlertListener;


    public UploadAlert(Activity activity, UploadAlertListener uploadAlertListener) {
        this.activity = activity;
        this.uploadAlertListener = uploadAlertListener;
    }


    public void showAlertDialog() {

        LayoutInflater inflater = activity.getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_upload, null);

        ImageView closeImageView = alertLayout.findViewById(R.id.lud_closeImage);
        LinearLayout filesLinear = alertLayout.findViewById(R.id.lud_filesLinear);
        LinearLayout cameraLinear = alertLayout.findViewById(R.id.lud_cameraLinear);


        final AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        //alert.setTitle("Choose File");
        alert.setView(alertLayout);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();


        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog(dialog);
            }
        });


        filesLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadAlertListener.onChooseFileClicked();
                dismissDialog(dialog);
            }
        });

        cameraLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadAlertListener.onCaptureCameraClicked();
                dismissDialog(dialog);

            }
        });

    }


    private void dismissDialog(AlertDialog alertDialog) {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }


}
