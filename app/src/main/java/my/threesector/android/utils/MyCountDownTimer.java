package my.threesector.android.utils;

import android.os.CountDownTimer;

import my.threesector.android.interfaces.CountDownInterface;


public class MyCountDownTimer extends CountDownTimer {

    private CountDownInterface countDownInterface = null;


    public MyCountDownTimer(long millisInFuture, long countDownInterval, CountDownInterface countDownInterface) {
        super(millisInFuture, countDownInterval);
        this.countDownInterface = countDownInterface;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        countDownInterface.onTicking(millisUntilFinished);
    }

    @Override
    public void onFinish() {
        countDownInterface.onFinished();
    }
}