package my.threesector.android.utils;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import my.threesector.android.interfaces.ChatUserListener;

public class AddChatUser {

    private DatabaseReference rootReference;
    private String senderId = "";
    private String senderName = "";
    private String senderImageUrl = "";

    //String senderId, String senderName, String senderImageUrl

    private String receiverId = "";
    private String receiverName = "";
    private String receiverImageUrl = "";

    private ChatUserListener chatUserListener = null;


    public AddChatUser(DatabaseReference rootReference, String senderId, String senderName, String senderImageUrl, String receiverId, String receiverName, String receiverImageUrl) {
        this.rootReference = rootReference;
        this.senderId = senderId;
        this.senderName = senderName;
        this.senderImageUrl = senderImageUrl;
        this.receiverId = receiverId;
        this.receiverName = receiverName;
        this.receiverImageUrl = receiverImageUrl;
    }


    public void setChatUserListener(ChatUserListener chatUserListener) {
        this.chatUserListener = chatUserListener;
    }


    public void fcmUserRegister() {
        rootReference.child(Constants.FDB_USERS).child(senderId).setValue(getUserMap(senderId, senderName, senderImageUrl)).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task1) {
                if (task1.isSuccessful()) {
                    fcmVendorRegister();
                } else {
                    chatUserListener.onFailed();
                }
            }
        });
    }

    private void fcmVendorRegister() {
        rootReference.child(Constants.FDB_USERS).child(receiverId).setValue(getUserMap(receiverId, receiverName, receiverImageUrl)).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task1) {
                if (task1.isSuccessful()) {
                    checkWeatherAlreadyFriends();
                } else {
                    chatUserListener.onFailed();
                }
            }
        });
    }


    private void checkWeatherAlreadyFriends() {

        rootReference.child(Constants.FDB_FRIENDS).child(senderId).child(receiverId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Log.e("value", "" + dataSnapshot.getValue());

                if (dataSnapshot.getValue() == null) {
                    combineFriends();
                } else {
                    chatUserListener.onUserCreated();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("value", "" + databaseError.getMessage());
                chatUserListener.onFailed();
            }
        });
    }


    private void combineFriends() {
        rootReference.child(Constants.FDB_FRIENDS).child(senderId).child(receiverId).setValue(getFriendInfo(senderId,senderName,senderImageUrl,receiverId, receiverName, receiverImageUrl, getMessageKey(senderId, receiverId))).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task1) {
                if (task1.isSuccessful()) {
                    checkWeatherAlreadyFriendsWithReceiver();
                } else {
                    chatUserListener.onFailed();
                }
            }
        });
    }


    private void checkWeatherAlreadyFriendsWithReceiver() {
        rootReference.child(Constants.FDB_FRIENDS).child(receiverId).child(senderId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e("value", "" + dataSnapshot.getValue());
                if (dataSnapshot.getValue() == null) {
                    combineFriendsForReceiver();
                } else {
                    chatUserListener.onUserCreated();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("value", "" + databaseError.getMessage());
                chatUserListener.onFailed();
            }
        });
    }


    private void combineFriendsForReceiver() {
        rootReference.child(Constants.FDB_FRIENDS).child(receiverId).child(senderId).setValue(getFriendInfo(receiverId,receiverName,receiverImageUrl,senderId, senderName, senderImageUrl, getMessageKey(senderId, receiverId))).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task1) {
                if (task1.isSuccessful()) {
                    chatUserListener.onUserCreated();
                } else {
                    chatUserListener.onFailed();
                }
            }
        });
    }


    private static Map getUserMap(String userId, String userName, String userImageUrl) {
        //String token_id = FirebaseInstanceId.getInstance().getToken();
        userName = MyUtils.checkStringValue(userName) ? userName : "Anonymous user";
        Map<String, String> userMap = new HashMap<>();
        //userMap.put("device_token", token_id);
        userMap.put("user_id", "" + userId);
        userMap.put("name", userName);
        userMap.put("image", userImageUrl);
        userMap.put("online", "true");
        userMap.put("createdDate", "" + System.currentTimeMillis());

        return userMap;
    }

    private static Map getFriendInfo(String senderId, String senderName, String senderImageUrl,String receiverId, String receiverName, String receiverImageUrl, String messageKey) {
        senderName = MyUtils.checkStringValue(senderName) ? senderName : "Anonymous user";
        receiverName = MyUtils.checkStringValue(receiverName) ? receiverName : "Anonymous user";
        Map<String, String> userMap = new HashMap<>();
        userMap.put("sender_id", "" + senderId);
        userMap.put("sender_name", senderName);
        userMap.put("sender_image", senderImageUrl);
        userMap.put("receiver_id", "" + receiverId);
        userMap.put("receiver_name", receiverName);
        userMap.put("receiver_image", receiverImageUrl);
        userMap.put("message_key", messageKey);
        userMap.put("date", "" + System.currentTimeMillis());
        return userMap;
    }


    private static String getMessageKey(String userId, String receiverId) {
        return "" + userId + "_" + receiverId;
    }


}
