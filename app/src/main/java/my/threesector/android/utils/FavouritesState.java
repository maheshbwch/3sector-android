package my.threesector.android.utils;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import my.threesector.android.interfaces.FavouritesListener;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouritesState {

    private boolean isAlreadyAdded = false;
    private FavouritesListener favouritesListener = null;


    public FavouritesState(boolean isAlreadyAdded) {
        this.isAlreadyAdded = isAlreadyAdded;
    }

    public void setFavouritesListener(FavouritesListener favouritesListener) {
        this.favouritesListener = favouritesListener;
    }


    public void addToFavourites(String postId, String accessToken) {
        try {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("postID", RequestBody.create(MediaType.parse("text/plain"), postId));
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.addToFavorites(map, (Constants.BEARER + accessToken));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);
                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status)) {

                                if (status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    String favStatus = jsonObject.optString("is");

                                    Log.e("favStatus", ":" + favStatus);

                                    if (MyUtils.checkStringValue(favStatus)) {

                                        if (favStatus.equalsIgnoreCase("removed")) {
                                            favouritesListener.onFavouritesSuccess(Constants.REMOVE_FROM_FAVOURITES);
                                        } else if (favStatus.equalsIgnoreCase("added")) {
                                            favouritesListener.onFavouritesSuccess(Constants.ADD_TO_FAVOURITES);
                                        }
                                    } else {
                                        favouritesListener.onFavouritesFailed(Constants.ERROR_RESPONSE, isAlreadyAdded ? ErrorConstants.FAVOURITE_REMOVE_ERROR : ErrorConstants.FAVOURITE_ADD_ERROR);
                                    }

                                } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {

                                    String msg = jsonObject.optString("msg");

                                    if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.TOKEN_EXPIRED)) {
                                        //TODO HANDLE TOKEN
                                        favouritesListener.onFavouritesFailed(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                        Log.e("error", "access token expired");
                                    } else {
                                        favouritesListener.onFavouritesFailed(Constants.ERROR_RESPONSE, isAlreadyAdded ? ErrorConstants.FAVOURITE_REMOVE_ERROR : ErrorConstants.FAVOURITE_ADD_ERROR);
                                    }
                                } else {
                                    Log.e("error", "error response");
                                    favouritesListener.onFavouritesFailed(Constants.ERROR_RESPONSE, isAlreadyAdded ? ErrorConstants.FAVOURITE_REMOVE_ERROR : ErrorConstants.FAVOURITE_ADD_ERROR);
                                }
                            } else {
                                favouritesListener.onFavouritesFailed(Constants.ERROR_RESPONSE, isAlreadyAdded ? ErrorConstants.FAVOURITE_REMOVE_ERROR : ErrorConstants.FAVOURITE_ADD_ERROR);
                            }
                            //{"status":"error","msg":"tokenExpired"}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            favouritesListener.onFavouritesFailed(Constants.ERROR_RESPONSE, isAlreadyAdded ? ErrorConstants.FAVOURITE_REMOVE_ERROR : ErrorConstants.FAVOURITE_ADD_ERROR);
                        }
                    } else {
                        Log.e("error", "null response");
                        favouritesListener.onFavouritesFailed(Constants.ERROR_RESPONSE, isAlreadyAdded ? ErrorConstants.FAVOURITE_REMOVE_ERROR : ErrorConstants.FAVOURITE_ADD_ERROR);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        favouritesListener.onFavouritesFailed(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    } else {
                        Log.e("error", "message other issue:" + t.getMessage());
                        favouritesListener.onFavouritesFailed(Constants.ERROR_RESPONSE, isAlreadyAdded ? ErrorConstants.FAVOURITE_REMOVE_ERROR : ErrorConstants.FAVOURITE_ADD_ERROR);
                    }
                }
            });
        } catch (Exception e) {
            favouritesListener.onFavouritesFailed(Constants.ERROR_RESPONSE, isAlreadyAdded ? ErrorConstants.FAVOURITE_REMOVE_ERROR : ErrorConstants.FAVOURITE_ADD_ERROR);
            Log.e("exp", ":" + e.getMessage());
        }
    }

}
