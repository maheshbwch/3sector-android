package my.threesector.android.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import my.threesector.android.R;
import my.threesector.android.interfaces.DialogInterface;


public class CustomDialog {

    private Activity activity;
    private String statusCode;
    private String statusMessage;
    private DialogInterface dialogInterface;
    private AlertDialog dialog = null;

    private String titleMessage = null;
    private String buttonTxt = null;
    private boolean isStatusMessageIsAPath = false;


    public CustomDialog(Activity activity, String statusCode, String statusMessage, DialogInterface dialogInterface) {
        this.activity = activity;
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.dialogInterface = dialogInterface;
    }


    public void setButtonTxt(String buttonTxt) {
        this.buttonTxt = buttonTxt;
    }

    public void setTitleMessage(String titleMessage) {
        this.titleMessage = titleMessage;
    }

    public void isStatusMessageIsAPath(boolean isStatusMessageIsAPath) {
        this.isStatusMessageIsAPath = isStatusMessageIsAPath;
    }


    public void showDialog() {

        LayoutInflater inflater = activity.getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.dialog_custom, null);

        ImageView close = alertLayout.findViewById(R.id.cd_imgClose);
        ImageView statusImage = alertLayout.findViewById(R.id.cd_statusImage);
        TextView statusTitleTxt = alertLayout.findViewById(R.id.cd_statusTitleTxt);
        TextView statusMessageTxt = alertLayout.findViewById(R.id.cd_statusMessageTxt);
        Button submitButton = alertLayout.findViewById(R.id.cd_submitButton);

        if (MyUtils.checkStringValue(titleMessage)) {
            statusTitleTxt.setVisibility(View.VISIBLE);
            statusTitleTxt.setText(titleMessage);
        } else {
            statusTitleTxt.setVisibility(View.GONE);
        }

        if (MyUtils.checkStringValue(statusMessage)) {
            statusMessageTxt.setText(statusMessage);
        }


        if (MyUtils.checkStringValue(buttonTxt)) {
            submitButton.setText(buttonTxt);
        } else {
            submitButton.setText("OKAY");
        }


        setUpIconAndButton(statusCode, statusImage, submitButton);

        final AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setView(alertLayout);
        alert.setCancelable(false);
        dialog = alert.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
                dialogInterface.onButtonClicked();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissDialog();
                dialogInterface.onDismissedClicked();
            }
        });

    }


    public void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    private void setUpIconAndButton(String status, ImageView imageView, Button button) {
        switch (status) {
            case Constants.DIALOG_SUCCESS:
                imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.success));
                button.setBackground(activity.getResources().getDrawable(R.drawable.success_button));
                break;
            case Constants.DIALOG_FAILURE:
                imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.failure));
                button.setBackground(activity.getResources().getDrawable(R.drawable.failure_button));
                break;
            case Constants.DIALOG_WARNING:
                imageView.setImageDrawable(activity.getResources().getDrawable(R.drawable.warning));
                button.setBackground(activity.getResources().getDrawable(R.drawable.warning_button));
                break;
                case Constants.DIALOG_GENERAL:
                imageView.setImageDrawable(activity.getResources().getDrawable(R.mipmap.app_logo));
                button.setBackground(activity.getResources().getDrawable(R.drawable.general_button));
                break;
            default:
                break;
        }
    }


}