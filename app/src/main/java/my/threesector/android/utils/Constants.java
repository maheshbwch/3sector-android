package my.threesector.android.utils;

public class Constants {

    public static final boolean DEBUG_MODE = true; //TODO SET FALSE BEFORE BUILD APK
    public static final boolean SKIP_OTP = false; //TODO SET FALSE BEFORE BUILD APK

    public static final String APP_NAME = "3 Sector";
    public static final String APP_NAME_UNDER_SCORE = "THREE_SECTOR";
    public static final String API_KEY = "293df0c34d1d7d71fe036200cb70e871";

    public static final int SPLASH_SCREEN_DELAY = 2000; //2 seconds
    public static final int OTP_TIME_OUT = 120;

    public static final int LOGIN = 2017;
    public static final int LOGIN_SUCCESS = 2018;
    public static final int LOGIN_FAILURE = 2019;

    public static final int BECOME_A_MERCHANT_RESULT = 1001;
    public static final int BECOME_A_MERCHANT_SUCCESS_RESULT = 1002;

    public static final int LOCATION_PERMISSION = 2002;
    public static final int CALL_PHONE = 2003;

    public static final String BEARER = "Bearer ";
    public static final String PREFS_NAME = "pref_name";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String LOGIN_ID = "login_id";
    public static final String LOGIN_PASSWORD = "login_password";

    public static final String USER_ROLE = "user_role";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_PROFILE_PICTURE = "user_profile_picture";
    public static final String USER_COVER_PICTURE = "user_cover_picture";
    public static final String USER_EMAIL_ADDRESS = "user_email_address";


    public static final String NORMAL_USER = "normal_user";
    public static final String MERCHANT_USER = "merchant_user";

    public static final int SUCCESS = 1;
    public static final int FAILURE = 0;

    public static final String DIALOG_SUCCESS = "0";
    public static final String DIALOG_FAILURE = "1";
    public static final String DIALOG_WARNING = "2";
    public static final String DIALOG_GENERAL = "3";

    public static final String REQUESTED = "0";
    public static final String ACCEPTED = "1";
    public static final String DECLINED = "2";
    public static final String COMPLETED = "3";
    public static final String WORKING = "4";

    public static final String LOGGED_IN = "loggedIn";
    public static final String API_ERROR = "error";
    public static final String API_SUCCESS = "success";
    public static final String API_SUCCESS_CODE = "1";
    public static final String API_FAILURE_CODE = "0";
    public static final String TOKEN_EXPIRED = "tokenExpired";
    public static final String USER_NOT_FOUND = "foundNoUser";

    public static final String ADD_TO_FAVOURITES = "add_to_favorites";
    public static final String REMOVE_FROM_FAVOURITES = "remove_from_favourites";

    public static final String DATE_FORMAT_REVERSE = "yyyy-MM-dd";
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String DAY_WITH_DATE_FORMAT = "EEEE, dd-MMM-yyyy";
    public static final String MONTH_NAME_DATE_FORMAT = "MMM";
    public static final String MONTH_NAME_FULL_DATE_FORMAT = "MMMM";
    public static final String TIME_WITH_MERIDIAN = "h:mm a";
    public static final String TIME_WITHOUT_MERIDIAN = "HH:mm";

    public static final String DATE_FUTURE = "future_date";
    public static final String DATE_PAST = "past_date";
    public static final String DATE_BIRTH_DATE = "birth_date";
    public static final String DATE_GENERAL = "general";

    public static final String NO_INTERNET = "no_internet";
    public static final String ERROR_RESPONSE = "error_response";
    public static final String TOKEN_SESSION_EXPIRED = "token_session_expired";

    public static final String NEAR_BY_RADIUS = "500";
    public static final String UNIT_KM = "km";

    public static final String EXPERIENCE_SEERVICES = "Experience Services";
    public static final String HOT_DEAL_PRODUCTS = "Hot Deal Products";
    public static final String RECOMMENDED_REAL_ESTATES = "Recommended Real Estate";
    public static final String HIGH_REVIEW_MACHINERY = "High Review Machinery";
    public static final String FEATURED_COMPANIES = "Featured Latest Joined Companies";

    public static final String FDB_USERS = "AOS_CHAT/Users";
    public static final String FDB_FRIENDS = "AOS_CHAT/Friends";
    public static final String FDB_MESSAGES = "AOS_CHAT/Messages";

    public static final String CONTRIBUTOR = "contributor";
    public static final String SUBSCRIBER = "subscriber";
    public static final String ADMINISTRATOR = "administrator";

    public static final String TEXT = "text";
    public static final String IMAGE = "image";





}
