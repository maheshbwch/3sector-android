package my.threesector.android.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;

import my.threesector.android.R;
import my.threesector.android.interfaces.DialogInterface;
import my.threesector.android.models.Login;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.CustomDialog;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    private Context context;
    private ImageView backgroundImage;
    private TextView btnRegister;
    private TextInputLayout nameTil, emailAddressTil, passwordTil, conPasswordTil;
    private TextInputEditText nameEdt, emailAddressEdt, passwordEdt, conPasswordEdt;
    private LinearLayout backLinear;
    private CheckBox privacyPolicyCheckBox, termsCheckBox;
    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        context = RegistrationActivity.this;

        backgroundImage = findViewById(R.id.ra_backgroundImage);

        nameTil = findViewById(R.id.ra_nameTil);
        nameEdt = findViewById(R.id.ra_nameEdt);

        emailAddressTil = findViewById(R.id.ra_emailAddressTil);
        emailAddressEdt = findViewById(R.id.ra_emailAddressEdt);

        passwordTil = findViewById(R.id.ra_passwordTil);
        passwordEdt = findViewById(R.id.ra_passwordEdt);

        conPasswordTil = findViewById(R.id.ra_conPasswordTil);
        conPasswordEdt = findViewById(R.id.ra_conPasswordEdt);

        privacyPolicyCheckBox = findViewById(R.id.ra_privacyPolicyCheckBox);
        termsCheckBox = findViewById(R.id.ra_termsCheckBox);

        btnRegister = findViewById(R.id.ra_btnRegister);
        backLinear = findViewById(R.id.ra_backLinear);

        listeners();

    }

    private void listeners() {
        try {
            Glide.with(context).load(R.drawable.splash1).placeholder(R.drawable.splash1).into(backgroundImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Typeface typeface = MyUtils.getRegularFont(context);
        nameTil.setTypeface(typeface);
        nameEdt.setTypeface(typeface);

        emailAddressTil.setTypeface(typeface);
        emailAddressEdt.setTypeface(typeface);

        passwordTil.setTypeface(typeface);
        passwordEdt.setTypeface(typeface);

        conPasswordTil.setTypeface(typeface);
        conPasswordEdt.setTypeface(typeface);

        privacyPolicyCheckBox.setTypeface(typeface);
        termsCheckBox.setTypeface(typeface);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFields();
            }
        });

        backLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closePage();
            }
        });


    }


    private void validateFields() {

        String name = nameEdt.getText().toString().trim();
        String emailAddress = emailAddressEdt.getText().toString().trim();
        String password = passwordEdt.getText().toString().trim();
        String conPassword = conPasswordEdt.getText().toString().trim();

        if (!MyUtils.checkStringValue(name)) {
            MyUtils.showLongToast(context, "Name required");
        } else if (!MyUtils.checkStringValue(emailAddress)) {
            MyUtils.showLongToast(context, "Email Address required");
        } else if (!MyUtils.isValidEmail(emailAddress)) {
            MyUtils.showLongToast(context, "Enter Valid Email Address");
        } else if ((!MyUtils.checkStringValue(password))) {
            MyUtils.showLongToast(context, "Password required");
        } else if ((!MyUtils.checkStringValue(conPassword))) {
            MyUtils.showLongToast(context, "Confirm Password required");
        } else if (!password.equalsIgnoreCase(conPassword)) {
            MyUtils.showLongToast(context, "Password and Confirm Password are not matched");
        } else if (!privacyPolicyCheckBox.isChecked()) {
            MyUtils.showLongToast(context, "Please accept Privacy Policy");
        } else if (!termsCheckBox.isChecked()) {
            MyUtils.showLongToast(context, "Please accept Terms and Condition");
        } else {
            signUpUser(name, emailAddress, password);
        }
    }


    private void signUpUser(String name, String emailAddress, String password) {
        try {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("username", RequestBody.create(MediaType.parse("text/plain"), name));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), emailAddress));
            map.put("password", RequestBody.create(MediaType.parse("text/plain"), password));
            map.put("isAgreeToPrivacyPolicy", RequestBody.create(MediaType.parse("text/plain"), "true"));
            map.put("isAgreeToTermsAndConditionals", RequestBody.create(MediaType.parse("text/plain"), "true"));


            progressDialog = MyUtils.showProgressLoader(context, "Signing Up..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);
            Call<Login> call = apiService.registerUser(map);
            call.enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {
                        Login login = response.body();
                        if (login != null) {

                            String status = login.getStatus();

                            Log.e("status", ":" + status);

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {


                                showAlertToUser(Constants.DIALOG_SUCCESS, "Sign Up", ErrorConstants.REGISTER_SUCCESS,true);

                                /*String accessToken = login.getToken();
                                 PreferenceHandler.storePreference(context, Constants.ACCESS_TOKEN, accessToken);
                                intentToHomePage();*/

                            } else {
                                String message = login.getMsg();
                                String ret = login.getRet();

                                Log.e("error11", "null response");
                                showAlertToUser(Constants.DIALOG_FAILURE, "Sign Up", (MyUtils.checkStringValue(message) ? message : (MyUtils.checkStringValue(ret) ? ret : ErrorConstants.REGISTER_ERROR)),false);
                            }

                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Sign Up", ErrorConstants.REGISTER_ERROR,false);
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Sign Up", ErrorConstants.REGISTER_ERROR,false);
                    }
                }

                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Sign Up", ErrorConstants.REGISTER_ERROR,false);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Sign Up", ErrorConstants.REGISTER_ERROR,false);
        }
    }


    private void showAlertToUser(String statusCode, String title, String statusMessage, boolean isSuccess) {
        CustomDialog customDialog = new CustomDialog(RegistrationActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                if (isSuccess) {
                    closePage();
                }
            }

            @Override
            public void onDismissedClicked() {
                if (isSuccess) {
                    closePage();
                }
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }

    private void intentToHomePage() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, RegistrationActivity.this);
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, RegistrationActivity.this);
    }


}
