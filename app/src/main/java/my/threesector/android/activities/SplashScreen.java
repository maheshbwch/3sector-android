package my.threesector.android.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import my.threesector.android.R;
import my.threesector.android.interfaces.ConnectivityInterface;
import my.threesector.android.interfaces.DialogInterface;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.CustomDialog;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.NetworkChangeReceiver;
import my.threesector.android.utils.PreferenceHandler;

public class SplashScreen extends AppCompatActivity {

    private Context context;
    private ImageView imgLogo;
    private BroadcastReceiver mNetworkReceiver = null;
    private CustomDialog customInternetDialog = null;

    private String fcmToken = "";
    private String deviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_splash);
        context = SplashScreen.this;

        imgLogo = findViewById(R.id.ss_imgLogo);

        try {
            Glide.with(context).load(R.drawable.splash1).placeholder(R.drawable.splash1).into(imgLogo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getFcmToken();

    }

    private void startHandler() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String accessToken = PreferenceHandler.getPreferenceFromString(context, Constants.ACCESS_TOKEN);
                if (MyUtils.checkStringValue(accessToken)) {
                    intentToHomePage();
                } else {
                    intentToLogin();
                }

            }
        }, Constants.SPLASH_SCREEN_DELAY);
    }


    private void intentToLogin() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivityForResult(intent, Constants.LOGIN);
        MyUtils.openOverrideAnimation(true, SplashScreen.this);
    }


    private void hideStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    //=======Internet listener starts here=======================================================================


    private void startNetworkBroadcast() {
        if (mNetworkReceiver != null) {

            Log.e("register", "not null");

            unregisterNetworkChanges();

            registerNetworkBroadcast();

        } else {
            Log.e("register", "null");
            registerNetworkBroadcast();
        }
    }

    private void registerNetworkBroadcast() {
        mNetworkReceiver = new NetworkChangeReceiver(new ConnectivityInterface() {
            @Override
            public void onConnected() {
                Log.e("network", "connected");
                dismissIfDialogsAreOpned();
                startHandler();
            }

            @Override
            public void onNotConnected() {
                Log.e("network", "not-connected");
                showNoInternetAlertDialog();
            }
        });
        registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    protected void unregisterNetworkChanges() {
        if (mNetworkReceiver != null) {
            unregisterReceiver(mNetworkReceiver);
        }
    }

    private CustomDialog getNoInternetAlertDialog() {
        customInternetDialog = new CustomDialog(SplashScreen.this, Constants.DIALOG_WARNING, "Please check your data connection", new DialogInterface() {
            @Override
            public void onButtonClicked() {
                Intent settingsIntent = new Intent(Settings.ACTION_SETTINGS);
                startActivity(settingsIntent);
            }

            @Override
            public void onDismissedClicked() {
                finish();
            }
        });
        customInternetDialog.setTitleMessage("No Internet Connection!");
        customInternetDialog.showDialog();
        return customInternetDialog;
    }

    private void showNoInternetAlertDialog() {

        dismissIfDialogsAreOpned();

        customInternetDialog = getNoInternetAlertDialog();
    }


    private void dismissIfDialogsAreOpned() {
        if (customInternetDialog != null) {
            customInternetDialog.dismissDialog();
        }
    }

    private void getFcmToken() {
        try {
            if (MyUtils.isInternetConnected(context)) {
                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("notifications", "getInstanceId failed", task.getException());
                        }
                        fcmToken = task.getResult().getToken();
                        Log.e("notifications", "token: " + fcmToken);
                    }
                });

                deviceId = MyUtils.getDeviceId(context);
                Log.e("deviceId", "deviceId: " + deviceId);
            }

        } catch (Exception e) {
            Log.e("exception_fcm_token", "" + e);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constants.LOGIN_SUCCESS:
                intentToHomePage();
                break;
            case Constants.LOGIN_FAILURE:
                finish();
                break;
            default:
                break;
        }
    }




    private void intentToHomePage() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, SplashScreen.this);
        finish();
    }


    //============on-resume && on-destroy====================
    @Override
    protected void onResume() {
        super.onResume();
        startNetworkBroadcast();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }
}
