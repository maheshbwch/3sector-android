package my.threesector.android.activities.webview;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.webservice.ApiConstants;

public class QuestionsActivity extends BaseActivity {

    private Context context = null;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_listing);
        context = QuestionsActivity.this;

        webView = findViewById(R.id.ali_webView);

        listeners();
    }


    private void listeners() {
        setTitleTxt("Questions");

        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAppCacheEnabled(false);
        //webView.addJavascriptInterface(new WebAppInterface(this), "Android");
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        webView.setWebViewClient(new MyWebClient());

        webView.loadUrl(ApiConstants.QUESTIONS_WEB_VIEW_URL);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity();
            }
        });
    }


    public class MyWebClient extends WebViewClient {
        private ProgressDialog progressDialog = null;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressDialog = MyUtils.showProgressLoader(context, "Loading..");

            Log.e("onPageStarted", ":" + url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.e("overRideUrl", ":" + url);
            return true;

        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            Log.e("onPageCommit", ":" + url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.e("onPageFinished", ":" + url);
            MyUtils.dismissProgressLoader(progressDialog);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            MyUtils.dismissProgressLoader(progressDialog);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        } else {
            closePage();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        /*if (webView.canGoBack()) {
            webView.goBack();
        } else {
            closePage();
        }*/
        closePage();
    }

}
