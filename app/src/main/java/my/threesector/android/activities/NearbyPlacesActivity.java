package my.threesector.android.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.listing.CategoryListingActivity;
import my.threesector.android.activities.listing.ListingDetailActivity;
import my.threesector.android.adapters.ListingAdapter;
import my.threesector.android.interfaces.FavouritesListener;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.models.CategoriesHome;
import my.threesector.android.models.CategoryListing;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.FavouritesState;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.ParseListing;
import my.threesector.android.utils.custom_spinner.OnItemSelected;
import my.threesector.android.utils.custom_spinner.SpinnerDialog;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearbyPlacesActivity extends BaseActivity {

    private Context context;
    private ImageView backImage;
    private RelativeLayout categoryDropDownRelative;
    private TextView categoryNameTxt;

    private FusedLocationProviderClient fusedLocationClient;
    private SupportMapFragment mapFragment;
    private GoogleMap map;

    private RecyclerView recyclerView;
    private ProgressBar categoryProgress;
    private ProgressDialog progressDialog = null;

    private ArrayList<CategoriesHome> categoriesHomeArrayList = new ArrayList<>();
    private ArrayList<CategoryListing> myListingArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_places);
        context = NearbyPlacesActivity.this;

        backImage = findViewById(R.id.anp_backImage);
        categoryDropDownRelative = findViewById(R.id.anp_categoryDropDownRelative);
        categoryNameTxt = findViewById(R.id.anp_categoryNameTxt);

        recyclerView = findViewById(R.id.anp_recyclerView);
        categoryProgress = findViewById(R.id.anp_categoryProgress);

        listeners();
    }

    private void listeners() {
        hideToolBar();

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.anp_mapView);

        initializeMapFragment();

        getCategoryList();

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

    }


    private void initializeMapFragment() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;

                map.setMyLocationEnabled(true);

                getCurrentLocation();


                /*String latitude = "3.8941425";
                String longitude = "103.3664117";
                getNearByPlaces(latitude, longitude);*/
            }
        });
    }


    private void getCurrentLocation() {
        fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {

                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                    String latitude = "" + latLng.latitude;
                    String longitude = "" + latLng.longitude;

                    Log.e("latitude", ":" + latitude);
                    Log.e("longitude", ":" + longitude);

                    map.addMarker(new MarkerOptions().position(latLng).title("My Location"));
                    map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                    getNearByPlaces(latitude, longitude);
                }
            }
        });
    }


    private void getCategoryList() {
        try {
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getCategories();
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        String jsonResponse = response.body();
                        Log.e("jsonResponse", ":" + jsonResponse);

                        //String categoriesHome = response.body();
                        if (jsonResponse != null) {

                            try {
                                JSONObject jsonObject = new JSONObject(jsonResponse);

                                String status = jsonObject.optString("status");

                                if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONArray jsonArray = jsonObject.optJSONArray("aTerms");

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        categoriesHomeArrayList.clear();
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            String urlType = null;
                                            String iconUrl = null;

                                            String termId = jsonArray.optJSONObject(i).optString("term_id");
                                            String categoryName = jsonArray.optJSONObject(i).optString("name");
                                            JSONObject iconObject = jsonArray.optJSONObject(i).optJSONObject("oIcon");

                                            if (iconObject != null) {
                                                urlType = iconObject.optString("type");
                                                iconUrl = iconObject.optString("url");
                                            }

                                            categoriesHomeArrayList.add(new CategoriesHome(termId, categoryName, urlType, iconUrl, ""));
                                        }
                                    }

                                    loadCategoryValues(categoriesHomeArrayList);

                                } else {
                                    Log.e("error", "error response");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void loadCategoryValues(ArrayList<CategoriesHome> categoryArrayList) {
        categoryDropDownRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpinnerDialog spinnerDialog = new SpinnerDialog(categoryArrayList, new OnItemSelected() {
                    @Override
                    public void onItemSelected(int position) {
                        String categoryId = categoryArrayList.get(position).getTermId();
                        String categoryName = categoryArrayList.get(position).getName();
                        categoryNameTxt.setText(categoryName);

                        Bundle bundle = new Bundle();
                        bundle.putString("termId", categoryId);
                        bundle.putString("name", categoryName);

                        intentToCategoryListing(bundle); //TODO SORT THE NEAR BY ITEMS
                    }
                });
                spinnerDialog.setTitle("Select Category");
                spinnerDialog.show(getSupportFragmentManager(), "dialog");
            }
        });
    }

    private void intentToCategoryListing(Bundle bundle) {
        Intent intent = new Intent(context, CategoryListingActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, NearbyPlacesActivity.this);
    }


    //=====================Get nearby places=============================================


    private void getNearByPlaces(String latitude, String longitude) {
        try {
            categoryProgress.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getNearByMe(latitude, longitude, Constants.NEAR_BY_RADIUS, Constants.UNIT_KM);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    categoryProgress.setVisibility(View.GONE);
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);
                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status)) {

                                if (status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONArray jsonArray = jsonObject.optJSONArray("oResults");

                                    myListingArrayList.clear();
                                    myListingArrayList.addAll(ParseListing.getCategoryListingArrayList(jsonArray));

                                    adaptCategoriesList();

                                    adaptMarkerToMapView();

                                } else {
                                    Log.e("error", "error response");
                                    showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.NEARBY_LISTING_ERROR);
                                }

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.NEARBY_LISTING_ERROR);
                        }

                    } else {
                        Log.e("error", "null response");
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.NEARBY_LISTING_ERROR);
                    }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    categoryProgress.setVisibility(View.GONE);
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        showBaseRelative(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    } else {
                        Log.e("error", "message other issue:" + t.getMessage());
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.NEARBY_LISTING_ERROR);
                    }
                }
            });
        } catch (Exception e) {
            categoryProgress.setVisibility(View.GONE);
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.NEARBY_LISTING_ERROR);
            Log.e("exp", ":" + e.getMessage());
        }
    }

    //========add companies location markers in the map view================================================


    private void adaptMarkerToMapView() {
        if (myListingArrayList.size() > 0) {

            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            for (CategoryListing categoryListing : myListingArrayList) {

                String title = categoryListing.getPostTitle();

                String latitude = categoryListing.getLatitude();
                String longitude = categoryListing.getLongitude();


                if (MyUtils.checkStringValue(latitude) && MyUtils.checkStringValue(longitude)) {
                    LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));

                    builder.include(latLng);

                    map.addMarker(new MarkerOptions().position(latLng).title(MyUtils.checkStringValue(title) ? title : ""));
                }
            }

            LatLngBounds bounds = builder.build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 0);
            map.animateCamera(cameraUpdate);

        }
    }


    //======Show nearby companies in list view===========================================================

    private void adaptCategoriesList() {
        if (myListingArrayList.size() > 0) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);

            ListingAdapter myAdapter = new ListingAdapter(context, myListingArrayList);
            myAdapter.setListingClickListeners(new ListingClickListeners() {
                @Override
                public void onItemClicked(int position) {
                    String listingId = "" + myListingArrayList.get(position).getListingId();
                    String listingTitle = "" + myListingArrayList.get(position).getPostTitle();
                    String logo = "" + myListingArrayList.get(position).getLogo();
                    String coverImage = "" + myListingArrayList.get(position).getCoverImg();

                    Bundle bundle = new Bundle();
                    bundle.putString("listingId", listingId);
                    bundle.putString("listingTitle", listingTitle);
                    bundle.putString("logo", logo);
                    bundle.putString("coverImage", coverImage);

                    intentToCategoryListingPreview(bundle);
                }

                @Override
                public void onFavouritesClicked(int position, SparkButton wishListButton) {

                    String postId = "" + myListingArrayList.get(position).getListingId();

                    addToFavorites(postId, wishListButton);
                }

                @Override
                public void onCallClicked(String phoneNumber) {
                    listingPhoneNumber = phoneNumber;
                    callPhoneNumber();
                }
            });
            recyclerView.setAdapter(myAdapter);
        } else {
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.NEARBY_LISTING_ERROR);
        }
    }

    private void addToFavorites(String postId, SparkButton wishListButton) {
        boolean state = wishListButton.isChecked();
        progressDialog = MyUtils.showProgressLoader(context, (state ? "Removing.." : "Adding.."));
        FavouritesState favouritesState = new FavouritesState(state);
        favouritesState.setFavouritesListener(new FavouritesListener() {
            @Override
            public void onFavouritesSuccess(String status) {
                MyUtils.dismissProgressLoader(progressDialog);

                if (status.equalsIgnoreCase(Constants.ADD_TO_FAVOURITES)) {
                    wishListButton.setChecked(true);
                } else if (status.equalsIgnoreCase(Constants.REMOVE_FROM_FAVOURITES)) {
                    wishListButton.setChecked(false);
                }
            }

            @Override
            public void onFavouritesFailed(String errorType, String error) {
                MyUtils.dismissProgressLoader(progressDialog);
                showBaseRelative(errorType, error);
                if (state) {
                    wishListButton.setChecked(true);
                } else {
                    wishListButton.setChecked(false);
                }
            }
        });
        favouritesState.addToFavourites(postId, accessToken);
    }


    private void intentToCategoryListingPreview(Bundle bundle) {
        Intent intent = new Intent(context, ListingDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, NearbyPlacesActivity.this);
    }


}
