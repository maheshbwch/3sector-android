package my.threesector.android.activities.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.activities.listing.ListingDetailActivity;
import my.threesector.android.adapters.ListingAdapter;
import my.threesector.android.interfaces.FavouritesListener;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.models.CategoryListing;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.FavouritesState;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.ParseListing;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyListingActivity extends BaseActivity {

    private Context context;
    private RecyclerView recyclerView;
    private ProgressBar categoryProgress;
    private ProgressDialog progressDialog = null;

    private ArrayList<CategoryListing> myListingArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favorites);
        context = MyListingActivity.this;

        recyclerView = findViewById(R.id.amf_recyclerView);
        categoryProgress = findViewById(R.id.amf_categoryProgress);

        listeners();
    }

    private void listeners() {
        setTitleTxt("My Listing");

        getMyListing("listing", "1"); //TODO PAGINATION NOT NEEDED
    }


    private void getMyListing(String type, String pageNumber) {
        try {
            categoryProgress.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getMyListing(type, pageNumber, (Constants.BEARER + accessToken));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    categoryProgress.setVisibility(View.GONE);
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);
                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status)) {

                                if (status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONArray jsonArray = jsonObject.optJSONArray("oResults");

                                    myListingArrayList.clear();
                                    myListingArrayList.addAll(ParseListing.getCategoryListingArrayList(jsonArray));

                                    adaptCategoriesList();

                                } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {

                                    String msg = jsonObject.optString("msg");

                                    if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.TOKEN_EXPIRED)) {
                                        //TODO HANDLE TOKEN
                                        showBaseRelative(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                        Log.e("error", "access token expired");
                                    } else {
                                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.MY_LISTING_ERROR);
                                    }
                                } else {
                                    Log.e("error", "error response");
                                    showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.MY_LISTING_ERROR);
                                }

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.MY_LISTING_ERROR);
                        }

                    } else {
                        Log.e("error", "null response");
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.MY_LISTING_ERROR);
                    }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    categoryProgress.setVisibility(View.GONE);
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        showBaseRelative(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    } else {
                        Log.e("error", "message other issue:" + t.getMessage());
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.MY_LISTING_ERROR);
                    }
                }
            });
        } catch (Exception e) {
            categoryProgress.setVisibility(View.GONE);
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.MY_LISTING_ERROR);
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void adaptCategoriesList() {
        if (myListingArrayList.size() > 0) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);

            ListingAdapter myAdapter = new ListingAdapter(context, myListingArrayList);
            myAdapter.setListingClickListeners(new ListingClickListeners() {
                @Override
                public void onItemClicked(int position) {
                    String listingId = "" + myListingArrayList.get(position).getListingId();
                    String listingTitle = "" + myListingArrayList.get(position).getPostTitle();
                    String logo = "" + myListingArrayList.get(position).getLogo();
                    String coverImage = "" + myListingArrayList.get(position).getCoverImg();

                    Bundle bundle = new Bundle();
                    bundle.putString("listingId", listingId);
                    bundle.putString("listingTitle", listingTitle);
                    bundle.putString("logo", logo);
                    bundle.putString("coverImage", coverImage);

                    intentToCategoryListingPreview(bundle);
                }

                @Override
                public void onFavouritesClicked(int position, SparkButton wishListButton) {

                    String postId = "" + myListingArrayList.get(position).getListingId();

                    addToFavorites(postId, wishListButton);
                }

                @Override
                public void onCallClicked(String phoneNumber) {
                    listingPhoneNumber = phoneNumber;
                    callPhoneNumber();
                }
            });
            recyclerView.setAdapter(myAdapter);
        } else {
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.MY_LISTING_ERROR);
        }
    }

    private void addToFavorites(String postId, SparkButton wishListButton) {
        boolean state = wishListButton.isChecked();
        progressDialog = MyUtils.showProgressLoader(context, (state ? "Removing.." : "Adding.."));
        FavouritesState favouritesState = new FavouritesState(state);
        favouritesState.setFavouritesListener(new FavouritesListener() {
            @Override
            public void onFavouritesSuccess(String status) {
                MyUtils.dismissProgressLoader(progressDialog);

                if (status.equalsIgnoreCase(Constants.ADD_TO_FAVOURITES)) {
                    wishListButton.setChecked(true);
                } else if (status.equalsIgnoreCase(Constants.REMOVE_FROM_FAVOURITES)) {
                    wishListButton.setChecked(false);
                }
            }

            @Override
            public void onFavouritesFailed(String errorType, String error) {
                MyUtils.dismissProgressLoader(progressDialog);
                showBaseRelative(errorType, error);
                if (state) {
                    wishListButton.setChecked(true);
                } else {
                    wishListButton.setChecked(false);
                }
            }
        });
        favouritesState.addToFavourites(postId, accessToken);
    }


    private void intentToCategoryListingPreview(Bundle bundle) {
        Intent intent = new Intent(context, ListingDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, MyListingActivity.this);
    }


}
