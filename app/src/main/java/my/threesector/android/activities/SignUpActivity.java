package my.threesector.android.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.kofigyan.stateprogressbar.StateProgressBar;

import my.threesector.android.R;
import my.threesector.android.fragments.sign_up.CompanyFragment;
import my.threesector.android.fragments.sign_up.ProfileFragment;
import my.threesector.android.fragments.sign_up.SignUpFragment;
import my.threesector.android.interfaces.SignUpModeListener;
import my.threesector.android.interfaces.SignUpNavigationListener;
import my.threesector.android.utils.MyUtils;

public class SignUpActivity extends AppCompatActivity {

    private Context context;
    private StateProgressBar stateprogressbar;
    protected String[] merchantDescription = {"ACCOUNT", "COMPANY", "PROFILE"};
    protected String[] freeSignUpDescription = {"ACCOUNT", "PROFILE"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        context = SignUpActivity.this;

        stateprogressbar = (StateProgressBar) findViewById(R.id.usage_stateprogressbar);

        listeners();

    }

    private void listeners() {

        setUpStepCounterForMerchant();


        commitSignUpFragment();

    }

    //=============COMMIT SIGN UP FRAGMENT==========================================================
    private void commitSignUpFragment() {

        SignUpModeListener signUpModeListener = new SignUpModeListener() {
            @Override
            public void onMerchantRegistrationClicked() {
                setUpStepCounterForMerchant();
            }

            @Override
            public void onFreeSignUpClicked() {
                setUpStepCounterForFreeSignUp();
            }

            @Override
            public void onBackButtonClicked() {
                closePage();
            }

            @Override
            public void onNextButtonClicked(boolean isFreeSignUpProcess) {
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                if (isFreeSignUpProcess) {
                    commitProfileFragment();
                } else {
                    commitCompanyDetailsFragment();
                }
            }
        };


        Fragment fragment = new SignUpFragment(signUpModeListener);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame_signup, fragment);
        ft.commit();
    }

    //=============COMMIT COMPANY DETAILS FRAGMENT==========================================================
    private void commitCompanyDetailsFragment() {
        SignUpNavigationListener signUpNavigationListener = new SignUpNavigationListener() {
            @Override
            public void onBackButtonClicked() {
                closePage();
                //getFragmentManager().popBackStack();
                /*if (companyFragment != null) {
                    companyFragment.getFragmentManager().popBackStack();
                }*/

            }

            @Override
            public void onNextButtonClicked() {
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
                commitProfileFragment();
            }
        };

        CompanyFragment companyFragment = new CompanyFragment(signUpNavigationListener);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame_signup, companyFragment);

        ft.commit();
    }

    //=============COMMIT COMPANY DETAILS FRAGMENT==========================================================
    private void commitProfileFragment() {

        SignUpNavigationListener signUpNavigationListener = new SignUpNavigationListener() {
            @Override
            public void onBackButtonClicked() {
            }

            @Override
            public void onNextButtonClicked() {
                intentToHomePage();
            }
        };

        Fragment fragment = new ProfileFragment(signUpNavigationListener);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame_signup, fragment);
        ft.commit();
    }

    //================SETUP STEP-UP PROGRESS=================================================================
    private void setUpStepCounterForMerchant() {
        stateprogressbar.setMaxStateNumber(StateProgressBar.StateNumber.THREE);
        stateprogressbar.setStateDescriptionData(merchantDescription);
    }

    private void setUpStepCounterForFreeSignUp() {
        stateprogressbar.setMaxStateNumber(StateProgressBar.StateNumber.TWO);
        stateprogressbar.setStateDescriptionData(freeSignUpDescription);
    }


    private void intentToHomePage() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, SignUpActivity.this);
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, SignUpActivity.this);
    }


}
