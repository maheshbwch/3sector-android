package my.threesector.android.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.listing.CategoryListingActivity;
import my.threesector.android.activities.listing.ListingDetailActivity;
import my.threesector.android.adapters.ListingAdapter;
import my.threesector.android.adapters.TagsAdapter;
import my.threesector.android.interfaces.FavouritesListener;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.interfaces.OnItemClicked;
import my.threesector.android.models.CategoryListing;
import my.threesector.android.models.SearchFieldListing;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.FavouritesState;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.ParseListing;
import my.threesector.android.utils.custom_spinner.OnItemSelected;
import my.threesector.android.utils.custom_spinner.SpinnerDialog;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends BaseActivity {

    private Context context;
    private ImageView backImage;
    private LinearLayout fieldListingLinear, regionLinear, categoryLinear;
    private TextView categoryTxt, regionTxt;
    private RecyclerView tagsRecycler, tagsResultRecycler;
    private ProgressBar progressBar, listingProgressBar;
    private ProgressDialog progressDialog = null;

    private ArrayList<SearchFieldListing> regionArrayList = new ArrayList<>();
    private ArrayList<SearchFieldListing> categoryArrayList = new ArrayList<>();
    private ArrayList<SearchFieldListing> tagsArrayList = new ArrayList<>();

    private ArrayList<CategoryListing> tagsResultArrayList = new ArrayList<>();
    private ListingAdapter tagsResultListAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        context = SearchActivity.this;

        backImage = findViewById(R.id.as_backImage);

        fieldListingLinear = findViewById(R.id.as_fieldListingLinear);

        regionLinear = findViewById(R.id.as_regionLinear);
        regionTxt = findViewById(R.id.as_regionTxt);

        categoryLinear = findViewById(R.id.as_categoryLinear);
        categoryTxt = findViewById(R.id.as_categoryTxt);

        tagsRecycler = findViewById(R.id.as_tagsRecycler);
        tagsResultRecycler = findViewById(R.id.as_tagsResultRecycler);
        progressBar = findViewById(R.id.as_progressBar);
        listingProgressBar = findViewById(R.id.as_listingProgressBar);

        listeners();
    }

    private void listeners() {
        hideToolBar();

        tagsRecycler.setLayoutManager(new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.HORIZONTAL));
        tagsRecycler.setHasFixedSize(true);

        tagsResultRecycler.setLayoutManager(new LinearLayoutManager(context));
        tagsResultRecycler.setHasFixedSize(true);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        getSearchFieldsListing();
    }


    private void getSearchFieldsListing() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getSearchFieldListing();
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        String jsonResponse = response.body();
                        Log.e("jsonResponse", ":" + jsonResponse);
                        if (jsonResponse != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonResponse);

                                String status = jsonObject.optString("status");

                                if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONArray jsonArray = jsonObject.optJSONArray("oResults");

                                    if (jsonArray != null && jsonArray.length() > 3) {


                                        JSONArray regionArray = jsonArray.optJSONObject(3).optJSONArray("options");
                                        JSONArray categoryArray = jsonArray.optJSONObject(4).optJSONArray("options");
                                        JSONArray tagsArray = jsonArray.optJSONObject(5).optJSONArray("options");

                                        if (regionArray != null && regionArray.length() > 0) {
                                            regionArrayList.clear();
                                            for (int i = 0; i < regionArray.length(); i++) {
                                                String name = regionArray.optJSONObject(i).optString("name");
                                                String id = regionArray.optJSONObject(i).optString("id");

                                                regionArrayList.add(new SearchFieldListing(name, id));
                                            }
                                            loadRegionValues(regionArrayList);
                                        }

                                        if (categoryArray != null && categoryArray.length() > 0) {
                                            categoryArrayList.clear();
                                            for (int i = 0; i < categoryArray.length(); i++) {
                                                String name = categoryArray.optJSONObject(i).optString("name");
                                                String id = categoryArray.optJSONObject(i).optString("id");

                                                categoryArrayList.add(new SearchFieldListing(name, id));
                                            }
                                            loadCategoryValues(categoryArrayList);
                                        }


                                        if (tagsArray != null && tagsArray.length() > 0) {
                                            tagsArrayList.clear();
                                            for (int i = 0; i < tagsArray.length(); i++) {
                                                String name = tagsArray.optJSONObject(i).optString("name");
                                                String id = tagsArray.optJSONObject(i).optString("id");
                                                tagsArrayList.add(new SearchFieldListing(name, id));
                                            }
                                            loadTagsToRecycler(tagsArrayList);
                                        }

                                        enableFieldsLinear(true);

                                    }


                                } else {
                                    Log.e("error", "error response");
                                    enableFieldsLinear(false);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("error", "null response");
                            enableFieldsLinear(false);
                        }
                    } else {
                        Log.e("error", "response un successful");
                        enableFieldsLinear(false);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("error", "message:" + t.getMessage());
                    enableFieldsLinear(false);
                }
            });
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            Log.e("exp", ":" + e.getMessage());
            enableFieldsLinear(false);
        }
    }


    private void enableFieldsLinear(boolean state) {
        if (state) {
            if (fieldListingLinear.getVisibility() == View.GONE) {
                fieldListingLinear.setVisibility(View.VISIBLE);
            }
        } else {
            if (fieldListingLinear.getVisibility() == View.VISIBLE) {
                fieldListingLinear.setVisibility(View.GONE);
            }
        }
    }

    //========Search by Region==============================================

    private void loadRegionValues(ArrayList<SearchFieldListing> regionArrayList) {
        regionLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpinnerDialog spinnerDialog = new SpinnerDialog(regionArrayList, new OnItemSelected() {
                    @Override
                    public void onItemSelected(int position) {
                        String regionId = regionArrayList.get(position).getId();
                        String regionName = regionArrayList.get(position).getName();
                        regionTxt.setText(regionName);

                        resetTagsResultsList();

                        getTagsAndRegionResult(false, regionId);
                    }
                });
                spinnerDialog.setTitle("Select Region");
                spinnerDialog.show(getSupportFragmentManager(), "dialog");
            }
        });
    }

    //========Search by Category==============================================

    private void loadCategoryValues(ArrayList<SearchFieldListing> categoryArrayList) {
        categoryLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SpinnerDialog spinnerDialog = new SpinnerDialog(categoryArrayList, new OnItemSelected() {
                    @Override
                    public void onItemSelected(int position) {
                        String categoryId = categoryArrayList.get(position).getId();
                        String categoryName = categoryArrayList.get(position).getName();
                        //categoryTxt.setText(categoryName);

                        Bundle bundle = new Bundle();
                        bundle.putString("termId", categoryId);
                        bundle.putString("name", categoryName);

                        intentToCategoryListing(bundle);
                    }
                });
                spinnerDialog.setTitle("Select Category");
                spinnerDialog.show(getSupportFragmentManager(), "dialog");
            }
        });
    }


    private void intentToCategoryListing(Bundle bundle) {
        Intent intent = new Intent(context, CategoryListingActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, SearchActivity.this);
    }

    //========Adapt Results==============================================

    private void loadTagsToRecycler(ArrayList<SearchFieldListing> tagsArrayList) {
        if (tagsArrayList.size() > 0) {
            TagsAdapter myAdapter = new TagsAdapter(context, tagsArrayList);
            myAdapter.setOnItemClicked(new OnItemClicked() {
                @Override
                public void onItemClicked(int position) {

                    Log.e("position", ":" + position);

                    resetTagsResultsList();

                    String tagId = tagsArrayList.get(position).getId();
                    getTagsAndRegionResult(true, tagId);

                }
            });
            tagsRecycler.setAdapter(myAdapter);
        }
    }


    //=======Tags result load to recycler view=========================================

    private void getTagsAndRegionResult(boolean isFromTags, String id) {
        try {
            listingProgressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = isFromTags ? apiService.getTagsListing(id) : apiService.getRegionListing(id);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    listingProgressBar.setVisibility(View.GONE);
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);

                    //String categoriesHome = response.body();
                    if (jsonResponse != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                JSONArray jsonArray = jsonObject.optJSONArray("aListings");

                                tagsResultArrayList.clear();
                                tagsResultArrayList.addAll(ParseListing.getCategoryListingArrayList(jsonArray));

                                adaptTagsResultsList();

                            } else {
                                Log.e("error", "error response");
                                resetTagsResultsList();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            resetTagsResultsList();
                        }
                    } else {
                        Log.e("error", "null response");
                        resetTagsResultsList();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    listingProgressBar.setVisibility(View.GONE);
                    Log.e("error", "message:" + t.getMessage());
                    resetTagsResultsList();
                }
            });
        } catch (Exception e) {
            listingProgressBar.setVisibility(View.GONE);
            Log.e("exp", ":" + e.getMessage());
            resetTagsResultsList();
        }
    }

    private void adaptTagsResultsList() {
        if (tagsResultArrayList.size() > 0) {

            tagsResultListAdapter = new ListingAdapter(context, tagsResultArrayList);
            tagsResultListAdapter.setListingClickListeners(new ListingClickListeners() {
                @Override
                public void onItemClicked(int position) {
                    String listingId = "" + tagsResultArrayList.get(position).getListingId();
                    String listingTitle = "" + tagsResultArrayList.get(position).getPostTitle();
                    String logo = "" + tagsResultArrayList.get(position).getLogo();
                    String coverImage = "" + tagsResultArrayList.get(position).getCoverImg();

                    Bundle bundle = new Bundle();
                    bundle.putString("listingId", listingId);
                    bundle.putString("listingTitle", listingTitle);
                    bundle.putString("logo", logo);
                    bundle.putString("coverImage", coverImage);

                    intentToCategoryListingPreview(bundle);
                }

                @Override
                public void onFavouritesClicked(int position, SparkButton wishListButton) {
                    String postId = "" + tagsResultArrayList.get(position).getListingId();

                    addToFavorites(postId, wishListButton);
                }

                @Override
                public void onCallClicked(String phoneNumber) {
                    listingPhoneNumber = phoneNumber;
                    callPhoneNumber();
                }
            });
            tagsResultRecycler.setAdapter(tagsResultListAdapter);
        } else {
            resetTagsResultsList();
        }
    }

    private void resetTagsResultsList() {
        if (tagsResultListAdapter != null) {
            tagsResultArrayList.clear();
            tagsResultListAdapter.notifyDataSetChanged();
        }
    }


    private void addToFavorites(String postId, SparkButton wishListButton) {
        boolean state = wishListButton.isChecked();
        progressDialog = MyUtils.showProgressLoader(context, (state ? "Removing.." : "Adding.."));
        FavouritesState favouritesState = new FavouritesState(state);
        favouritesState.setFavouritesListener(new FavouritesListener() {
            @Override
            public void onFavouritesSuccess(String status) {
                MyUtils.dismissProgressLoader(progressDialog);

                if (status.equalsIgnoreCase(Constants.ADD_TO_FAVOURITES)) {
                    wishListButton.setChecked(true);
                } else if (status.equalsIgnoreCase(Constants.REMOVE_FROM_FAVOURITES)) {
                    wishListButton.setChecked(false);
                }
            }

            @Override
            public void onFavouritesFailed(String errorType, String error) {
                MyUtils.dismissProgressLoader(progressDialog);
                showBaseRelative(errorType, error);
                if (state) {
                    wishListButton.setChecked(true);
                } else {
                    wishListButton.setChecked(false);
                }
            }
        });
        favouritesState.addToFavourites(postId, accessToken);
    }


    private void intentToCategoryListingPreview(Bundle bundle) {
        Intent intent = new Intent(context, ListingDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, SearchActivity.this);
    }


}
