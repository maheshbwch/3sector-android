package my.threesector.android.activities.account;

import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {

    private ImageView coverImageView;
    private CircleImageView profileImageView;
    private TextInputLayout firstNameTil, lastNameTil, displayNameTil, positionNameTil;
    private TextInputEditText firstNameEdt, lastNameEdt, displayNameEdt, positionNameEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        coverImageView = findViewById(R.id.aup_coverImageView);
        profileImageView = findViewById(R.id.aup_profileImageView);

        firstNameTil = findViewById(R.id.aup_firstNameTil);
        firstNameEdt = findViewById(R.id.aup_firstNameEdt);

        lastNameTil = findViewById(R.id.aup_lastNameTil);
        lastNameEdt = findViewById(R.id.aup_lastNameEdt);

        displayNameTil = findViewById(R.id.aup_displayNameTil);
        displayNameEdt = findViewById(R.id.aup_displayNameEdt);

        positionNameTil = findViewById(R.id.aup_positionNameTil);
        positionNameEdt = findViewById(R.id.aup_positionNameEdt);

        listeners();
    }

    private void listeners() {
        setTitleTxt("Profile");

        Typeface typeface = MyUtils.getRegularFont(context);
        firstNameTil.setTypeface(typeface);
        firstNameEdt.setTypeface(typeface);

        lastNameTil.setTypeface(typeface);
        lastNameEdt.setTypeface(typeface);

        displayNameTil.setTypeface(typeface);
        displayNameEdt.setTypeface(typeface);

        positionNameTil.setTypeface(typeface);
        positionNameEdt.setTypeface(typeface);

        getProfileDetails();
    }


    //=================Get user profile details===========================================================


    private void getProfileDetails() {
        try {
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getProfileDetails(Constants.BEARER + accessToken);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");
                            String msg = jsonObject.optString("msg");

                            if (MyUtils.checkStringValue(status)) {

                                if (status.equalsIgnoreCase(Constants.API_SUCCESS)) {


                                    JSONObject jsonObject1 = jsonObject.optJSONObject("oResults");
                                    if (jsonObject1 != null) {
                                        JSONObject basicInfoObject = jsonObject1.optJSONObject("oBasicInfo");

                                        if (basicInfoObject != null) {
                                            String first_name = basicInfoObject.optString("first_name");
                                            String last_name = basicInfoObject.optString("last_name");
                                            String displayName = basicInfoObject.optString("display_name");
                                            String profileImage = basicInfoObject.optString("avatar");
                                            String coverImage = basicInfoObject.optString("cover_image");
                                            String position = basicInfoObject.optString("position");

                                            Log.e("displayName", ":" + displayName);


                                            firstNameEdt.setText(MyUtils.checkStringValue(first_name) ? first_name : "");
                                            lastNameEdt.setText(MyUtils.checkStringValue(last_name) ? last_name : "");
                                            displayNameEdt.setText(MyUtils.checkStringValue(displayName) ? displayName : "");
                                            positionNameEdt.setText(MyUtils.checkStringValue(position) ? position : "");
                                            if (MyUtils.checkStringValue(profileImage)) {
                                                Glide.with(context).load(profileImage).placeholder(R.drawable.user_pic).into(profileImageView);
                                            }

                                            if (MyUtils.checkStringValue(coverImage)) {
                                                Glide.with(context).load(coverImage).into(coverImageView);
                                            }
                                        }

                                    }


                                } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {
                                    if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.USER_NOT_FOUND)) {
                                        //TODO HANDLE TOKEN
                                        showBaseRelative(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                    }
                                }
                            }

                            //{"status": "error","msg": "foundNoUser"}

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("error", "null response");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
        }
    }


}
