package my.threesector.android.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import my.threesector.android.R;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PermissionAlert;
import my.threesector.android.utils.PermissionRequest;
import my.threesector.android.utils.PreferenceHandler;

public class BaseActivity extends AppCompatActivity {

    public Context context;
    public TextView titleTxt;
    public ImageView backImage;

    public RelativeLayout toolBarRelative, baseRelative;
    public ImageView errorBackIconImage, errorTypeIcon;
    public TextView errorTxt, errorCloseTxt;

    public LinearLayout bottomNavigationLinear;
    public BottomNavigationView bottomNavigation;

    public String accessToken = "";
    public String listingPhoneNumber = "";

    @Override
    public void setContentView(int layoutResID) {
        LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        FrameLayout activityContainer = (FrameLayout) linearLayout.findViewById(R.id.ba_activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        context = BaseActivity.this;

        toolBarRelative = linearLayout.findViewById(R.id.ba_toolBarRelative);
        titleTxt = linearLayout.findViewById(R.id.ba_titleTxt);
        backImage = linearLayout.findViewById(R.id.ba_backImage);

        bottomNavigationLinear = linearLayout.findViewById(R.id.ba_bottomNavigationLinear);
        bottomNavigation = linearLayout.findViewById(R.id.ba_bottomNavigation);

        baseRelative = linearLayout.findViewById(R.id.ba_errorRelative);
        errorBackIconImage = linearLayout.findViewById(R.id.ba_errorBackIconImage);
        errorTypeIcon = linearLayout.findViewById(R.id.ba_errorTypeIcon);
        errorTxt = linearLayout.findViewById(R.id.ba_errorTxt);
        errorCloseTxt = linearLayout.findViewById(R.id.ba_errorCloseTxt);

        clickListeners();

        super.setContentView(linearLayout);
    }

    private void clickListeners() {
        accessToken = PreferenceHandler.getPreferenceFromString(context, Constants.ACCESS_TOKEN);
        Log.e("accessToken", ":" + accessToken);


        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        errorBackIconImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });


        errorCloseTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        baseRelative.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }


    public void setTitleTxt(String title) {
        MyUtils.viewHtmlTxt(title, titleTxt);
    }


    public void showBaseRelative(String type, String content) {
        if (baseRelative.getVisibility() == View.GONE) {
            baseRelative.setVisibility(View.VISIBLE);
        }
        switch (type) {
            case Constants.NO_INTERNET:
                errorTypeIcon.setImageResource(R.drawable.ic_no_internet);
                break;
            case Constants.ERROR_RESPONSE:
                errorTypeIcon.setImageResource(R.drawable.ic_sad_smiley1);
                break;
            case Constants.TOKEN_SESSION_EXPIRED:
                errorTypeIcon.setImageResource(R.drawable.ic_sad_smiley);
                break;
            default:
                break;
        }
        errorTxt.setText(content);
    }


    public void hideBaseRelative() {
        if (baseRelative.getVisibility() == View.VISIBLE) {
            baseRelative.setVisibility(View.GONE);
        }
    }


    public void hideToolBar() {
        if (toolBarRelative.getVisibility() == View.VISIBLE) {
            toolBarRelative.setVisibility(View.GONE);
        }
    }

    public void showBottomNavigationView() {
        if (bottomNavigationLinear.getVisibility() == View.GONE) {
            bottomNavigationLinear.setVisibility(View.VISIBLE);
        }
    }

    public void callPhoneNumber() {
        if (PermissionRequest.askForActivityPermission(Manifest.permission.CALL_PHONE, Constants.CALL_PHONE, BaseActivity.this)) {
            MyUtils.openCallIntent(context, listingPhoneNumber);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    MyUtils.openCallIntent(context, listingPhoneNumber);
                    return;
                } else {
                    if (context == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, Manifest.permission.CALL_PHONE)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(context, "PHONE");
                        permissionAlert.show();
                    }
                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        closePage();
    }

    public void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, BaseActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume", "base activity");
    }
}