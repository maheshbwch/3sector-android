package my.threesector.android.activities.listing;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.kofigyan.stateprogressbar.StateProgressBar;

import my.threesector.android.R;
import my.threesector.android.fragments.sign_up.CompanyFragment;
import my.threesector.android.fragments.sign_up.ProfileFragment;
import my.threesector.android.interfaces.SignUpNavigationListener;
import my.threesector.android.utils.MyUtils;

public class AddCompanyListing extends AppCompatActivity {

    private Context context;
    private StateProgressBar stateprogressbar;
    protected String[] merchantDescription = {"COMPANY", "PROFILE"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_add);
        context = AddCompanyListing.this;

        stateprogressbar = (StateProgressBar) findViewById(R.id.acl_stateProgressBar);

        listeners();

    }

    private void listeners() {

        setUpStepCounterForMerchant();


        commitCompanyDetailsFragment();

    }


    //=============COMMIT COMPANY DETAILS FRAGMENT==========================================================
    private void commitCompanyDetailsFragment() {
        SignUpNavigationListener signUpNavigationListener = new SignUpNavigationListener() {
            @Override
            public void onBackButtonClicked() {
                closePage();
                //getFragmentManager().popBackStack();
                /*if (companyFragment != null) {
                    companyFragment.getFragmentManager().popBackStack();
                }*/

            }

            @Override
            public void onNextButtonClicked() {
                stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
                commitProfileFragment();
            }
        };

        CompanyFragment companyFragment = new CompanyFragment(signUpNavigationListener);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame_add_listing, companyFragment);

        ft.commit();
    }

    //=============COMMIT COMPANY DETAILS FRAGMENT==========================================================
    private void commitProfileFragment() {

        SignUpNavigationListener signUpNavigationListener = new SignUpNavigationListener() {
            @Override
            public void onBackButtonClicked() {
            }

            @Override
            public void onNextButtonClicked() {
                finish();
            }
        };

        Fragment fragment = new ProfileFragment(signUpNavigationListener);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame_add_listing, fragment);
        ft.commit();
    }

    //================SETUP STEP-UP PROGRESS=================================================================
    private void setUpStepCounterForMerchant() {
        stateprogressbar.setMaxStateNumber(StateProgressBar.StateNumber.TWO);
        stateprogressbar.setStateDescriptionData(merchantDescription);
    }

    @Override
    public void onBackPressed() {
        closePage();
    }

    private void closePage() {
        finish();
        MyUtils.openOverrideAnimation(false, AddCompanyListing.this);
    }


}
