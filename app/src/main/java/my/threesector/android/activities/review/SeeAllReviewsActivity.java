package my.threesector.android.activities.review;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.adapters.ReviewsListAdapter;
import my.threesector.android.models.Reviews;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeeAllReviewsActivity extends BaseActivity {


    private Context context;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ArrayList<Reviews> reviewsArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_reviews);
        context = SeeAllReviewsActivity.this;

        recyclerView = findViewById(R.id.alr_recyclerView);
        progressBar = findViewById(R.id.alr_categoryProgress);

        Bundle bundle = getIntent().getExtras();
        listeners(bundle);
    }

    private void listeners(Bundle bundle) {
        setTitleTxt("All Reviews");

        if (bundle != null) {
            String listingId = bundle.getString("listingId");
            getAllReviewsList(listingId);
        }
    }


    private void getAllReviewsList(String listingId) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getListingReview(listingId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    progressBar.setVisibility(View.GONE);
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);

                    if (jsonResponse != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            JSONArray jsonArray = jsonObject.optJSONArray("aReviews");

                            if (jsonArray != null && jsonArray.length() > 0) {

                                reviewsArrayList.clear();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String iconUrl = null;
                                    String userProfileUrl = null;
                                    String userProfileName = null;
                                    double productQualityRating = 0.0;
                                    double overAllQualityRating = 0.0;


                                    String reviewId = jsonArray.optJSONObject(i).optString("ID");
                                    String postTitle = jsonArray.optJSONObject(i).optString("postTitle");
                                    String postContent = jsonArray.optJSONObject(i).optString("postContent");
                                    String postDate = jsonArray.optJSONObject(i).optString("postDate");

                                    JSONObject galleryObject = jsonArray.optJSONObject(i).optJSONObject("oGallery");
                                    if (galleryObject != null) {
                                        JSONArray imagesArray = galleryObject.optJSONArray("large");

                                        if (imagesArray != null && imagesArray.length() > 0) {
                                            iconUrl = imagesArray.optJSONObject(0).optString("url");
                                        }
                                    }

                                    JSONObject reviewObject = jsonArray.optJSONObject(i).optJSONObject("oReviews");
                                    if (reviewObject != null) {
                                        JSONArray reviewArray = reviewObject.optJSONArray("oDetails");

                                        if (reviewArray != null && reviewArray.length() > 1) {
                                            productQualityRating = reviewArray.optJSONObject(0).optDouble("score");
                                            overAllQualityRating = reviewArray.optJSONObject(1).optDouble("score");
                                        }
                                    }

                                    JSONObject userInfoObject = jsonArray.optJSONObject(i).optJSONObject("oUserInfo");
                                    if (userInfoObject != null) {
                                        userProfileUrl = userInfoObject.optString("avatar");
                                        userProfileName = userInfoObject.optString("displayName");
                                    }

                                    reviewsArrayList.add(new Reviews(reviewId, userProfileUrl, userProfileName, postTitle, postContent, postDate, productQualityRating, overAllQualityRating));
                                }
                            } else {
                                showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ERROR_REVIEW_DETAIL);
                            }

                            adaptReviewsList();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ERROR_REVIEW_DETAIL);
                        }
                    } else {
                        Log.e("error", "null response");
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ERROR_REVIEW_DETAIL);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    progressBar.setVisibility(View.GONE);
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        showBaseRelative(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    } else {
                        Log.e("error", "message other issue:" + t.getMessage());
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ERROR_REVIEW_DETAIL);
                    }
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
            progressBar.setVisibility(View.GONE);
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ERROR_REVIEW_DETAIL);
        }
    }

    private void adaptReviewsList() {
        if (reviewsArrayList.size() > 0) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);

            ReviewsListAdapter myAdapter = new ReviewsListAdapter(context, reviewsArrayList);
            recyclerView.setAdapter(myAdapter);
        } else {
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ERROR_REVIEW_DETAIL);
        }
    }

}
