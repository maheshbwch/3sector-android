package my.threesector.android.activities.webview;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PreferenceHandler;
import my.threesector.android.webservice.ApiConstants;

public class BecomeMerchant extends BaseActivity {

    private Context context = null;
    private WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_listing);
        context = BecomeMerchant.this;

        webView = findViewById(R.id.ali_webView);

        listeners();
    }


    private void listeners() {
        setTitleTxt("Become an Author");

        String loginId = "" + PreferenceHandler.getPreferenceFromString(context, Constants.LOGIN_ID);
        String loginPassword = PreferenceHandler.getPreferenceFromString(context, Constants.LOGIN_PASSWORD);

        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAppCacheEnabled(false);
        //webView.addJavascriptInterface(new WebAppInterface(this), "Android");
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        webView.setWebViewClient(new MyWebClient());

        //webView.loadUrl(ApiConstants.AUTO_LOGIN_WEB_VIEW_URL);
        webView.loadUrl(ApiConstants.AUTO_LOGIN_WEB_VIEW_URL + "?ZvhxhcCelG=" + loginId + "&opEZBQxPYR=" + loginPassword);

        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity(Constants.BECOME_A_MERCHANT_RESULT);
            }
        });
    }


    public class MyWebClient extends WebViewClient {

        private ProgressDialog progressDialog = null;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressDialog = MyUtils.showProgressLoader(context, "Loading..");
            Log.e("onPageStarted", ":" + url);

            if (url.equalsIgnoreCase(ApiConstants.ADD_LISTING_WEB_VIEW_URL)){
                closeActivity(Constants.BECOME_A_MERCHANT_SUCCESS_RESULT);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.e("overRideUrl", ":" + url);
            return true;

        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
            Log.e("onPageCommit", ":" + url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.e("onPageFinished", ":" + url);
            MyUtils.dismissProgressLoader(progressDialog);

            if (url.equalsIgnoreCase(ApiConstants.BASE_URL)) {
                webView.loadUrl(ApiConstants.BECOME_MERCHANT_WEB_VIEW_URL);
            }
        }
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            MyUtils.dismissProgressLoader(progressDialog);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        } else {
            closePage();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        closeActivity(Constants.BECOME_A_MERCHANT_RESULT);
    }

    private void closeActivity(int code) {
       /* if (webView.canGoBack()) {
            webView.goBack();
        } else {
            closePage();
        }*/
        clearCookies(context);
        setResult(code);
        closePage();
    }


    public void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Log.e("log", "Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            Log.e("log", "Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

}
