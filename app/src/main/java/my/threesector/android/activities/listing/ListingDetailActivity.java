package my.threesector.android.activities.listing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.activities.chat.ConversationActivity;
import my.threesector.android.activities.review.AddReviewActivity;
import my.threesector.android.activities.review.SeeAllReviewsActivity;
import my.threesector.android.adapters.ReviewsListAdapter;
import my.threesector.android.interfaces.ChatUserListener;
import my.threesector.android.interfaces.FavouritesListener;
import my.threesector.android.models.Reviews;
import my.threesector.android.utils.AddChatUser;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.FavouritesState;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PreferenceHandler;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListingDetailActivity extends BaseActivity {

    private Context context;
    private TextView listingNameTxt, ratingNumberTxt, phoneNumberTxt, categoryNameTxt, aboutCompanyTxt, businessHoursTxt, addressTxt;
    private MaterialCardView phoneNumberCard;
    private ImageView coverImageView, listingProfileImage;
    private RatingBar ratingBar;


    private LinearLayout wishListLinear, messageLinear;
    private SparkButton wishListButton;

    private RecyclerView recyclerView;
    private LinearLayout reviewLinear, seeAllReviewLinear, writeReviewLinear;
    private ArrayList<Reviews> reviewsArrayList = new ArrayList<>();
    private ProgressDialog progressDialog = null;

    private DatabaseReference rootReference, usersReference;

    private String listingId = "";

    private String senderId = "";
    private String senderName = "";
    private String senderImageUrl = "";

    private String receiverId = "";
    private String receiverName = "";
    private String receiverImageUrl = "";

   /*private String senderId = "11";
    private String senderName = "GunaSekar";
    private String senderImageUrl = "https://image.freepik.com/free-vector/businessman-profile-cartoon_18591-58479.jpg";

    private String receiverId = "22";
    private String receiverName = "Srinivas";
    private String receiverImageUrl = "https://sm.askmen.com/askmen_in/article/f/facebook-p/facebook-profile-picture-affects-chances-of-gettin_gstt.jpg";*/

    /*private String senderId = "22";
    private String senderName = "Srinivas";
    private String senderImageUrl = "https://sm.askmen.com/askmen_in/article/f/facebook-p/facebook-profile-picture-affects-chances-of-gettin_gstt.jpg";

    private String receiverId = "11";
    private String receiverName = "GunaSekar";
    private String receiverImageUrl = "https://image.freepik.com/free-vector/businessman-profile-cartoon_18591-58479.jpg";*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_detail);
        context = ListingDetailActivity.this;

        listingNameTxt = findViewById(R.id.ald_listingNameTxt);
        coverImageView = findViewById(R.id.ald_coverImageView);
        listingProfileImage = findViewById(R.id.ald_listingProfileImage);

        ratingBar = findViewById(R.id.ald_ratingBar);
        ratingNumberTxt = findViewById(R.id.ald_ratingNumberTxt);

        wishListLinear = findViewById(R.id.ald_wishListLinear);
        wishListButton = findViewById(R.id.ald_wishListButton);

        messageLinear = findViewById(R.id.ald_messageLinear);

        phoneNumberCard = findViewById(R.id.ald_phoneNumberCard);
        phoneNumberTxt = findViewById(R.id.ald_phoneNumberTxt);
        categoryNameTxt = findViewById(R.id.ald_categoryNameTxt);
        aboutCompanyTxt = findViewById(R.id.ald_aboutCompanyTxt);
        businessHoursTxt = findViewById(R.id.ald_businessHoursTxt);
        addressTxt = findViewById(R.id.ald_addressTxt);

        reviewLinear = findViewById(R.id.ald_reviewLinear);
        recyclerView = findViewById(R.id.ald_recyclerView);
        seeAllReviewLinear = findViewById(R.id.ald_seeAllReviewLinear);
        writeReviewLinear = findViewById(R.id.ald_writeReviewLinear);

        Bundle bundle = getIntent().getExtras();
        listeners(bundle);

    }

    private void listeners(Bundle bundle) {

        if (bundle != null) {
            listingId = bundle.getString("listingId");
            String title = bundle.getString("listingTitle");
            String logo = bundle.getString("logo");
            String coverImage = bundle.getString("coverImage");

            Log.e("data", ":" + listingId + " - " + title);

            setTitleTxt(title);

            MyUtils.viewHtmlTxt(title, listingNameTxt);

            if (MyUtils.checkStringValue(coverImage)) {
                Glide.with(context).load(coverImage).into(coverImageView);
            }

            if (MyUtils.checkStringValue(logo)) {
                Glide.with(context).load(logo).placeholder(R.mipmap.app_logo).into(listingProfileImage);
            }

            if (MyUtils.checkStringValue(listingId)) {
                getCategoryListingDetails(listingId);

                getReviews(listingId);
            }

            senderId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);
            senderName = PreferenceHandler.getPreferenceFromString(context, Constants.USER_NAME);
            senderImageUrl = PreferenceHandler.getPreferenceFromString(context, Constants.USER_PROFILE_PICTURE);

            rootReference = FirebaseDatabase.getInstance().getReference();
            usersReference = FirebaseDatabase.getInstance().getReference().child(Constants.FDB_USERS);


        }

        adaptReviewsList();

        seeAllReviewLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intentToSeeAllReviewPage();
            }
        });

        writeReviewLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intentToAddReviewPage();

                /*ReviewDialog reviewDialog = new ReviewDialog(context, new ReviewInterface() {
                    @Override
                    public void onReviewSubmitted() {

                    }
                });
                reviewDialog.show(getSupportFragmentManager(), "dialog");*/

            }
        });


        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

    }


    private void getCategoryListingDetails(String listingId) {
        try {
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getCategoryListingDetails(listingId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        String jsonResponse = response.body();
                        Log.e("jsonResponse", ":" + jsonResponse);

                        if (jsonResponse != null) {

                            try {
                                JSONObject jsonObject = new JSONObject(jsonResponse);

                                String status = jsonObject.optString("status");

                                if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONObject resultObject = jsonObject.optJSONObject("oResults");

                                    if (resultObject != null) {

                                        String tagLine = resultObject.optString("tagLine");
                                        String phone = resultObject.optString("phone");

                                        if (MyUtils.checkStringValue(phone)) {
                                            phoneNumberTxt.setText(phone);
                                            phoneNumberTxt.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    listingPhoneNumber = phone;
                                                    callPhoneNumber();
                                                }
                                            });
                                        }

                                        aboutCompanyTxt.setText(MyUtils.checkStringValue(tagLine) ? tagLine : "-");

                                        JSONObject addressObject = resultObject.optJSONObject("oAddress");
                                        if (addressObject != null) {
                                            String address = addressObject.optString("address");
                                            addressTxt.setText(MyUtils.checkStringValue(address) ? address : "-");
                                        }


                                        JSONObject reviewObject = resultObject.optJSONObject("oReview");
                                        if (reviewObject != null) {
                                            int mode = reviewObject.optInt("mode");
                                            int average = reviewObject.optInt("average");

                                            ratingBar.setRating((float) average);
                                            ratingNumberTxt.setText("" + average + "/" + mode);
                                        }

                                        JSONObject authorObject = resultObject.optJSONObject("oAuthor");
                                        if (authorObject != null) {
                                            receiverId = authorObject.optString("ID");
                                            receiverName = authorObject.optString("displayName");
                                            receiverImageUrl = authorObject.optString("avatar");


                                            if ((senderId != null && receiverId != null) && !senderId.equalsIgnoreCase(receiverId)) {
                                                messageLinear.setAlpha(1);

                                                messageLinear.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        progressDialog = MyUtils.showProgressLoader(context, "Loading..");
                                                        AddChatUser addChatUser = new AddChatUser(rootReference, senderId, senderName, senderImageUrl, receiverId, receiverName, receiverImageUrl);
                                                        addChatUser.setChatUserListener(new ChatUserListener() {
                                                            @Override
                                                            public void onUserCreated() {
                                                                MyUtils.dismissProgressLoader(progressDialog);

                                                                DatabaseReference databaseReference = rootReference.child(Constants.FDB_FRIENDS).child(senderId).child(receiverId);
                                                                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        String message_key = dataSnapshot.child("message_key").getValue(String.class);

                                                                        Log.e("message_key", ":" + message_key);

                                                                        Bundle bundle = new Bundle();
                                                                        bundle.putString("senderId", senderId);
                                                                        bundle.putString("senderName", senderName);
                                                                        bundle.putString("senderImage", senderImageUrl);
                                                                        bundle.putString("receiverId", receiverId);
                                                                        bundle.putString("receiverName", receiverName);
                                                                        bundle.putString("receiverImage", receiverImageUrl);
                                                                        bundle.putString("messageKey", message_key);

                                                                        intentToConversationActivity(bundle);
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                    }
                                                                });


                                                            }

                                                            @Override
                                                            public void onFailed() {
                                                                MyUtils.dismissProgressLoader(progressDialog);
                                                            }
                                                        });
                                                        addChatUser.fcmUserRegister();
                                                    }
                                                });

                                            } else {
                                                messageLinear.setAlpha((float) 0.5);
                                            }
                                        }


                                        JSONObject businessObject = resultObject.optJSONObject("businessStatus");
                                        if (businessObject != null) {
                                            String state = businessObject.optString("text");
                                            businessHoursTxt.setText(MyUtils.checkStringValue(state) ? state : "-");
                                        }

                                        JSONObject termObject = resultObject.optJSONObject("oTerm");
                                        if (termObject != null) {
                                            String name = termObject.optString("name");
                                            categoryNameTxt.setText(MyUtils.checkStringValue(name) ? name : "-");
                                        }


                                        wishListLinear.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                addToFavorites(listingId, wishListButton);
                                            }
                                        });

                                    }
                                } else {
                                    Log.e("error", "error response");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void intentToConversationActivity(Bundle bundle) {
        Intent intent = new Intent(context, ConversationActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, ListingDetailActivity.this);
    }


    //======Add product to wishlist=======================================


    private void addToFavorites(String postId, SparkButton wishListButton) {
        boolean state = wishListButton.isChecked();
        progressDialog = MyUtils.showProgressLoader(context, (state ? "Removing.." : "Adding.."));
        FavouritesState favouritesState = new FavouritesState(state);
        favouritesState.setFavouritesListener(new FavouritesListener() {
            @Override
            public void onFavouritesSuccess(String status) {
                MyUtils.dismissProgressLoader(progressDialog);

                if (status.equalsIgnoreCase(Constants.ADD_TO_FAVOURITES)) {
                    wishListButton.setChecked(true);
                } else if (status.equalsIgnoreCase(Constants.REMOVE_FROM_FAVOURITES)) {
                    wishListButton.setChecked(false);
                }
            }

            @Override
            public void onFavouritesFailed(String errorType, String error) {
                MyUtils.dismissProgressLoader(progressDialog);
                showBaseRelative(errorType, error);
                if (state) {
                    wishListButton.setChecked(true);
                } else {
                    wishListButton.setChecked(false);
                }
            }
        });
        favouritesState.addToFavourites(postId, accessToken);
    }


    //======Get Reviews===================================================


    private void getReviews(String listingId) {
        try {
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getListingReview(listingId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);

                    if (jsonResponse != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            JSONArray jsonArray = jsonObject.optJSONArray("aReviews");

                            if (jsonArray != null && jsonArray.length() > 0) {

                                reviewsArrayList.clear();
                                enableReviewLinear(true);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String iconUrl = null;
                                    String userProfileUrl = null;
                                    String userProfileName = null;
                                    double productQualityRating = 0.0;
                                    double overAllQualityRating = 0.0;


                                    String reviewId = jsonArray.optJSONObject(i).optString("ID");
                                    String postTitle = jsonArray.optJSONObject(i).optString("postTitle");
                                    String postContent = jsonArray.optJSONObject(i).optString("postContent");
                                    String postDate = jsonArray.optJSONObject(i).optString("postDate");

                                    JSONObject galleryObject = jsonArray.optJSONObject(i).optJSONObject("oGallery");
                                    if (galleryObject != null) {
                                        JSONArray imagesArray = galleryObject.optJSONArray("large");

                                        if (imagesArray != null && imagesArray.length() > 0) {
                                            iconUrl = imagesArray.optJSONObject(0).optString("url");
                                        }
                                    }

                                    JSONObject reviewObject = jsonArray.optJSONObject(i).optJSONObject("oReviews");
                                    if (reviewObject != null) {
                                        JSONArray reviewArray = reviewObject.optJSONArray("oDetails");

                                        if (reviewArray != null && reviewArray.length() > 1) {
                                            productQualityRating = reviewArray.optJSONObject(0).optDouble("score");
                                            overAllQualityRating = reviewArray.optJSONObject(1).optDouble("score");
                                        }
                                    }

                                    JSONObject userInfoObject = jsonArray.optJSONObject(i).optJSONObject("oUserInfo");
                                    if (userInfoObject != null) {
                                        userProfileUrl = userInfoObject.optString("avatar");
                                        userProfileName = userInfoObject.optString("displayName");
                                    }

                                    reviewsArrayList.add(new Reviews(reviewId, userProfileUrl, userProfileName, postTitle, postContent, postDate, productQualityRating, overAllQualityRating));
                                }
                            } else {
                                enableReviewLinear(false);
                            }

                            adaptReviewsList();


                        } catch (JSONException e) {
                            e.printStackTrace();
                            enableReviewLinear(false);
                        }
                    } else {
                        Log.e("error", "null response");
                        enableReviewLinear(false);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    enableReviewLinear(false);
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
            enableReviewLinear(false);
        }
    }


    private void adaptReviewsList() {
        if (reviewsArrayList.size() > 0) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);

            ReviewsListAdapter myAdapter = new ReviewsListAdapter(context, reviewsArrayList);
            recyclerView.setAdapter(myAdapter);
        } else {
            enableReviewLinear(false);
        }
    }

    private void enableReviewLinear(boolean state) {
        if (state) {
            if (reviewLinear.getVisibility() == View.GONE) {
                reviewLinear.setVisibility(View.VISIBLE);
            }
        } else {
            if (reviewLinear.getVisibility() == View.VISIBLE) {
                reviewLinear.setVisibility(View.GONE);
            }
        }
    }


    //=====end of the Review==============================================================


    private void intentToAddReviewPage() {
        Bundle bundle = new Bundle();
        bundle.putString("listingId", listingId);
        Intent intent = new Intent(context, AddReviewActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    private void intentToSeeAllReviewPage() {
        Bundle bundle = new Bundle();
        bundle.putString("listingId", listingId);
        Intent intent = new Intent(context, SeeAllReviewsActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, ListingDetailActivity.this);
    }

}
