package my.threesector.android.activities.review;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.interfaces.DialogInterface;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.CustomDialog;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddReviewActivity extends BaseActivity {

    private Context context;
    private ProgressDialog progressDialog = null;


    private String listingId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_review);
        context = AddReviewActivity.this;

        Bundle bundle = getIntent().getExtras();
        listeners(bundle);

        listeners(bundle);
    }


    private void listeners(Bundle bundle) {
        hideToolBar();

        if (bundle != null) {
            listingId = bundle.getString("listingId");
        }

        ImageView closeImage = findViewById(R.id.arr_closeImage);
        RatingBar qualityRatingBar = findViewById(R.id.arr_qualityRatingBar);
        RatingBar overallRatingBar = findViewById(R.id.arr_overallRatingBar);

        TextInputLayout reviewTitleTil = findViewById(R.id.arr_reviewTitleTil);
        TextInputEditText reviewTitleEdt = findViewById(R.id.arr_reviewTitleEdt);

        TextInputLayout reviewDescriptionTil = findViewById(R.id.arr_reviewDescriptionTil);
        TextInputEditText reviewDescriptionEdt = findViewById(R.id.arr_reviewDescriptionEdt);

        LinearLayout submitTxt = findViewById(R.id.arr_submitReviewLinear);

        Typeface typeface = MyUtils.getRegularFont(context);
        reviewTitleTil.setTypeface(typeface);
        reviewTitleEdt.setTypeface(typeface);

        reviewDescriptionTil.setTypeface(typeface);
        reviewDescriptionEdt.setTypeface(typeface);

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        submitTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String title = reviewTitleEdt.getText().toString();
                String description = reviewDescriptionEdt.getText().toString();

                if (!MyUtils.checkStringValue(title)) {
                    MyUtils.showLongToast(context, "Review Title required");
                } else if (!MyUtils.checkStringValue(description)) {
                    MyUtils.showLongToast(context, "Review Description required");
                } else {
                   addReview(title,description,listingId); //TODO API NOT UPDATING VALUES IN REAL TIME

                }


            }
        });
    }


    private void addReview(String reviewTitle,String description,String listingId) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating..");

            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("reviewTitle", RequestBody.create(MediaType.parse("text/plain"), reviewTitle));
            map.put("yourReview", RequestBody.create(MediaType.parse("text/plain"), description));
            map.put("ID", RequestBody.create(MediaType.parse("text/plain"), listingId)); //TODO CHECK THE INPUT PARAM

            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.addReview(map, (Constants.BEARER + accessToken));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);
                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status)) {

                                if (status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    showAlertToUser(Constants.DIALOG_SUCCESS, "Post Review", ErrorConstants.ADD_REVIEW_SUCCESS);

                                } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {

                                    String msg = jsonObject.optString("msg");

                                    if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.TOKEN_EXPIRED)) {
                                        //TODO HANDLE TOKEN
                                        showBaseRelative(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                        Log.e("error", "access token expired");
                                    } else {
                                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ADD_REVIEW_ERROR);
                                    }
                                } else {
                                    Log.e("error", "error response");
                                    showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ADD_REVIEW_ERROR);
                                }
                            }
                            //{"status":"error","msg":"tokenExpired"}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ADD_REVIEW_ERROR);
                        }
                    } else {
                        Log.e("error", "null response");
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ADD_REVIEW_ERROR);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        showBaseRelative(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    } else {
                        Log.e("error", "message other issue:" + t.getMessage());
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ADD_REVIEW_ERROR);

                    }
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.ADD_REVIEW_ERROR);
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(AddReviewActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }





}
