package my.threesector.android.activities.chat;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.adapters.ChatMessageAdapter;
import my.threesector.android.models.ChatMessages;
import my.threesector.android.models.ConvMessages;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PreferenceHandler;

public class ConversationActivity extends BaseActivity {

    private Context context = null;
    private ImageView backImage;
    private CircleImageView chatUserProfileImageView;
    private TextView chatUserNameTxt;
    //private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView messagesRecyclerView;
    private ProgressBar messageProgress;
    private EditText messageTxtEdt;
    private LinearLayout sendBtnLinear;
    private ProgressDialog progressDialog = null;

    String userId = "";

    String senderId = "";
    String senderName = "";
    String senderImage = "";
    String receiverId = "";
    String receiverName = "";
    String receiverImage = "";
    String messageKey = "";

    private ArrayList<ConvMessages> convMessagesArrayList = new ArrayList<>();
    private ChatMessageAdapter chatMessageAdapter = null;
    private DatabaseReference messageReference = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coversation);
        context = ConversationActivity.this;

        backImage = findViewById(R.id.ca_backImage);
        chatUserProfileImageView = findViewById(R.id.ca_chatUserProfileImage);
        chatUserNameTxt = findViewById(R.id.ca_chatUserNameTxt);

        //swipeRefreshLayout = findViewById(R.id.ca_swipeRefreshLayout);
        messagesRecyclerView = findViewById(R.id.ca_messagesRecyclerView);
        messageProgress = findViewById(R.id.ca_messageProgress);

        messageTxtEdt = findViewById(R.id.ac_messageTxtEdt);
        sendBtnLinear = findViewById(R.id.ac_sendBtnLinear);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            senderId = bundle.getString("senderId");
            senderName = bundle.getString("senderName");
            senderImage = bundle.getString("senderImage");

            receiverId = bundle.getString("receiverId");
            receiverName = bundle.getString("receiverName");
            receiverImage = bundle.getString("receiverImage");

            messageKey = bundle.getString("messageKey");
        }

        listeners();
    }

    private void listeners() {
        hideToolBar();

        chatUserNameTxt.setText(MyUtils.checkStringValue(receiverName) ? receiverName : "-");
        Glide.with(context).load(receiverImage).placeholder(R.drawable.user_pic).into(chatUserProfileImageView);

        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        //linearLayoutManager.setStackFromEnd(true);
        //linearLayoutManager.setReverseLayout(true);
        messagesRecyclerView.setLayoutManager(linearLayoutManager);
        messagesRecyclerView.setHasFixedSize(true);

        Typeface typeface = MyUtils.getRegularFont(context);
        messageTxtEdt.setTypeface(typeface);


        sendBtnLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageContent = messageTxtEdt.getText().toString().trim();
                if (MyUtils.checkStringValue(messageContent)) {

                    sendMessage(messageContent, messageKey, senderId, Constants.TEXT);

                } else {
                    MyUtils.showLongToast(context, "Text Message required");
                }
            }
        });


        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePage();
            }
        });

        /*swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getChatMessages();
            }
        });*/

        DatabaseReference rootReference = FirebaseDatabase.getInstance().getReference();
        messageReference = rootReference.child(Constants.FDB_MESSAGES);


        getChatMessages();
    }


    //=================Get Chat Messages================================================


    private void getChatMessages() {

        convMessagesArrayList.clear();
        adaptMessagesList();

        Query chatMessagesQuery = messageReference.orderByChild("message_key").equalTo(messageKey);

        chatMessagesQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ChatMessages chatMessages = (ChatMessages) dataSnapshot.getValue(ChatMessages.class);
                if (chatMessages != null) {
                    loadChatMessagesToArrayList(chatMessages);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void loadChatMessagesToArrayList(ChatMessages chatMessages) {

        String chatSenderId = chatMessages.getSender_id();
        boolean isMessageOwner = (MyUtils.checkStringValue(userId) && MyUtils.checkStringValue(chatSenderId)) && userId.equalsIgnoreCase(chatSenderId);

        convMessagesArrayList.add(new ConvMessages(senderId, senderName, senderImage, receiverId, receiverName, receiverImage, isMessageOwner, chatMessages));
        chatMessageAdapter.notifyDataSetChanged();
    }

    private void adaptMessagesList() {
        chatMessageAdapter = new ChatMessageAdapter(context, convMessagesArrayList);
        messagesRecyclerView.setAdapter(chatMessageAdapter);
    }

    //================Send new message============================================================

    private void sendMessage(String message, String messageKey, String senderId, String type) {
        Map<String, String> messageMap = new HashMap<>();
        messageMap.put("message_text", message);
        messageMap.put("message_key", messageKey);
        messageMap.put("sender_id", senderId);
        messageMap.put("type", type);
        messageMap.put("message_date", "" + System.currentTimeMillis());
        messageReference.push().setValue(messageMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e("CHAT_ACTIVITY", "Cannot add message to database");
                } else {
                    clearMessageText();
                }
            }
        });
    }


    private void clearMessageText() {
        messageTxtEdt.setText("");
    }


}
