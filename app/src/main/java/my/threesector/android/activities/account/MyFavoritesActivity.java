package my.threesector.android.activities.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.activities.listing.ListingDetailActivity;
import my.threesector.android.adapters.FavouritesAdapter;
import my.threesector.android.interfaces.FavouritesListener;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.models.Favourites;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.FavouritesState;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFavoritesActivity extends BaseActivity {

    private Context context;
    private RecyclerView recyclerView;
    private ProgressBar categoryProgress;
    private ProgressDialog progressDialog = null;


    private ArrayList<Favourites> favouritesArrayList = new ArrayList<>();
    private FavouritesAdapter myAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favorites);
        context = MyFavoritesActivity.this;

        recyclerView = findViewById(R.id.amf_recyclerView);
        categoryProgress = findViewById(R.id.amf_categoryProgress);

        listeners();
    }

    private void listeners() {

        setTitleTxt("Favourites");

        getFavouritesListing("1"); //TODO PAGINATION NOT NEEDED
    }


    private void getFavouritesListing(String pageNumber) {
        try {
            categoryProgress.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getMyFavourites(pageNumber, (Constants.BEARER + accessToken));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    categoryProgress.setVisibility(View.GONE);
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);
                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status)) {

                                if (status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONArray jsonArray = jsonObject.optJSONArray("oResults");

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        favouritesArrayList.clear();
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            String imageUrl = null;
                                            String listingId = jsonArray.optJSONObject(i).optString("ID");
                                            String postTitle = jsonArray.optJSONObject(i).optString("postTitle");
                                            String tagLine = jsonArray.optJSONObject(i).optString("tagLine");

                                            JSONObject imageObject = jsonArray.optJSONObject(i).optJSONObject("oFeaturedImg");
                                            if (imageObject != null) {
                                                imageUrl = imageObject.optString("large");
                                            }

                                            favouritesArrayList.add(new Favourites(listingId, postTitle, tagLine, imageUrl));
                                        }
                                        adaptCategoriesList();
                                    } else {
                                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.FAVOURITES_LISTING_ERROR);
                                    }
                                } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {

                                    String msg = jsonObject.optString("msg");

                                    if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.TOKEN_EXPIRED)) {
                                        showBaseRelative(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                        Log.e("error", "access token expired");
                                    } else {
                                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.FAVOURITES_LISTING_ERROR);
                                    }
                                } else {
                                    Log.e("error", "error response");
                                    showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.FAVOURITES_LISTING_ERROR);
                                }
                            }
                            //{"status":"error","msg":"tokenExpired"}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.FAVOURITES_LISTING_ERROR);
                        }
                    } else {
                        Log.e("error", "null response");
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.FAVOURITES_LISTING_ERROR);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    categoryProgress.setVisibility(View.GONE);
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        showBaseRelative(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    } else {
                        Log.e("error", "message other issue:" + t.getMessage());
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.FAVOURITES_LISTING_ERROR);
                    }
                }
            });
        } catch (Exception e) {
            categoryProgress.setVisibility(View.GONE);
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.FAVOURITES_LISTING_ERROR);
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void adaptCategoriesList() {
        if (favouritesArrayList.size() > 0) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);

            myAdapter = new FavouritesAdapter(context, favouritesArrayList);
            myAdapter.setListingClickListeners(new ListingClickListeners() {
                @Override
                public void onItemClicked(int position) {

                    String listingId = "" + favouritesArrayList.get(position).getListingId();
                    String listingTitle = "" + favouritesArrayList.get(position).getPostTitle();
                    String logo = "" + favouritesArrayList.get(position).getIconUrl();
                    String coverImage = ""; //TODO SETUP COVER IMAGE HERE

                    Bundle bundle = new Bundle();
                    bundle.putString("listingId", listingId);
                    bundle.putString("listingTitle", listingTitle);
                    bundle.putString("logo", logo);
                    bundle.putString("coverImage", coverImage);

                    intentToCategoryListingPreview(bundle);
                }

                @Override
                public void onFavouritesClicked(int position, SparkButton wishListButton1) {

                    String postId = "" + favouritesArrayList.get(position).getListingId();
                    removeFromFavourites(position, postId);
                }

                @Override
                public void onCallClicked(String phoneNumber) {
                    listingPhoneNumber = phoneNumber;
                    callPhoneNumber();
                }
            });
            recyclerView.setAdapter(myAdapter);
        } else {
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.FAVOURITES_LISTING_ERROR);
        }
    }


    private void removeFromFavourites(int position, String postId) {
        progressDialog = MyUtils.showProgressLoader(context, "Removing..");
        FavouritesState favouritesState = new FavouritesState(true);
        favouritesState.setFavouritesListener(new FavouritesListener() {
            @Override
            public void onFavouritesSuccess(String status) {
                MyUtils.dismissProgressLoader(progressDialog);

                if (status.equalsIgnoreCase(Constants.REMOVE_FROM_FAVOURITES)) {
                    favouritesArrayList.remove(position);

                    if (myAdapter != null) {
                        myAdapter.notifyDataSetChanged();
                    }

                    if (favouritesArrayList.size() == 0) {
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.FAVOURITES_LISTING_ERROR);
                    }
                }
            }

            @Override
            public void onFavouritesFailed(String errorType, String error) {
                MyUtils.dismissProgressLoader(progressDialog);
                showBaseRelative(errorType, error);
            }
        });
        favouritesState.addToFavourites(postId, accessToken);
    }


    private void intentToCategoryListingPreview(Bundle bundle) {
        Intent intent = new Intent(context, ListingDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, MyFavoritesActivity.this);
    }

    /*private void showErrorTxt() {
        if (errorTxt.getVisibility() == View.GONE) {
            errorTxt.setVisibility(View.VISIBLE);
            errorTxt.setText(ErrorConstants.FAVOURITES_LISTING_ERROR);
        }
    }*/


}
