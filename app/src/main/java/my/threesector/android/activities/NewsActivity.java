package my.threesector.android.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.adapters.NewsAdapter;
import my.threesector.android.models.News;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsActivity extends BaseActivity {

    private Context context;
    private RecyclerView newsRecyclerView;
    private ProgressBar progressBar;

    private ArrayList<News> newsArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        context = NewsActivity.this;

        newsRecyclerView = findViewById(R.id.an_newsRecyclerView);
        progressBar = findViewById(R.id.an_progressBar);

        listeners();
    }

    private void listeners() {
        setTitleTxt("News");

        getNewsListing();
    }


    private void getNewsListing() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getNewsListing();
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        String jsonResponse = response.body();
                        Log.e("jsonResponse", ":" + jsonResponse);
                        if (jsonResponse != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonResponse);

                                String status = jsonObject.optString("status");

                                if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONArray jsonArray = jsonObject.optJSONArray("oResults");

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        newsArrayList.clear();
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            String authorImage = null;
                                            String displayName = null;
                                            String imageUrl = null;

                                            String postTitle = jsonArray.optJSONObject(i).optString("postTitle");
                                            JSONObject authorObject = jsonArray.optJSONObject(i).optJSONObject("oAuthor");
                                            if (authorObject != null) {
                                                authorImage = authorObject.optString("avatar");
                                                displayName = authorObject.optString("displayName");
                                            }
                                            JSONObject imageObject = jsonArray.optJSONObject(i).optJSONObject("oFeaturedImg");
                                            if (imageObject != null) {
                                                imageUrl = imageObject.optString("large");
                                            }
                                            String postDate = jsonArray.optJSONObject(i).optString("postDate");

                                            newsArrayList.add(new News(authorImage, displayName, postTitle, imageUrl, postDate));
                                        }

                                        adaptListToRecycler();
                                    }


                                } else {
                                    Log.e("error", "error response");
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("error", "null response");
                        }
                    } else {
                        Log.e("error", "response un successful");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            progressBar.setVisibility(View.GONE);
            Log.e("exp", ":" + e.getMessage());
        }
    }

    private void adaptListToRecycler() {
        if (newsArrayList.size() > 0) {
            newsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            newsRecyclerView.setHasFixedSize(true);

            NewsAdapter myAdapter = new NewsAdapter(context, newsArrayList);
            newsRecyclerView.setAdapter(myAdapter);
        }
    }


}
