package my.threesector.android.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;

import my.threesector.android.R;
import my.threesector.android.interfaces.DialogInterface;
import my.threesector.android.models.Login;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.CustomDialog;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PreferenceHandler;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    private Context context;
    private ImageView backgroundImage;
    private TextView txtSignUp, txtForgotPassword;
    private TextInputLayout emailAddressTil, passwordTil;
    private TextInputEditText emailAddressEdt, passwordEdt;
    private LinearLayout loginLinear;
    private ProgressDialog progressDialog = null;

    private String fcmToken = "";
    private String deviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;

        backgroundImage = findViewById(R.id.al_backgroundImage);

        emailAddressTil = findViewById(R.id.al_emailAddressTil);
        emailAddressEdt = findViewById(R.id.al_emailAddressEdt);

        passwordTil = findViewById(R.id.al_passwordTil);
        passwordEdt = findViewById(R.id.al_passwordEdt);

        txtSignUp = findViewById(R.id.al_btnSignUp);
        loginLinear = findViewById(R.id.al_loginLinear);
        txtForgotPassword = findViewById(R.id.al_btnForgotPassword);

        listeners();
    }

    private void listeners() {
        try {
            Glide.with(context).load(R.drawable.splash1).placeholder(R.drawable.splash1).into(backgroundImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Typeface typeface = MyUtils.getRegularFont(context);
        emailAddressTil.setTypeface(typeface);
        emailAddressEdt.setTypeface(typeface);

        passwordTil.setTypeface(typeface);
        passwordEdt.setTypeface(typeface);


        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToSignUpPage();
            }
        });

        loginLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFields();
            }
        });

        //getFcmToken();
    }

    private void validateFields() {

        String emailAddress = emailAddressEdt.getText().toString().trim();
        String password = passwordEdt.getText().toString().trim();

        if (!MyUtils.checkStringValue(emailAddress)) {
            MyUtils.showLongToast(context, "Email Address required");
        }  else if ((!MyUtils.checkStringValue(password))) {
            MyUtils.showLongToast(context, "Password required");
        } else {
            signInUser(emailAddress, password);
        }
    }


    private void signInUser(String emailAddress, String password) {
        try {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("username", RequestBody.create(MediaType.parse("text/plain"), emailAddress));
            map.put("password", RequestBody.create(MediaType.parse("text/plain"), password));


            progressDialog = MyUtils.showProgressLoader(context, "Signing In..");
            ApiInterface apiService = HttpRequest.getInstance().create(ApiInterface.class);


            Call<Login> call = apiService.loginUser(map);
            call.enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (response.isSuccessful()) {
                        Login login = response.body();
                        if (login != null) {

                            String status = login.getStatus();

                            Log.e("status", ":" + status);

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.LOGGED_IN)) {

                                String accessToken = login.getToken();
                                String userID = "" + login.getOUserInfo().getUserID();
                                String userRole = "" + login.getOUserInfo().getRole();
                                String userName = login.getOUserInfo().getUserName();
                                String avatarUrl = login.getOUserInfo().getAvatar();
                                String coverImageUrl = login.getOUserInfo().getCoverImg();

                                PreferenceHandler.storePreference(context, Constants.LOGIN_ID, emailAddress);
                                PreferenceHandler.storePreference(context, Constants.LOGIN_PASSWORD, password);

                                PreferenceHandler.storePreference(context, Constants.ACCESS_TOKEN, accessToken);
                                PreferenceHandler.storePreference(context, Constants.USER_ID, userID);
                                PreferenceHandler.storePreference(context, Constants.USER_ROLE, userRole);
                                PreferenceHandler.storePreference(context, Constants.USER_NAME, userName);
                                PreferenceHandler.storePreference(context, Constants.USER_PROFILE_PICTURE, avatarUrl);
                                PreferenceHandler.storePreference(context, Constants.USER_COVER_PICTURE, coverImageUrl);

                                closePage(Constants.LOGIN_SUCCESS);

                            } else {
                                Log.e("error11", "null response");
                                showAlertToUser(Constants.DIALOG_FAILURE, "Sign In", ErrorConstants.LOGIN_ERROR);
                            }

                        } else {
                            Log.e("error", "null response");
                            showAlertToUser(Constants.DIALOG_FAILURE, "Sign In", ErrorConstants.LOGIN_ERROR);
                        }
                    } else {
                        Log.e("error", "response un successful");
                        showAlertToUser(Constants.DIALOG_FAILURE, "Sign In", ErrorConstants.LOGIN_ERROR);
                    }
                }

                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    MyUtils.dismissProgressLoader(progressDialog);
                    showAlertToUser(Constants.DIALOG_FAILURE, "Sign In", ErrorConstants.LOGIN_ERROR);
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
            showAlertToUser(Constants.DIALOG_FAILURE, "Sign In", ErrorConstants.LOGIN_ERROR);
        }
    }


    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(LoginActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                //closePage(Constants.LOGIN_FAILURE);
            }

            @Override
            public void onDismissedClicked() {
                //closePage(Constants.LOGIN_FAILURE);
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }




    /*private void getFcmToken() {
        try {
            if (MyUtils.isInternetConnected(context)) {
                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("notifications", "getInstanceId failed", task.getException());
                        }
                        fcmToken = task.getResult().getToken();
                        Log.e("notifications", "token: " + fcmToken);
                    }
                });

                deviceId = MyUtils.getDeviceId(context);
                Log.e("deviceId", "deviceId: " + deviceId);
            }
        } catch (Exception e) {
            Log.e("exception_fcm_token", "" + e);
        }
    }









    private void intentToHomePage() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        finish();
        MyUtils.openOverrideAnimation(true, LoginActivity.this);
    }

   */

    private void intentToSignUpPage() {
        Intent intent = new Intent(context, RegistrationActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, LoginActivity.this);
    }


    @Override
    public void onBackPressed() {
        closePage(Constants.LOGIN_FAILURE);
    }

    private void closePage(int code) {
        setResult(code);
        finish();
        MyUtils.openOverrideAnimation(false, LoginActivity.this);
    }
}
