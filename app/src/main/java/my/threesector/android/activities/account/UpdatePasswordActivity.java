package my.threesector.android.activities.account;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.interfaces.DialogInterface;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.CustomDialog;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePasswordActivity extends BaseActivity {

    private Context context;
    private TextInputLayout tilNewPass, tilConfirmPass;
    private TextInputEditText edtNewPass, edtConfirmPass;
    private TextView txtUpdate;
    private ProgressDialog progressDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        context = UpdatePasswordActivity.this;

        tilNewPass = findViewById(R.id.aup_new_passwordTil);
        edtNewPass = findViewById(R.id.aup_new_passwordEdt);

        tilConfirmPass = findViewById(R.id.aup_conPasswordTil);
        edtConfirmPass = findViewById(R.id.aup_conPasswordEdt);

        txtUpdate = findViewById(R.id.aup_txtUpdate);

        listeners();
    }

    private void listeners() {
        setTitleTxt("Update Password");

        Typeface typeface = MyUtils.getRegularFont(context);
        tilNewPass.setTypeface(typeface);
        edtNewPass.setTypeface(typeface);

        tilConfirmPass.setTypeface(typeface);
        edtConfirmPass.setTypeface(typeface);

        txtUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPassword = edtNewPass.getText().toString();
                String confirmPassword = edtConfirmPass.getText().toString();

                if (!MyUtils.checkStringValue(newPassword)) {
                    MyUtils.showLongToast(context, "New Password required");
                } else if (!MyUtils.checkStringValue(confirmPassword)) {
                    MyUtils.showLongToast(context, "Confirm New Password required");
                } else {
                    if (newPassword.equals(confirmPassword)) {
                        changePassword(newPassword);
                    } else {
                        MyUtils.showLongToast(context, "New Password and Confirm New Password are incorrect");
                    }

                }
            }
        });
    }


    private void changePassword(String newPassword) {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Updating..");
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("new_password", RequestBody.create(MediaType.parse("text/plain"), newPassword));
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.updatePassword(map, (Constants.BEARER + accessToken));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);
                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status)) {

                                if (status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    showAlertToUser(Constants.DIALOG_SUCCESS, "Update Password", ErrorConstants.UPDATE_PASSWORD_SUCCESS);

                                } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {

                                    String msg = jsonObject.optString("msg");

                                    if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.TOKEN_EXPIRED)) {
                                        //TODO HANDLE TOKEN
                                        showBaseRelative(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                        Log.e("error", "access token expired");
                                    } else {
                                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.UPDATE_PASSWORD_ERROR);
                                    }
                                } else {
                                    Log.e("error", "error response");
                                    showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.UPDATE_PASSWORD_ERROR);
                                }
                            }
                            //{"status":"error","msg":"tokenExpired"}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.UPDATE_PASSWORD_ERROR);
                        }
                    } else {
                        Log.e("error", "null response");
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.UPDATE_PASSWORD_ERROR);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        showBaseRelative(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    } else {
                        Log.e("error", "message other issue:" + t.getMessage());
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.UPDATE_PASSWORD_ERROR);
                    }
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.UPDATE_PASSWORD_ERROR);
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(UpdatePasswordActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {
                closePage();
            }

            @Override
            public void onDismissedClicked() {
                closePage();
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }


}
