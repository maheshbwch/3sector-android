package my.threesector.android.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.activities.webview.AddListingWebView;
import my.threesector.android.activities.webview.BecomeMerchant;
import my.threesector.android.adapters.HomePagerAdapter;
import my.threesector.android.interfaces.CallListener;
import my.threesector.android.interfaces.HomeListener;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PreferenceHandler;

public class HomeActivity extends BaseActivity implements HomeListener, CallListener {

    private Context context;
    private CircleImageView userProfileImage;
    private TextView titleTxt, becomeMerchantTxt, addListingTxt;
    private ViewPager viewPager;

    private ProgressDialog progressDialog = null;

    private HomePagerAdapter homePagerAdapter = null;

    private String profilePicture = null;

    private HomeListener homeListener = null;
    private CallListener callListener = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = HomeActivity.this;
        homeListener = this;
        callListener = this;

        titleTxt = findViewById(R.id.ah_titleTxt);

        becomeMerchantTxt = findViewById(R.id.ah_becomeMerchantTxt);
        addListingTxt = findViewById(R.id.ah_addListingTxt);
        userProfileImage = findViewById(R.id.ah_userProfileImage);

        viewPager = findViewById(R.id.ah_viewPager);

        init();
    }

    private void init() {
        hideToolBar();
        showBottomNavigationView();
        listeners();
    }

    private void listeners() {

        profilePicture = PreferenceHandler.getPreferenceFromString(context, Constants.USER_PROFILE_PICTURE);

        becomeMerchantTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToBecomeAuthorWebViewActivity();
            }
        });

        addListingTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToWebViewActivity();
            }
        });


        homePagerAdapter = new HomePagerAdapter(getSupportFragmentManager(), homeListener, callListener);
        viewPager.setAdapter(homePagerAdapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.e("selected", "page:" + position);
                setUpToolBarTitle(position);
                setNavigationSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        showIconForHomePage(true);
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.ahm_home_item:
                        setUpToolBarTitle(0);
                        setNavigationSelected(0);
                        showPagerFragment(0);
                        break;
                    case R.id.ahm_chat_item:
                        setUpToolBarTitle(1);
                        setNavigationSelected(1);
                        showPagerFragment(1);
                        break;
                   /* case R.id.ahm_contact_item:
                        setUpToolBarTitle(2);
                        setNavigationSelected(2);
                        showPagerFragment(2);
                        break;*/
                    case R.id.ahm_project_item:
                        setUpToolBarTitle(2);
                        setNavigationSelected(2);
                        showPagerFragment(2);
                        break;
                    case R.id.ahm_profile_item:
                        setUpToolBarTitle(3);
                        setNavigationSelected(3);
                        showPagerFragment(3);
                        break;
                    default:
                        break;
                }
                Log.e("menu", "item:" + menuItem.getItemId());
                return false;
            }
        });

    }

    private void setUpToolBarTitle(int position) {
        switch (position) {
            case 0:
                titleTxt.setText("Home");
                showIconForHomePage(true);
                showUserProfilePic(false);
                break;
            case 1:
                titleTxt.setText("Chat");
                showUserProfilePic(true);
                showIconForHomePage(false);
                break;
            /*case 2:
                titleTxt.setText("Following Contacts");
                showIconForHomePage(false);
                showUserProfilePic(false);
                break;*/
            case 2:
                titleTxt.setText("Projects");
                showIconForHomePage(false);
                showUserProfilePic(false);
                break;
            case 3:
                titleTxt.setText("Profile");
                showIconForHomePage(false);
                showUserProfilePic(false);
                break;
            default:
                break;
        }
    }

    private void showIconForHomePage(boolean state) {
        if (state) {

            //showAuthorIcon(true);
            //showAddListingIcon(true);

            String userRole = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ROLE);
            Log.e("userRole", ":" + userRole);

            if (MyUtils.checkStringValue(userRole)) {
                if ((userRole.equalsIgnoreCase(Constants.CONTRIBUTOR) || userRole.equalsIgnoreCase(Constants.ADMINISTRATOR))) {
                    showAddListingIcon(true);
                    showAuthorIcon(false);
                } else if (userRole.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                    showAuthorIcon(true);
                    showAddListingIcon(false);
                } else {
                    showAuthorIcon(false);
                    showAddListingIcon(false);
                }
            }

        } else {
            showAuthorIcon(false);
            showAddListingIcon(false);
        }
    }

    private void showAuthorIcon(boolean state) {
        if (state) {
            if (becomeMerchantTxt.getVisibility() == View.GONE) {
                becomeMerchantTxt.setVisibility(View.VISIBLE);
            }
        } else {
            if (becomeMerchantTxt.getVisibility() == View.VISIBLE) {
                becomeMerchantTxt.setVisibility(View.GONE);
            }
        }
    }

    private void showAddListingIcon(boolean state) {
        if (state) {
            if (addListingTxt.getVisibility() == View.GONE) {
                addListingTxt.setVisibility(View.VISIBLE);
            }
        } else {
            if (addListingTxt.getVisibility() == View.VISIBLE) {
                addListingTxt.setVisibility(View.GONE);
            }
        }
    }


    private void showUserProfilePic(boolean state) {
        if (state) {
            if (userProfileImage.getVisibility() == View.GONE) {
                userProfileImage.setVisibility(View.VISIBLE);
                if (MyUtils.checkStringValue(profilePicture)) {
                    Glide.with(context).load(profilePicture).placeholder(R.drawable.user_pic).into(userProfileImage);
                }
            }
        } else {
            if (userProfileImage.getVisibility() == View.VISIBLE) {
                userProfileImage.setVisibility(View.GONE);
            }
        }
    }


    private void setNavigationSelected(int position) {
        bottomNavigation.getMenu().getItem(position).setChecked(true);
    }

    private void showPagerFragment(int position) {
        viewPager.setCurrentItem(position);
    }


    //=================BECOME A AUTHOR API CALL===================================================

    /*private void becomeAAuthor() {
        try {
            progressDialog = MyUtils.showProgressLoader(context, "Loading..");
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.becomeAnAuthor(Constants.BEARER + accessToken);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");
                            String message = jsonObject.optString("message");

                            if (MyUtils.checkStringValue(status)) {

                                if (status.equalsIgnoreCase(Constants.API_SUCCESS_CODE)) {

                                    PreferenceHandler.storePreference(context, Constants.USER_ROLE, Constants.MERCHANT_USER);
                                    userRole = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ROLE);
                                    showIconForHomePage(true);

                                    showAlertToUser(Constants.DIALOG_SUCCESS, "Become a Merchant", (MyUtils.checkStringValue(message) ? message : ErrorConstants.BECOME_AUTHOR_SUCCESS));
                                } else if (status.equalsIgnoreCase(Constants.API_FAILURE_CODE)) {

                                    showIconForHomePage(true);
                                    showAlertToUser(Constants.DIALOG_FAILURE, "Become a Merchant", (MyUtils.checkStringValue(message) ? message : ErrorConstants.BECOME_AUTHOR_ERROR));

                                } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {
                                    String msg = jsonObject.optString("msg");
                                    if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.TOKEN_EXPIRED)) {
                                        homeListener.onApiCallFailed(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                    }
                                }
                            }

                            //{"status": 1,"message": "Success, Now you are an author"}
                            //{"status":0, "message":"This user is already an Author"}
                            //{"status": "error", "msg": "tokenExpired"}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("error", "null response");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    MyUtils.dismissProgressLoader(progressDialog);
                    Log.e("error", "message:" + t.getMessage());
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        homeListener.onApiCallFailed(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    }
                }
            });
        } catch (Exception e) {
            MyUtils.dismissProgressLoader(progressDialog);
            Log.e("exp", ":" + e.getMessage());
        }
    }*/


    /*private void showAlertToUser(String statusCode, String title, String statusMessage) {
        CustomDialog customDialog = new CustomDialog(HomeActivity.this, statusCode, statusMessage, new DialogInterface() {
            @Override
            public void onButtonClicked() {

            }

            @Override
            public void onDismissedClicked() {
            }
        });
        customDialog.setTitleMessage(title);
        customDialog.showDialog();
    }*/

    /*private void intentToAddListingPage() {
        Intent intent = new Intent(context, AddCompanyListing.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, HomeActivity.this);
    }*/

    private void intentToWebViewActivity() {
        Intent intent = new Intent(context, AddListingWebView.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, HomeActivity.this);
    }

    private void intentToBecomeAuthorWebViewActivity() {
        Intent intent = new Intent(context, BecomeMerchant.class);
        startActivityForResult(intent, Constants.BECOME_A_MERCHANT_RESULT);
        MyUtils.openOverrideAnimation(true, HomeActivity.this);
    }

    private void intentToLoginPage() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, HomeActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Constants.LOGIN_SUCCESS:
                //becomeAAuthor();
                break;
            case Constants.LOGIN_FAILURE:

                break;
            case Constants.BECOME_A_MERCHANT_SUCCESS_RESULT:
                showAuthorIcon(false);
                showAddListingIcon(true);
                PreferenceHandler.storePreference(context, Constants.USER_ROLE, Constants.CONTRIBUTOR);
                break;
            default:
                break;
        }
    }


    @Override
    public void onApiCallSuccess() {
        hideBaseRelative();
    }

    @Override
    public void onApiCallFailed(String errorType, String error) {
        showBaseRelative(errorType, error);
    }

    @Override
    public void onCallClicked(String phoneNumber) {
        listingPhoneNumber = phoneNumber;
        callPhoneNumber();
    }


}
