package my.threesector.android.activities.listing;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.BaseActivity;
import my.threesector.android.adapters.ListingAdapter;
import my.threesector.android.interfaces.FavouritesListener;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.models.CategoryListing;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.FavouritesState;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.ParseListing;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryListingActivity extends BaseActivity {

    private Context context;
    private RecyclerView recyclerView;
    //private TextView errorTxt;
    private ProgressBar categoryProgress;
    private ProgressDialog progressDialog = null;

    private ArrayList<CategoryListing> categoryListingArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        context = CategoryListingActivity.this;


        recyclerView = findViewById(R.id.acl_recyclerView);
        //errorTxt = findViewById(R.id.acl_errorTxt);
        categoryProgress = findViewById(R.id.acl_categoryProgress);

        Bundle bundle = getIntent().getExtras();

        listeners(bundle);

    }

    private void listeners(Bundle bundle) {

        if (bundle != null) {
            String termId = bundle.getString("termId");
            String name = bundle.getString("name");

            Log.e("data", ":" + termId + " - " + name);

            setTitleTxt(MyUtils.checkStringValue(name) ? name : "Category Listing");

            if (MyUtils.checkStringValue(termId)) {
                getCategoryListing(termId);
            }
        }


    }


    private void getCategoryListing(String termId) {
        try {
            categoryProgress.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getCategoryListing(termId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    categoryProgress.setVisibility(View.GONE);
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);

                    //String categoriesHome = response.body();
                    if (jsonResponse != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                JSONArray jsonArray = jsonObject.optJSONArray("oResults");
                                categoryListingArrayList.clear();
                                categoryListingArrayList.addAll(ParseListing.getCategoryListingArrayList(jsonArray));

                                adaptCategoriesList();

                            } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {

                                String msg = jsonObject.optString("msg");

                                if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.TOKEN_EXPIRED)) {
                                    //TODO HANDLE TOKEN
                                    showBaseRelative(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                    Log.e("error", "access token expired");
                                } else {
                                    showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.CATEGORIES_LISTING_ERROR);
                                }
                            } else {
                                Log.e("error", "error response");
                                showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.CATEGORIES_LISTING_ERROR);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.CATEGORIES_LISTING_ERROR);
                        }
                    } else {
                        Log.e("error", "null response");
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.CATEGORIES_LISTING_ERROR);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    categoryProgress.setVisibility(View.GONE);
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        showBaseRelative(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    } else {
                        Log.e("error", "message other issue:" + t.getMessage());
                        showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.MY_LISTING_ERROR);
                    }
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            categoryProgress.setVisibility(View.GONE);
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.CATEGORIES_LISTING_ERROR);
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void adaptCategoriesList() {
        if (categoryListingArrayList.size() > 0) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setHasFixedSize(true);

            ListingAdapter myAdapter = new ListingAdapter(context, categoryListingArrayList);
            myAdapter.setListingClickListeners(new ListingClickListeners() {
                @Override
                public void onItemClicked(int position) {
                    String listingId = "" + categoryListingArrayList.get(position).getListingId();
                    String listingTitle = "" + categoryListingArrayList.get(position).getPostTitle();
                    String logo = "" + categoryListingArrayList.get(position).getLogo();
                    String coverImage = "" + categoryListingArrayList.get(position).getCoverImg();

                    Bundle bundle = new Bundle();
                    bundle.putString("listingId", listingId);
                    bundle.putString("listingTitle", listingTitle);
                    bundle.putString("logo", logo);
                    bundle.putString("coverImage", coverImage);

                    intentToCategoryListingPreview(bundle);
                }

                @Override
                public void onFavouritesClicked(int position, SparkButton wishListButton) {
                    String postId = "" + categoryListingArrayList.get(position).getListingId();

                    addToFavorites(postId, wishListButton);
                }

                @Override
                public void onCallClicked(String phoneNumber) {
                    listingPhoneNumber = phoneNumber;
                    callPhoneNumber();
                }
            });
            recyclerView.setAdapter(myAdapter);
        } else {
            showBaseRelative(Constants.ERROR_RESPONSE, ErrorConstants.CATEGORIES_LISTING_ERROR);
        }
    }

    private void addToFavorites(String postId, SparkButton wishListButton) {
        boolean state = wishListButton.isChecked();
        progressDialog = MyUtils.showProgressLoader(context, (state ? "Removing.." : "Adding.."));
        FavouritesState favouritesState = new FavouritesState(state);
        favouritesState.setFavouritesListener(new FavouritesListener() {
            @Override
            public void onFavouritesSuccess(String status) {
                MyUtils.dismissProgressLoader(progressDialog);

                if (status.equalsIgnoreCase(Constants.ADD_TO_FAVOURITES)) {
                    wishListButton.setChecked(true);
                } else if (status.equalsIgnoreCase(Constants.REMOVE_FROM_FAVOURITES)) {
                    wishListButton.setChecked(false);
                }
            }

            @Override
            public void onFavouritesFailed(String errorType, String error) {
                MyUtils.dismissProgressLoader(progressDialog);
                showBaseRelative(errorType, error);
                if (state) {
                    wishListButton.setChecked(true);
                } else {
                    wishListButton.setChecked(false);
                }
            }
        });
        favouritesState.addToFavourites(postId, accessToken);
    }


    private void intentToCategoryListingPreview(Bundle bundle) {
        Intent intent = new Intent(context, ListingDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, CategoryListingActivity.this);
    }


}
