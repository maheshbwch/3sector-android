package my.threesector.android.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import my.threesector.android.R;

public class CareTakerListDialog extends DialogFragment {

    public CareTakerListDialog() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View rootView = inflater.inflate(R.layout.dialog_custom_spinner, null);

        setUpView(rootView);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setView(rootView);

        /*alertDialog.setPositiveButton("CLOSE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });*/

        //alertDialog.setTitle(MyUtils.checkStringValue(title) ? title : "Select Item");

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return dialog;
    }

    /*public void setTitle(String title) {
        this.title = title;
    }*/


    private void setUpView(View rootView) {
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.dcs_listItemsRecycler);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        /*careTakersAdapter = new CareTakersAdapter(getActivity(), arrayList,R.layout.item_care_taker1);
        careTakersAdapter.setOnItemClicked(new OnItemClicked() {
            @Override
            public void onItemClicked(int position) {
                dismiss();
                onItemSelected.onItemSelected(position);
            }
        });
        recyclerView.setAdapter(careTakersAdapter);*/
    }




    /*public void updateStatusInSpinner(int position, boolean state) {
        if (licenceRecyclerAdapter != null && licenseDataArrayList.size() > 0) {
            Log.e("pos", "pos:" + position);
            licenceRecyclerAdapter.updateStatus(position, state);
            licenceRecyclerAdapter.notifyDataSetChanged();
        }
    }*/


    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }
}
