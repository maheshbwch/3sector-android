package my.threesector.android.fragments.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.chat.ConversationActivity;
import my.threesector.android.adapters.ChatUsersAdapter;
import my.threesector.android.interfaces.HomeListener;
import my.threesector.android.interfaces.OnItemClicked;
import my.threesector.android.models.ChatUsers;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PreferenceHandler;

public class ChatFragment extends Fragment {


    private Context context = null;
    private RecyclerView recyclerView;
    private TextView errorTxt;
    private ProgressBar chatListProgressBar;

    private ArrayList<ChatUsers> chatUsersArrayList = new ArrayList<>();
    private ChatUsersAdapter chatUsersAdapter = null;

    private HomeListener homeListener = null;
    private String userId = "";

    private DatabaseReference friendsDataBaseReference = null;

    public ChatFragment(HomeListener homeListener) {
        this.homeListener = homeListener;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        context = getActivity();

        init(rootView);

        return rootView;
    }

    private void init(View view) {
        recyclerView = view.findViewById(R.id.fc_chatListRecyclerView);
        chatListProgressBar = view.findViewById(R.id.fc_chatListProgressBar);
        errorTxt = view.findViewById(R.id.fc_errorTxt);


        listeners();
    }

    private void listeners() {
        userId = PreferenceHandler.getPreferenceFromString(context, Constants.USER_ID);

        getChatUsers();
    }

    private void getChatUsers() {
        friendsDataBaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.FDB_FRIENDS).child(userId);
        friendsDataBaseReference.keepSynced(true);

        chatUsersArrayList.clear();
        adaptChatUsersList();

        friendsDataBaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ChatUsers chatUsers = (ChatUsers) dataSnapshot.getValue(ChatUsers.class);
                if (chatUsers != null) {
                    loadChatUsersToArrayList(chatUsers);
                } else {
                    showErrorTxt();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showErrorTxt();
            }
        });
    }

    private void loadChatUsersToArrayList(ChatUsers chatUsers) {
        chatUsersArrayList.add(chatUsers);
        chatUsersAdapter.notifyDataSetChanged();

        if (chatUsersArrayList.size() == 0) {
            showErrorTxt();
        }
        if (chatUsersArrayList.size() > 0) {
            resetViews();
        }
    }


    private void adaptChatUsersList() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);

        chatUsersAdapter = new ChatUsersAdapter(context, chatUsersArrayList);
        chatUsersAdapter.setOnItemClicked(new OnItemClicked() {
            @Override
            public void onItemClicked(int position) {
                String senderId = chatUsersArrayList.get(position).getSender_id();
                String senderName = chatUsersArrayList.get(position).getSender_name();
                String senderImage = chatUsersArrayList.get(position).getSender_image();

                String receiverId = chatUsersArrayList.get(position).getReceiver_id();
                String receiverName = chatUsersArrayList.get(position).getReceiver_name();
                String receiverImage = chatUsersArrayList.get(position).getReceiver_image();

                String message_key = chatUsersArrayList.get(position).getMessage_key();

                Bundle bundle = new Bundle();
                bundle.putString("senderId", senderId);
                bundle.putString("senderName", senderName);
                bundle.putString("senderImage", senderImage);
                bundle.putString("receiverId", receiverId);
                bundle.putString("receiverName", receiverName);
                bundle.putString("receiverImage", receiverImage);
                bundle.putString("messageKey", message_key);

                intentToConversationActivity(bundle);
            }
        });
        recyclerView.setAdapter(chatUsersAdapter);
    }

    private void showErrorTxt() {
        if (errorTxt.getVisibility() == View.GONE) {
            errorTxt.setVisibility(View.VISIBLE);
            errorTxt.setText(ErrorConstants.ERROR_NO_CHAT_USERS);
        }
    }

    private void resetViews() {
        if (errorTxt.getVisibility() == View.VISIBLE) {
            errorTxt.setVisibility(View.GONE);
        }
    }


    private void intentToConversationActivity(Bundle bundle) {
        Intent intent = new Intent(context, ConversationActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }


}
