package my.threesector.android.fragments.sign_up;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import my.threesector.android.R;
import my.threesector.android.interfaces.SignUpModeListener;

public class SignUpFragment extends Fragment {

    private TextView merchantBtn, freeSignUpBtn, goBackBtn, nextStepBtn;

    private SignUpModeListener signUpModeListener;
    private boolean isFreeSignUpProcess = false;

    public SignUpFragment(SignUpModeListener signUpModeListener) {
        this.signUpModeListener = signUpModeListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View view) {
        merchantBtn = view.findViewById(R.id.fsu_merchantBtn);
        freeSignUpBtn = view.findViewById(R.id.fsu_freeSignUpBtn);
        goBackBtn = view.findViewById(R.id.fsu_goBackBtn);
        nextStepBtn = view.findViewById(R.id.fsu_nextStepBtn);


        listeners();
    }

    private void listeners() {
        merchantBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableLayoutsForMerchantBtn();
            }
        });

        freeSignUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableLayoutsForFreeSignUpBtn();
            }
        });
        goBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpModeListener.onBackButtonClicked();
            }
        });

        nextStepBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpModeListener.onNextButtonClicked(isFreeSignUpProcess);
            }
        });

    }

    //=============toggle button========================================
    private void enableLayoutsForMerchantBtn() {
        merchantBtn.setBackground(getResources().getDrawable(R.drawable.blue_curved_bg));
        merchantBtn.setTextColor(getResources().getColor(R.color.white));
        freeSignUpBtn.setBackground(getResources().getDrawable(R.drawable.blue_unselect_bg));
        freeSignUpBtn.setTextColor(getResources().getColor(R.color.black));
        signUpModeListener.onMerchantRegistrationClicked();
        isFreeSignUpProcess = false;
    }


    private void enableLayoutsForFreeSignUpBtn() {
        merchantBtn.setBackground(getResources().getDrawable(R.drawable.blue_unselect_bg));
        merchantBtn.setTextColor(getResources().getColor(R.color.black));
        freeSignUpBtn.setBackground(getResources().getDrawable(R.drawable.blue_curved_bg));
        freeSignUpBtn.setTextColor(getResources().getColor(R.color.white));
        signUpModeListener.onFreeSignUpClicked();
        isFreeSignUpProcess = true;
    }


}
