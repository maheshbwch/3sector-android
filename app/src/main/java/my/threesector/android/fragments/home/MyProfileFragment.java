package my.threesector.android.fragments.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.activities.SplashScreen;
import my.threesector.android.activities.account.MyFavoritesActivity;
import my.threesector.android.activities.account.MyListingActivity;
import my.threesector.android.activities.account.ProfileActivity;
import my.threesector.android.activities.account.UpdatePasswordActivity;
import my.threesector.android.interfaces.CustomAlertInterface;
import my.threesector.android.interfaces.HomeListener;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.CustomAlert;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PreferenceHandler;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileFragment extends Fragment {

    private Context context;
    private ImageView coverImageView;
    private CircleImageView profileImageView;
    private TextView userNameTxt;
    private LinearLayout accountLinear, listingLinear, favouritesLinear, notificationsLinear, changePasswordLinear, logoutLinear;


    private HomeListener homeListener = null;

    public MyProfileFragment(HomeListener homeListener) {
        this.homeListener = homeListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);
        context = getActivity();

        init(rootView);

        return rootView;
    }

    private void init(View view) {
        coverImageView = view.findViewById(R.id.fmp_coverImageView);
        profileImageView = view.findViewById(R.id.fmp_profileImage);
        userNameTxt = view.findViewById(R.id.fmp_userNameTxt);

        accountLinear = view.findViewById(R.id.fmp_accountLinear);
        listingLinear = view.findViewById(R.id.fmp_listingLinear);
        favouritesLinear = view.findViewById(R.id.fmp_favouritesLinear);
        notificationsLinear = view.findViewById(R.id.fmp_notificationsLinear);
        changePasswordLinear = view.findViewById(R.id.fmp_changePasswordLinear);
        logoutLinear = view.findViewById(R.id.fmp_logoutLinear);

        listeners();
    }

    private void listeners() {
        getUserSessionValues();

        getProfileDetails();

        accountLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToProfilePage();
            }
        });

        listingLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToMyListingPage();
            }
        });

        favouritesLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToMyFavoritesPage();
            }
        });

        changePasswordLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToUpdatePasswordPage();
            }
        });

        logoutLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogOutAlert();
            }
        });

        homeListener.onApiCallSuccess();
    }


    private void getUserSessionValues() {
        String userName = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.USER_NAME);
        String profilePicture = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.USER_PROFILE_PICTURE);
        String coverPicture = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.USER_COVER_PICTURE);

        userNameTxt.setText(MyUtils.checkStringValue(userName) ? userName : "-");
        if (MyUtils.checkStringValue(profilePicture)) {
            Glide.with(context).load(profilePicture).placeholder(R.drawable.user_pic).into(profileImageView);
        }

        if (MyUtils.checkStringValue(coverPicture)) {
            Glide.with(context).load(coverPicture).into(coverImageView);
        }
    }


    //=================Get user profile details===========================================================


    private void getProfileDetails() {
        try {
            String accessToken = PreferenceHandler.getPreferenceFromString(context, Constants.ACCESS_TOKEN);
            Log.e("accessToken", ":" + accessToken);

            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getProfileDetails(Constants.BEARER + accessToken);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    //homeListener.onApiCallSuccess();
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");
                            String msg = jsonObject.optString("msg");

                            if (MyUtils.checkStringValue(status)) {

                                if (status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONObject jsonObject1 = jsonObject.optJSONObject("oResults");
                                    if (jsonObject1 != null) {
                                        JSONObject basicInfoObject = jsonObject1.optJSONObject("oBasicInfo");

                                        if (basicInfoObject != null) {
                                            String userName = basicInfoObject.optString("user_name");
                                            String profileImage = basicInfoObject.optString("avatar");
                                            String coverImage = basicInfoObject.optString("cover_image");
                                            String emailAddress = basicInfoObject.optString("email");

                                            Log.e("userName", ":" + userName);

                                            if (MyUtils.checkStringValue(userName)) {
                                                PreferenceHandler.storePreference(context, Constants.USER_NAME, userName);
                                            }

                                            if (MyUtils.checkStringValue(profileImage)) {
                                                PreferenceHandler.storePreference(context, Constants.USER_PROFILE_PICTURE, profileImage);
                                            }

                                            if (MyUtils.checkStringValue(coverImage)) {
                                                PreferenceHandler.storePreference(context, Constants.USER_COVER_PICTURE, coverImage);
                                            }

                                            if (MyUtils.checkStringValue(emailAddress)) {
                                                PreferenceHandler.storePreference(context, Constants.USER_EMAIL_ADDRESS, emailAddress);
                                            }
                                        }
                                    }

                                    getUserSessionValues();


                                } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {
                                    /*if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.USER_NOT_FOUND)) {
                                        homeListener.onApiCallFailed(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                    }*/
                                }
                            }

                            //{"status": "error","msg": "foundNoUser"}

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("error", "null response");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("error", "message:" + t.getMessage());
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        homeListener.onApiCallFailed(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    }
                }
            });
        } catch (Exception e) {
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void intentToProfilePage() {
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }


    private void intentToMyListingPage() {
        Intent intent = new Intent(getActivity(), MyListingActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }


    private void intentToUpdatePasswordPage() {
        Intent intent = new Intent(getActivity(), UpdatePasswordActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }


    private void intentToMyFavoritesPage() {
        Intent intent = new Intent(getActivity(), MyFavoritesActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }


    private void showLogOutAlert() {
        CustomAlert customAlert = new CustomAlert(context, "You are sure want to logout?", "LOGOUT", "CANCEL", new CustomAlertInterface() {
            @Override
            public void onPositiveButtonClicked() {
                PreferenceHandler.clearPreferences(context);
                Intent intent = new Intent(context, SplashScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onNegativeButtonClicked() {

            }
        });
        customAlert.showDialog(R.color.green, R.color.red);
    }


}
