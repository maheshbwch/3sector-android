package my.threesector.android.fragments.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import my.threesector.android.R;
import my.threesector.android.interfaces.HomeListener;

public class ContactsFragment extends Fragment /*implements Sectionizer*/ {

    /*RecyclerView recyclerView;
    List<AnimalObject> list;
    SectionedRecyclerViewAdapter sectionedRecyclerViewAdapter;*/

    private HomeListener homeListener = null;

    public ContactsFragment(HomeListener homeListener) {
        this.homeListener = homeListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_contacts, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View view) {

        /*recyclerView = view.findViewById(R.id.recyclerViewActivityExample);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        list = new ArrayList<>();
        list.add(new AnimalObject("Arun", "A"));
        list.add(new AnimalObject("Alex", "A"));
        list.add(new AnimalObject("Arokiya", "A"));

        list.add(new AnimalObject("Daniel", "D"));
        list.add(new AnimalObject("David", "D"));

        list.add(new AnimalObject("Ganesh", "G"));
        list.add(new AnimalObject("Gowtham", "G"));

        list.add(new AnimalObject("Karthick", "K"));
        list.add(new AnimalObject("Kumar", "K"));

        MyAdapter myAdapter = new MyAdapter();


        sectionedRecyclerViewAdapter = new SectionedRecyclerViewAdapter(getActivity(), R.layout.layout_list_section, R.id.textViewItemSection, myAdapter, this);
        sectionedRecyclerViewAdapter.setSections(list);

        recyclerView.setAdapter(sectionedRecyclerViewAdapter);

        listeners();*/
    }

    /*private void listeners() {

    }


    public class ContactsAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView title;
        ViewHolderClickListener clickListener;

        public ContactsAdapter(View itemView, ViewHolderClickListener clickListener) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.textViewItemList);
            this.clickListener = clickListener;
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            if (clickListener != null)
                clickListener.onClick(v);

        }
    }

    public interface ViewHolderClickListener {
        public void onClick(View v);
    }

    @Override
    public String getSectionTitle(Object object) {
        return ((AnimalObject) object).type;
    }


    //-------------------Adapter----------------------------
    public class MyAdapter extends RecyclerView.Adapter<ContactsAdapter> implements ViewHolderClickListener {

        @Override
        public int getItemCount() {
            return list.size();
        }

        @Override
        public ContactsAdapter onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_contact_item, parent, false);

            return new ContactsAdapter(itemView, this);
        }

        @Override
        public void onBindViewHolder(ContactsAdapter holder, int position) {
            AnimalObject animalObject = list.get(position);
            holder.title.setText(animalObject.name);

        }

        @Override
        public void onClick(View v) {

        }

    }*/


}
