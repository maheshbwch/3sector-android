package my.threesector.android.fragments.home;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.NearbyPlacesActivity;
import my.threesector.android.activities.NewsActivity;
import my.threesector.android.activities.SearchActivity;
import my.threesector.android.activities.listing.CategoryListingActivity;
import my.threesector.android.activities.listing.ListingDetailActivity;
import my.threesector.android.activities.webview.QuestionsActivity;
import my.threesector.android.adapters.HomeIndustryAdapter;
import my.threesector.android.adapters.SectionHeadAdapter;
import my.threesector.android.interfaces.CallListener;
import my.threesector.android.interfaces.FavouritesListener;
import my.threesector.android.interfaces.HomeListener;
import my.threesector.android.interfaces.OnItemClicked;
import my.threesector.android.interfaces.SectionClickListener;
import my.threesector.android.models.CategoriesHome;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.FavouritesState;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PermissionAlert;
import my.threesector.android.utils.PermissionRequest;
import my.threesector.android.utils.PreferenceHandler;
import my.threesector.android.utils.custom_spinner.OnItemSelected;
import my.threesector.android.utils.custom_spinner.SpinnerDialog;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private LinearLayout categoriesLinear, searchLinear;
    private RecyclerView industry_recycler, sectionHeaderRecycler;
    private RelativeLayout selectCategoryRelative;
    private ProgressBar categoryProgress;
    private ArrayList<CategoriesHome> categoriesHomeArrayList = new ArrayList<>();
    private TextView nearByTxt, questionsTxt, newsTxt;
    private ProgressDialog progressDialog = null;

    private HomeListener homeListener = null;
    private CallListener callListener = null;
    private String accessToken = null;

    public HomeFragment(HomeListener homeListener, CallListener callListener) {
        this.homeListener = homeListener;
        this.callListener = callListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View view) {
        searchLinear = view.findViewById(R.id.fh_searchLinear);
        nearByTxt = view.findViewById(R.id.fh_nearByTxt);
        questionsTxt = view.findViewById(R.id.fh_questionsTxt);
        newsTxt = view.findViewById(R.id.fh_newsTxt);

        categoriesLinear = view.findViewById(R.id.fh_categoriesLinear);
        industry_recycler = view.findViewById(R.id.fh_industry_recycler);
        categoryProgress = view.findViewById(R.id.fh_categoryProgress);

        selectCategoryRelative = view.findViewById(R.id.fh_selectCategoryRelative);

        sectionHeaderRecycler = view.findViewById(R.id.fh_sectionHeaderRecycler);
        listeners();
    }

    private void listeners() {
        accessToken = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.ACCESS_TOKEN);
        Log.e("accessToken", ":" + accessToken);

        industry_recycler.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        industry_recycler.setHasFixedSize(true);

        sectionHeaderRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        getCategoryList();

        searchLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToSearchPage();
            }
        });

        nearByTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (PermissionRequest.askForPermission(Manifest.permission.ACCESS_FINE_LOCATION, Constants.LOCATION_PERMISSION, HomeFragment.this)) {
                    intentToNearByPage();
                }

            }
        });

        questionsTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToQuestionsPage();
            }
        });

        newsTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToNewsPage();
            }
        });
    }


    //===========Get Categories List================================================

    private void getCategoryList() {
        try {
            categoryProgress.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getCategories();
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    categoryProgress.setVisibility(View.GONE);
                    homeListener.onApiCallSuccess();
                    if (response.isSuccessful()) {
                        String jsonResponse = response.body();
                        Log.e("jsonResponse", ":" + jsonResponse);

                        //String categoriesHome = response.body();
                        if (jsonResponse != null) {

                            try {
                                JSONObject jsonObject = new JSONObject(jsonResponse);

                                String status = jsonObject.optString("status");

                                if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONArray jsonArray = jsonObject.optJSONArray("aTerms");

                                    if (jsonArray != null && jsonArray.length() > 0) {

                                        categoriesHomeArrayList.clear();
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            String urlType = null;
                                            String iconUrl = null;

                                            String termId = jsonArray.optJSONObject(i).optString("term_id");
                                            String categoryName = jsonArray.optJSONObject(i).optString("name");
                                            JSONObject iconObject = jsonArray.optJSONObject(i).optJSONObject("oIcon");

                                            if (iconObject != null) {
                                                urlType = iconObject.optString("type");
                                                iconUrl = iconObject.optString("url");
                                            }

                                            categoriesHomeArrayList.add(new CategoriesHome(termId, categoryName, urlType, iconUrl, getSectionTitle(i)));
                                        }
                                    }

                                    adaptCategoriesList();

                                    adaptForSectionHeader();

                                    showCategoryPopup();

                                } else {
                                    Log.e("error", "error response");
                                    enableCategoriesLinear(false);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("error", "null response");
                            enableCategoriesLinear(false);
                        }
                    } else {
                        Log.e("error", "response un successful");
                        enableCategoriesLinear(false);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    categoryProgress.setVisibility(View.GONE);
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        homeListener.onApiCallFailed(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    }
                    enableCategoriesLinear(false);
                }
            });
        } catch (Exception e) {
            categoryProgress.setVisibility(View.GONE);
            Log.e("exp", ":" + e.getMessage());

            enableCategoriesLinear(false);
        }
    }


    private String getSectionTitle(int position) {
        switch (position) {
            case 0:
                return Constants.EXPERIENCE_SEERVICES;
            case 1:
                return Constants.HOT_DEAL_PRODUCTS;
            case 2:
                return Constants.RECOMMENDED_REAL_ESTATES;
            case 3:
                return Constants.HIGH_REVIEW_MACHINERY;
            case 4:
                return Constants.FEATURED_COMPANIES;
            default:
                return "";
        }
    }


    private void adaptCategoriesList() {
        if (categoriesHomeArrayList.size() > 0) {

            enableCategoriesLinear(true);

            HomeIndustryAdapter myAdapter = new HomeIndustryAdapter(getActivity(), categoriesHomeArrayList);
            myAdapter.setOnItemClicked(new OnItemClicked() {
                @Override
                public void onItemClicked(int position) {

                    String termId = "" + categoriesHomeArrayList.get(position).getTermId();
                    String name = "" + categoriesHomeArrayList.get(position).getName();

                    Bundle bundle = new Bundle();
                    bundle.putString("termId", termId);
                    bundle.putString("name", name);

                    intentToCategoryListing(bundle);
                }
            });
            industry_recycler.setAdapter(myAdapter);
        } else {
            enableCategoriesLinear(false);
        }
    }

    private void enableCategoriesLinear(boolean state) {
        if (state) {
            if (categoriesLinear.getVisibility() == View.GONE) {
                categoriesLinear.setVisibility(View.VISIBLE);
            }
        } else {
            if (categoriesLinear.getVisibility() == View.VISIBLE) {
                categoriesLinear.setVisibility(View.GONE);
            }
        }
    }

    private void intentToCategoryListing(Bundle bundle) {
        Intent intent = new Intent(getActivity(), CategoryListingActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }

    //================Home page sections================================================

    private void adaptForSectionHeader() {
        if (categoriesHomeArrayList.size() > 0) {
            SectionHeadAdapter myAdapter = new SectionHeadAdapter(getActivity(), categoriesHomeArrayList);
            myAdapter.setOnItemClicked(new OnItemClicked() {
                @Override
                public void onItemClicked(int position) {

                    String termId = "" + categoriesHomeArrayList.get(position).getTermId();
                    String name = "" + categoriesHomeArrayList.get(position).getName();

                    Bundle bundle = new Bundle();
                    bundle.putString("termId", termId);
                    bundle.putString("name", name);

                    intentToCategoryListing(bundle);
                }
            });
            myAdapter.setSectionClickListener(new SectionClickListener() {
                @Override
                public void onItemClicked(Bundle bundle) {
                    intentToCategoryListingPreview(bundle);
                }

                @Override
                public void onFavouritesClicked(String postId, SparkButton wishListButton) {
                    addToFavorites(postId, wishListButton);
                }

                @Override
                public void onCallClicked(String phoneNumber) {
                    callListener.onCallClicked(phoneNumber);
                }
            });
            sectionHeaderRecycler.setAdapter(myAdapter);
        }
    }


    private void addToFavorites(String postId, SparkButton wishListButton) {
        boolean state = wishListButton.isChecked();
        progressDialog = MyUtils.showProgressLoader(getActivity(), (state ? "Removing.." : "Adding.."));
        FavouritesState favouritesState = new FavouritesState(state);
        favouritesState.setFavouritesListener(new FavouritesListener() {
            @Override
            public void onFavouritesSuccess(String status) {
                MyUtils.dismissProgressLoader(progressDialog);

                if (status.equalsIgnoreCase(Constants.ADD_TO_FAVOURITES)) {
                    wishListButton.setChecked(true);
                } else if (status.equalsIgnoreCase(Constants.REMOVE_FROM_FAVOURITES)) {
                    wishListButton.setChecked(false);
                }
            }

            @Override
            public void onFavouritesFailed(String errorType, String error) {
                MyUtils.dismissProgressLoader(progressDialog);
                homeListener.onApiCallFailed(errorType, error);

                if (state) {
                    wishListButton.setChecked(true);
                } else {
                    wishListButton.setChecked(false);
                }
            }
        });
        favouritesState.addToFavourites(postId, accessToken);
    }


    private void intentToCategoryListingPreview(Bundle bundle) {
        Intent intent = new Intent(getActivity(), ListingDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }


    //===========show category popup================================================================

    private void showCategoryPopup() {
        selectCategoryRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoriesHomeArrayList.size() > 0) {
                    SpinnerDialog spinnerDialog = new SpinnerDialog(categoriesHomeArrayList, new OnItemSelected() {
                        @Override
                        public void onItemSelected(int position) {
                            String categoryId = categoriesHomeArrayList.get(position).getTermId();
                            String categoryName = categoriesHomeArrayList.get(position).getName();

                            Bundle bundle = new Bundle();
                            bundle.putString("termId", categoryId);
                            bundle.putString("name", categoryName);

                            intentToCategoryListing(bundle);
                        }
                    });
                    spinnerDialog.setTitle("Select Category");
                    spinnerDialog.show(getActivity().getSupportFragmentManager(), "dialog");
                }
            }
        });
    }


    //==============Intent to other activities========================================================

    private void intentToSearchPage() {
        Intent intent = new Intent(getActivity(), SearchActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }


    private void intentToQuestionsPage() {
        Intent intent = new Intent(getActivity(), QuestionsActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }

    private void intentToNewsPage() {
        Intent intent = new Intent(getActivity(), NewsActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }

    private void intentToNearByPage() {
        Intent intent = new Intent(getActivity(), NearbyPlacesActivity.class);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }


    //======Handle location permission================================================================

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    intentToNearByPage();
                    return;
                } else {
                    if (getActivity() == null) {
                        return;
                    }
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                        Log.e("permission", "denied");
                        return;
                    } else {
                        Log.e("permission", "completely denied");

                        PermissionAlert permissionAlert = new PermissionAlert(getActivity(), "LOCATION");
                        permissionAlert.show();
                    }
                }
                break;
            default:
                break;
        }
    }


}
