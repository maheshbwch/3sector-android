package my.threesector.android.fragments.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.activities.listing.ListingDetailActivity;
import my.threesector.android.adapters.ProjectListingAdapter;
import my.threesector.android.interfaces.FavouritesListener;
import my.threesector.android.interfaces.HomeListener;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.models.ProjectListing;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.ErrorConstants;
import my.threesector.android.utils.FavouritesState;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.PreferenceHandler;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectFragment extends Fragment {

    private Context context;
    private RecyclerView recyclerView;
    private TextView errorTxt;
    private ProgressBar categoryProgress;
    private ProgressDialog progressDialog = null;

    private ArrayList<ProjectListing> projectListingArrayList = new ArrayList<>();

    private String accessToken = null;

    private HomeListener homeListener = null;

    public ProjectFragment(HomeListener homeListener) {
        this.homeListener = homeListener;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_project, container, false);
        context = getActivity();

        init(rootView);

        return rootView;
    }

    private void init(View view) {
        recyclerView = view.findViewById(R.id.fp_recyclerView);
        errorTxt = view.findViewById(R.id.fp_errorTxt);
        categoryProgress = view.findViewById(R.id.fp_categoryProgress);

        listeners();
    }

    private void listeners() {
        accessToken = PreferenceHandler.getPreferenceFromString(getActivity(), Constants.ACCESS_TOKEN);
        Log.e("accessToken", ":" + accessToken);

        getProjectListing("project", "1"); //TODO PERFORM PAGINATION
    }


    private void getProjectListing(String type, String pageNumber) {
        try {
            categoryProgress.setVisibility(View.VISIBLE);
            ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
            Call<String> call = apiService.getProjectListing(type, pageNumber, (Constants.BEARER + accessToken));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    categoryProgress.setVisibility(View.GONE);
                    homeListener.onApiCallSuccess();
                    String jsonResponse = response.body();
                    Log.e("jsonResponse", ":" + jsonResponse);
                    if (jsonResponse != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);

                            String status = jsonObject.optString("status");

                            if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                JSONArray jsonArray = jsonObject.optJSONArray("oResults");

                                if (jsonArray != null && jsonArray.length() > 0) {

                                    projectListingArrayList.clear();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        String urlType = null;
                                        String iconUrl = null;
                                        String address = null;
                                        String categoryName = null;

                                        String listingId = jsonArray.optJSONObject(i).optString("ID");
                                        String postTitle = jsonArray.optJSONObject(i).optString("postTitle");
                                        String tagLine = jsonArray.optJSONObject(i).optString("tagLine");
                                        String logo = jsonArray.optJSONObject(i).optString("logo");
                                        String coverImg = jsonArray.optJSONObject(i).optString("coverImg");

                                        JSONObject addressObject = jsonArray.optJSONObject(i).optJSONObject("oAddress");
                                        if (addressObject != null) {
                                            address = addressObject.optString("address");
                                        }

                                        JSONObject termObject = jsonArray.optJSONObject(i).optJSONObject("oTerm");
                                        if (termObject != null) {
                                            categoryName = termObject.optString("name");
                                        }

                                        JSONObject iconObject = jsonArray.optJSONObject(i).optJSONObject("oIcon");
                                        if (iconObject != null) {
                                            urlType = iconObject.optString("type");
                                            iconUrl = iconObject.optString("url");
                                        }

                                        projectListingArrayList.add(new ProjectListing(listingId, postTitle, tagLine, logo, coverImg, address, categoryName, urlType, iconUrl));
                                    }

                                    adaptCategoriesList();
                                }
                            } else if (status.equalsIgnoreCase(Constants.API_ERROR)) {
                                String msg = jsonObject.optString("msg");
                                if (MyUtils.checkStringValue(msg) && msg.equalsIgnoreCase(Constants.TOKEN_EXPIRED)) {
                                    homeListener.onApiCallFailed(Constants.TOKEN_SESSION_EXPIRED, ErrorConstants.TOKEN_EXPIRED);
                                    Log.e("error", "access token expired");
                                }else {
                                    showErrorTxt();
                                }
                            } else {
                                Log.e("error", "error response");
                                showErrorTxt();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e("error", "null response");
                        showErrorTxt();
                    }

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    categoryProgress.setVisibility(View.GONE);
                    showErrorTxt();
                    if (t instanceof IOException) {
                        Log.e("error", "message no network:" + t.getMessage());
                        homeListener.onApiCallFailed(Constants.NO_INTERNET, ErrorConstants.BROKEN_INTERNET);
                    }
                    Log.e("error", "message:" + t.getMessage());
                }
            });
        } catch (Exception e) {
            categoryProgress.setVisibility(View.GONE);
            showErrorTxt();
            Log.e("exp", ":" + e.getMessage());
        }
    }


    private void adaptCategoriesList() {
        if (projectListingArrayList.size() > 0) {
            //recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
            recyclerView.setHasFixedSize(true);

            ProjectListingAdapter myAdapter = new ProjectListingAdapter(getActivity(), projectListingArrayList);
            myAdapter.setListingClickListeners(new ListingClickListeners() {
                @Override
                public void onItemClicked(int position) {

                    String listingId = "" + projectListingArrayList.get(position).getListingId();
                    String listingTitle = "" + projectListingArrayList.get(position).getPostTitle();
                    String logo = "" + projectListingArrayList.get(position).getLogo();
                    String coverImage = "" + projectListingArrayList.get(position).getCoverImg();

                    Bundle bundle = new Bundle();
                    bundle.putString("listingId", listingId);
                    bundle.putString("listingTitle", listingTitle);
                    bundle.putString("logo", logo);
                    bundle.putString("coverImage", coverImage);

                    intentToCategoryListingPreview(bundle);
                }

                @Override
                public void onFavouritesClicked(int position, SparkButton wishListButton) {

                    String postId = "" + projectListingArrayList.get(position).getListingId();

                    addToFavorites(postId, wishListButton);
                }

                @Override
                public void onCallClicked(String phoneNumber) {

                }
            });
            recyclerView.setAdapter(myAdapter);
        } else {
            showErrorTxt();
        }
    }

    private void addToFavorites(String postId, SparkButton wishListButton) {
        boolean state = wishListButton.isChecked();
        progressDialog = MyUtils.showProgressLoader(context, (state ? "Removing.." : "Adding.."));
        FavouritesState favouritesState = new FavouritesState(state);
        favouritesState.setFavouritesListener(new FavouritesListener() {
            @Override
            public void onFavouritesSuccess(String status) {
                MyUtils.dismissProgressLoader(progressDialog);

                if (status.equalsIgnoreCase(Constants.ADD_TO_FAVOURITES)) {
                    wishListButton.setChecked(true);
                } else if (status.equalsIgnoreCase(Constants.REMOVE_FROM_FAVOURITES)) {
                    wishListButton.setChecked(false);
                }
            }

            @Override
            public void onFavouritesFailed(String errorType, String error) {
                MyUtils.dismissProgressLoader(progressDialog);
                //showBaseRelative(errorType, error);
                if (state) {
                    wishListButton.setChecked(true);
                } else {
                    wishListButton.setChecked(false);
                }
            }
        });
        favouritesState.addToFavourites(postId, accessToken);
    }


    private void intentToCategoryListingPreview(Bundle bundle) {
        Intent intent = new Intent(context, ListingDetailActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        MyUtils.openOverrideAnimation(true, getActivity());
    }


    private void showErrorTxt() {
        if (errorTxt.getVisibility() == View.GONE) {
            errorTxt.setVisibility(View.VISIBLE);
            errorTxt.setText(ErrorConstants.PROJECT_LISTING_ERROR);
        }
    }


}
