package my.threesector.android.fragments.sign_up;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.adapters.PackagesListAdapter;
import my.threesector.android.interfaces.SignUpNavigationListener;
import my.threesector.android.models.PackageModel;

public class CompanyFragment extends Fragment {

    private TextInputEditText companyNameEdt;
    private RelativeLayout categoryDropDownRelative;
    private TextView categoryDropDownTxt, goBackBtn, nextStepBtn;
    private RecyclerView packageRecyclerView;

    private SignUpNavigationListener signUpNavigationListener;
    private boolean isFreeSignUpProcess = false;

    public CompanyFragment(SignUpNavigationListener signUpNavigationListener) {
        this.signUpNavigationListener = signUpNavigationListener;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_company_details, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View view) {
        companyNameEdt = view.findViewById(R.id.fcd_companyNameEdt);
        categoryDropDownRelative = view.findViewById(R.id.fcd_categoryDropDownRelative);
        categoryDropDownTxt = view.findViewById(R.id.fcd_categoryDropDownTxt);
        packageRecyclerView = view.findViewById(R.id.packageRecyclerView);
        goBackBtn = view.findViewById(R.id.fcd_goBackBtn);
        nextStepBtn = view.findViewById(R.id.fcd_nextStepBtn);

        listeners();
    }

    private void listeners() {
        goBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpNavigationListener.onBackButtonClicked();
            }
        });

        nextStepBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpNavigationListener.onNextButtonClicked();
            }
        });

        updatePackagesList();
    }


    private void updatePackagesList() {
        ArrayList<PackageModel> packageModelArrayList = new ArrayList<>();
        packageModelArrayList.add(new PackageModel("Package1", "STANDARD TIER", true));
        packageModelArrayList.add(new PackageModel("Package2", "ADVANCED TIER", false));
        packageModelArrayList.add(new PackageModel("Package3", "PROFESSIONAL TIER", false));
        packageModelArrayList.add(new PackageModel("Package4", "GLOBAL TIER", false));

        packageRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        packageRecyclerView.setHasFixedSize(true);

        if (packageModelArrayList.size() > 0) {
            PackagesListAdapter packagesListAdapter = new PackagesListAdapter(getActivity(), packageModelArrayList);
            packageRecyclerView.setAdapter(packagesListAdapter);
        }
    }

}