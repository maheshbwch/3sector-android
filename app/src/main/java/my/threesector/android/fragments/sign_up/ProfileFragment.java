package my.threesector.android.fragments.sign_up;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import my.threesector.android.R;
import my.threesector.android.interfaces.SignUpNavigationListener;
import my.threesector.android.utils.MyUtils;

public class ProfileFragment extends Fragment {

    private LinearLayout signUpLinear;
    private CheckBox termsCheckBox;

    private SignUpNavigationListener signUpNavigationListener;


    public ProfileFragment(SignUpNavigationListener signUpNavigationListener) {
        this.signUpNavigationListener = signUpNavigationListener;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View view) {
        termsCheckBox = view.findViewById(R.id.fp_termsCheckBox);
        signUpLinear = view.findViewById(R.id.fp_signUpLinear);
        listeners();
    }

    private void listeners() {
        Typeface typeface = MyUtils.getMedBoldFont(getActivity());
        termsCheckBox.setTypeface(typeface);

        signUpLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpNavigationListener.onNextButtonClicked();
            }
        });

    }

}