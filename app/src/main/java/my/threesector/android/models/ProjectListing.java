package my.threesector.android.models;

public class ProjectListing {

    private String listingId;
    private String postTitle;
    private String tagLine;
    private String logo;
    private String coverImg;
    private String address;
    private String categoryName;
    private String urlType;
    private String iconUrl;

    public ProjectListing(String listingId, String postTitle, String tagLine, String logo, String coverImg, String address, String categoryName, String urlType, String iconUrl) {
        this.listingId = listingId;
        this.postTitle = postTitle;
        this.tagLine = tagLine;
        this.logo = logo;
        this.coverImg = coverImg;
        this.address = address;
        this.categoryName = categoryName;
        this.urlType = urlType;
        this.iconUrl = iconUrl;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getTagLine() {
        return tagLine;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
