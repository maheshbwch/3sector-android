package my.threesector.android.models;

public class ChatUsers {

    String sender_id;
    String sender_name;
    String sender_image;
    String receiver_id;
    String receiver_name;
    String receiver_image;
    String message_key;
    String date;

    public ChatUsers() {

    }

    public ChatUsers(String sender_id, String sender_name, String sender_image, String receiver_id, String receiver_name, String receiver_image, String message_key, String date) {
        this.sender_id = sender_id;
        this.sender_name = sender_name;
        this.sender_image = sender_image;
        this.receiver_id = receiver_id;
        this.receiver_name = receiver_name;
        this.receiver_image = receiver_image;
        this.message_key = message_key;
        this.date = date;
    }


    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getSender_image() {
        return sender_image;
    }

    public void setSender_image(String sender_image) {
        this.sender_image = sender_image;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public String getReceiver_image() {
        return receiver_image;
    }

    public void setReceiver_image(String receiver_image) {
        this.receiver_image = receiver_image;
    }

    public String getMessage_key() {
        return message_key;
    }

    public void setMessage_key(String message_key) {
        this.message_key = message_key;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
