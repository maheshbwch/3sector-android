package my.threesector.android.models;

public class News {

    String authorImage;
    String authorName;
    String newsTitle;
    String newsImageUrl;
    String postDate;

    public News(String authorImage, String authorName, String newsTitle, String newsImageUrl, String postDate) {
        this.authorImage = authorImage;
        this.authorName = authorName;
        this.newsTitle = newsTitle;
        this.newsImageUrl = newsImageUrl;
        this.postDate = postDate;
    }

    public String getAuthorImage() {
        return authorImage;
    }

    public void setAuthorImage(String authorImage) {
        this.authorImage = authorImage;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsImageUrl() {
        return newsImageUrl;
    }

    public void setNewsImageUrl(String newsImageUrl) {
        this.newsImageUrl = newsImageUrl;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }
}
