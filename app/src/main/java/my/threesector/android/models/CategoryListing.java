package my.threesector.android.models;

public class CategoryListing {

    private String listingId;
    private String postTitle;
    private String tagLine;
    private String phoneNumber;
    private String logo;
    private String coverImg;
    private String address;
    private String latitude;
    private String longitude;
    private String businessStatus;
    private int mode;
    private int average;
    private String categoryName;
    private String urlType;
    private String iconUrl;

    public CategoryListing(String listingId, String postTitle, String tagLine, String phoneNumber, String logo, String coverImg, String address, String latitude, String longitude, String businessStatus, int mode, int average, String categoryName, String urlType, String iconUrl) {
        this.listingId = listingId;
        this.postTitle = postTitle;
        this.tagLine = tagLine;
        this.phoneNumber = phoneNumber;
        this.logo = logo;
        this.coverImg = coverImg;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.businessStatus = businessStatus;
        this.mode = mode;
        this.average = average;
        this.categoryName = categoryName;
        this.urlType = urlType;
        this.iconUrl = iconUrl;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getTagLine() {
        return tagLine;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBusinessStatus() {
        return businessStatus;
    }

    public void setBusinessStatus(String businessStatus) {
        this.businessStatus = businessStatus;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getAverage() {
        return average;
    }

    public void setAverage(int average) {
        this.average = average;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }



    /*@SerializedName("status")
    @Expose
    private String status;
    @SerializedName("maxPages")
    @Expose
    private Integer maxPages;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("next")
    @Expose
    private Boolean next;
    @SerializedName("oResults")
    @Expose
    private ArrayList<OResult> oResults = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getMaxPages() {
        return maxPages;
    }

    public void setMaxPages(Integer maxPages) {
        this.maxPages = maxPages;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Boolean getNext() {
        return next;
    }

    public void setNext(Boolean next) {
        this.next = next;
    }

    public ArrayList<OResult> getOResults() {
        return oResults;
    }

    public void setOResults(ArrayList<OResult> oResults) {
        this.oResults = oResults;
    }


    public class OAddress {
        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lng")
        @Expose
        private String lng;
        @SerializedName("address")
        @Expose
        private String address;

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }


    public class OAuthor {
        @SerializedName("ID")
        @Expose
        private String iD;
        @SerializedName("displayName")
        @Expose
        private String displayName;
        @SerializedName("avatar")
        @Expose
        private String avatar;

        public String getID() {
            return iD;
        }

        public void setID(String iD) {
            this.iD = iD;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }


    public class OFavorite {

        @SerializedName("isMyFavorite")
        @Expose
        private Boolean isMyFavorite;
        @SerializedName("totalFavorites")
        @Expose
        private Integer totalFavorites;
        @SerializedName("text")
        @Expose
        private String text;

        public Boolean getIsMyFavorite() {
            return isMyFavorite;
        }

        public void setIsMyFavorite(Boolean isMyFavorite) {
            this.isMyFavorite = isMyFavorite;
        }

        public Integer getTotalFavorites() {
            return totalFavorites;
        }

        public void setTotalFavorites(Integer totalFavorites) {
            this.totalFavorites = totalFavorites;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

    }

    public class OFeaturedImg {

        @SerializedName("large")
        @Expose
        private String large;
        @SerializedName("medium")
        @Expose
        private String medium;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;

        public String getLarge() {
            return large;
        }

        public void setLarge(String large) {
            this.large = large;
        }

        public String getMedium() {
            return medium;
        }

        public void setMedium(String medium) {
            this.medium = medium;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

    }

    public class OPriceRange {

        @SerializedName("mode")
        @Expose
        private String mode;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("minimumPrice")
        @Expose
        private String minimumPrice;
        @SerializedName("maximumPrice")
        @Expose
        private String maximumPrice;
        @SerializedName("currency")
        @Expose
        private String currency;

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMinimumPrice() {
            return minimumPrice;
        }

        public void setMinimumPrice(String minimumPrice) {
            this.minimumPrice = minimumPrice;
        }

        public String getMaximumPrice() {
            return maximumPrice;
        }

        public void setMaximumPrice(String maximumPrice) {
            this.maximumPrice = maximumPrice;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

    }


    public class OResult {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("postTitle")
        @Expose
        private String postTitle;
        @SerializedName("postLink")
        @Expose
        private String postLink;
        @SerializedName("tagLine")
        @Expose
        private String tagLine;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("logo")
        @Expose
        private String logo;
        *//*@SerializedName("oVideos")
        @Expose
        private ArrayList<OVideo> oVideos = new ArrayList<>();*//*
        @SerializedName("timezone")
        @Expose
        private String timezone;
        @SerializedName("coverImg")
        @Expose
        private String coverImg;
        @SerializedName("oAddress")
        @Expose
        private OAddress oAddress;
        @SerializedName("oFeaturedImg")
        @Expose
        private OFeaturedImg oFeaturedImg;
       *//* @SerializedName("businessStatus")
        @Expose
        private String businessStatus;*//*
     *//*@SerializedName("oPriceRange")
        @Expose
        private OPriceRange oPriceRange;*//*
        @SerializedName("claimStatus")
        @Expose
        private String claimStatus;
        *//*@SerializedName("oSocialNetworks")
        @Expose
        private OSocialNetworks oSocialNetworks;*//*
     *//*@SerializedName("oGallery")
        @Expose
        private OGallery oGallery;*//*
        @SerializedName("oCustomSettings")
        @Expose
        private String oCustomSettings;
        @SerializedName("oReview")
        @Expose
        private OReview oReview;
        @SerializedName("oFavorite")
        @Expose
        private OFavorite oFavorite;
        @SerializedName("oAuthor")
        @Expose
        private OAuthor oAuthor;
        @SerializedName("isReport")
        @Expose
        private Boolean isReport;
        @SerializedName("isReview")
        @Expose
        private Boolean isReview;
        @SerializedName("oTerm")
        @Expose
        private OTerm oTerm;
        @SerializedName("oIcon")
        @Expose
        private OIcon oIcon;

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getPostTitle() {
            return postTitle;
        }

        public void setPostTitle(String postTitle) {
            this.postTitle = postTitle;
        }

        public String getPostLink() {
            return postLink;
        }

        public void setPostLink(String postLink) {
            this.postLink = postLink;
        }

        public String getTagLine() {
            return tagLine;
        }

        public void setTagLine(String tagLine) {
            this.tagLine = tagLine;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

       *//* public String getOVideos() {
            return oVideos;
        }

        public void setOVideos(String oVideos) {
            this.oVideos = oVideos;
        }*//*

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

        public String getCoverImg() {
            return coverImg;
        }

        public void setCoverImg(String coverImg) {
            this.coverImg = coverImg;
        }

        public OAddress getOAddress() {
            return oAddress;
        }

        public void setOAddress(OAddress oAddress) {
            this.oAddress = oAddress;
        }

        public OFeaturedImg getOFeaturedImg() {
            return oFeaturedImg;
        }

        public void setOFeaturedImg(OFeaturedImg oFeaturedImg) {
            this.oFeaturedImg = oFeaturedImg;
        }

      *//*  public String getBusinessStatus() {
            return businessStatus;
        }

        public void setBusinessStatus(String businessStatus) {
            this.businessStatus = businessStatus;
        }*//*

     *//*public OPriceRange getOPriceRange() {
            return oPriceRange;
        }

        public void setOPriceRange(OPriceRange oPriceRange) {
            this.oPriceRange = oPriceRange;
        }*//*

        public String getClaimStatus() {
            return claimStatus;
        }

        public void setClaimStatus(String claimStatus) {
            this.claimStatus = claimStatus;
        }

        *//*public OSocialNetworks getOSocialNetworks() {
            return oSocialNetworks;
        }

        public void setOSocialNetworks(OSocialNetworks oSocialNetworks) {
            this.oSocialNetworks = oSocialNetworks;
        }*//*

        public String getOCustomSettings() {
            return oCustomSettings;
        }

        public void setOCustomSettings(String oCustomSettings) {
            this.oCustomSettings = oCustomSettings;
        }

        public OReview getOReview() {
            return oReview;
        }

        public void setOReview(OReview oReview) {
            this.oReview = oReview;
        }

        public OFavorite getOFavorite() {
            return oFavorite;
        }

        public void setOFavorite(OFavorite oFavorite) {
            this.oFavorite = oFavorite;
        }

        public OAuthor getOAuthor() {
            return oAuthor;
        }

        public void setOAuthor(OAuthor oAuthor) {
            this.oAuthor = oAuthor;
        }

        public Boolean getIsReport() {
            return isReport;
        }

        public void setIsReport(Boolean isReport) {
            this.isReport = isReport;
        }

        public Boolean getIsReview() {
            return isReview;
        }

        public void setIsReview(Boolean isReview) {
            this.isReview = isReview;
        }

        public OTerm getOTerm() {
            return oTerm;
        }

        public void setOTerm(OTerm oTerm) {
            this.oTerm = oTerm;
        }

        public OIcon getOIcon() {
            return oIcon;
        }

        public void setOIcon(OIcon oIcon) {
            this.oIcon = oIcon;
        }

    }

    public class OVideo {

        @SerializedName("src")
        @Expose
        private String src;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;

        public String getSrc() {
            return src;
        }

        public void setSrc(String src) {
            this.src = src;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

    }


    public class OIcon {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("url")
        @Expose
        private String url;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }


    public class OReview {

        @SerializedName("mode")
        @Expose
        private Integer mode = 0;
        @SerializedName("average")
        @Expose
        private Integer average = 0;
        @SerializedName("quality")
        @Expose
        private String quality;

        public Integer getMode() {
            return mode;
        }

        public void setMode(Integer mode) {
            this.mode = mode;
        }

        public Integer getAverage() {
            return average;
        }

        public void setAverage(Integer average) {
            this.average = average;
        }

        public String getQuality() {
            return quality;
        }

        public void setQuality(String quality) {
            this.quality = quality;
        }

    }


    public class OSocialNetworks {

        @SerializedName("facebook")
        @Expose
        private String facebook;
        @SerializedName("twitter")
        @Expose
        private String twitter;
        @SerializedName("google-plus")
        @Expose
        private String googlePlus;
        @SerializedName("tumblr")
        @Expose
        private String tumblr;
        @SerializedName("vk")
        @Expose
        private String vk;
        @SerializedName("odnoklassniki")
        @Expose
        private String odnoklassniki;
        @SerializedName("youtube")
        @Expose
        private String youtube;
        @SerializedName("vimeo")
        @Expose
        private String vimeo;

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getTwitter() {
            return twitter;
        }

        public void setTwitter(String twitter) {
            this.twitter = twitter;
        }

        public String getGooglePlus() {
            return googlePlus;
        }

        public void setGooglePlus(String googlePlus) {
            this.googlePlus = googlePlus;
        }

        public String getTumblr() {
            return tumblr;
        }

        public void setTumblr(String tumblr) {
            this.tumblr = tumblr;
        }

        public String getVk() {
            return vk;
        }

        public void setVk(String vk) {
            this.vk = vk;
        }

        public String getOdnoklassniki() {
            return odnoklassniki;
        }

        public void setOdnoklassniki(String odnoklassniki) {
            this.odnoklassniki = odnoklassniki;
        }

        public String getYoutube() {
            return youtube;
        }

        public void setYoutube(String youtube) {
            this.youtube = youtube;
        }

        public String getVimeo() {
            return vimeo;
        }

        public void setVimeo(String vimeo) {
            this.vimeo = vimeo;
        }

    }


    public class OTerm {

        @SerializedName("term_id")
        @Expose
        private Integer termId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("term_group")
        @Expose
        private Integer termGroup;
        @SerializedName("term_taxonomy_id")
        @Expose
        private Integer termTaxonomyId;
        @SerializedName("taxonomy")
        @Expose
        private String taxonomy;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("parent")
        @Expose
        private Integer parent;
        @SerializedName("count")
        @Expose
        private Integer count;
        @SerializedName("filter")
        @Expose
        private String filter;

        public Integer getTermId() {
            return termId;
        }

        public void setTermId(Integer termId) {
            this.termId = termId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public Integer getTermGroup() {
            return termGroup;
        }

        public void setTermGroup(Integer termGroup) {
            this.termGroup = termGroup;
        }

        public Integer getTermTaxonomyId() {
            return termTaxonomyId;
        }

        public void setTermTaxonomyId(Integer termTaxonomyId) {
            this.termTaxonomyId = termTaxonomyId;
        }

        public String getTaxonomy() {
            return taxonomy;
        }

        public void setTaxonomy(String taxonomy) {
            this.taxonomy = taxonomy;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getParent() {
            return parent;
        }

        public void setParent(Integer parent) {
            this.parent = parent;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

    }*/

    /*public class OGallery {

        @SerializedName("large")
        @Expose
        private ArrayList<Large> large = new ArrayList<>();
        @SerializedName("medium")
        @Expose
        private ArrayList<Medium> medium = new ArrayList<>();

        public ArrayList<Large> getLarge() {
            return large;
        }

        public void setLarge(ArrayList<Large> large) {
            this.large = large;
        }

        public ArrayList<Medium> getMedium() {
            return medium;
        }

        public void setMedium(ArrayList<Medium> medium) {
            this.medium = medium;
        }

    }


    public class Medium {

        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }
    public class Large {

        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("id")
        @Expose
        private Integer id;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

    }*/

}
