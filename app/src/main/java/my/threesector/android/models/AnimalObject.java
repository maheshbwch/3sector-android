package my.threesector.android.models;

public class AnimalObject {

    public String name;
    public String type;

    public AnimalObject(final String name, final String type){

        this.name = name ;
        this.type = type ;
    }

}
