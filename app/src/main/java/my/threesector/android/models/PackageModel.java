package my.threesector.android.models;

public class PackageModel {

    String packageSerialNumber;
    String packageName;
    private boolean isSelected = false;

    public PackageModel(String packageSerialNumber, String packageName, boolean isSelected) {
        this.packageSerialNumber = packageSerialNumber;
        this.packageName = packageName;
        this.isSelected = isSelected;
    }

    public String getPackageSerialNumber() {
        return packageSerialNumber;
    }

    public void setPackageSerialNumber(String packageSerialNumber) {
        this.packageSerialNumber = packageSerialNumber;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
