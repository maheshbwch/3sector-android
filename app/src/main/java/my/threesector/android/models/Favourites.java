package my.threesector.android.models;

public class Favourites {

    private String listingId;
    private String postTitle;
    private String tagLine;
    private String iconUrl;

    public Favourites(String listingId, String postTitle,String tagLine, String iconUrl) {
        this.listingId = listingId;
        this.postTitle = postTitle;
        this.tagLine = tagLine;
        this.iconUrl = iconUrl;
    }

    public String getTagLine() {
        return tagLine;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
