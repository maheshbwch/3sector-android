package my.threesector.android.models;

public class CategoriesHome {

    String termId;
    String name;
    String type;
    String url;
    String sectionTitle;

    public CategoriesHome(String termId, String name, String type, String url, String sectionTitle) {
        this.termId = termId;
        this.name = name;
        this.type = type;
        this.url = url;
        this.sectionTitle = sectionTitle;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }



    @Override
    public String toString() {
        return name;
    }


/*@SerializedName("status")
    @Expose
    private String status;
    @SerializedName("aTerms")
    @Expose
    private ArrayList<ATerm> aTerms = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ATerm> getATerms() {
        return aTerms;
    }

    public void setATerms(ArrayList<ATerm> aTerms) {
        this.aTerms = aTerms;
    }


    public class ATerm {

        @SerializedName("term_id")
        @Expose
        private Integer termId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("term_group")
        @Expose
        private Integer termGroup;
        @SerializedName("term_taxonomy_id")
        @Expose
        private Integer termTaxonomyId;
        @SerializedName("taxonomy")
        @Expose
        private String taxonomy;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("parent")
        @Expose
        private Integer parent;
        @SerializedName("count")
        @Expose
        private Integer count;
        @SerializedName("filter")
        @Expose
        private String filter;
        @SerializedName("featuredImg")
        @Expose
        private String featuredImg;
        @SerializedName("oIcon")
        @Expose
        //@Expose(deserialize = false, serialize = false)
        private transient OIcon oIcon;

        public Integer getTermId() {
            return termId;
        }

        public void setTermId(Integer termId) {
            this.termId = termId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public Integer getTermGroup() {
            return termGroup;
        }

        public void setTermGroup(Integer termGroup) {
            this.termGroup = termGroup;
        }

        public Integer getTermTaxonomyId() {
            return termTaxonomyId;
        }

        public void setTermTaxonomyId(Integer termTaxonomyId) {
            this.termTaxonomyId = termTaxonomyId;
        }

        public String getTaxonomy() {
            return taxonomy;
        }

        public void setTaxonomy(String taxonomy) {
            this.taxonomy = taxonomy;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getParent() {
            return parent;
        }

        public void setParent(Integer parent) {
            this.parent = parent;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public String getFeaturedImg() {
            return featuredImg;
        }

        public void setFeaturedImg(String featuredImg) {
            this.featuredImg = featuredImg;
        }

        public OIcon getOIcon() {
            return oIcon;
        }

        public void setOIcon(OIcon oIcon) {
            this.oIcon = oIcon;
        }
    }


    public class OIcon {

        @SerializedName("type")
        @Expose
        private String type = null;
        @SerializedName("url")
        @Expose
        private String url = "";

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }
*/
}
