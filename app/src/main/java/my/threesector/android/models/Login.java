package my.threesector.android.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("return")
    @Expose
    private String ret;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("oUserInfo")
    @Expose
    private OUserInfo oUserInfo;


    public String getRet() {
        return ret;
    }

    public void setRet(String ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public OUserInfo getOUserInfo() {
        return oUserInfo;
    }

    public void setOUserInfo(OUserInfo oUserInfo) {
        this.oUserInfo = oUserInfo;
    }


    public class OUserInfo {

        @SerializedName("userID")
        @Expose
        private Integer userID;
        @SerializedName("displayName")
        @Expose
        private String displayName;
        @SerializedName("userName")
        @Expose
        private String userName;
        @SerializedName("avatar")
        @Expose
        private String avatar;
        @SerializedName("position")
        @Expose
        private String position;
        @SerializedName("coverImg")
        @Expose
        private String coverImg;

        @SerializedName("role")
        @Expose
        private String role;

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public Integer getUserID() {
            return userID;
        }

        public void setUserID(Integer userID) {
            this.userID = userID;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getCoverImg() {
            return coverImg;
        }

        public void setCoverImg(String coverImg) {
            this.coverImg = coverImg;
        }

    }

}
