package my.threesector.android.models;

public class ConvMessages {

    String senderId;
    String senderName;
    String senderImage;
    String receiverId;
    String receiverName;
    String receiverImage;
    boolean isMessageOwner;
    ChatMessages chatMessages;

    public ConvMessages(String senderId, String senderName, String senderImage, String receiverId, String receiverName, String receiverImage, boolean isMessageOwner, ChatMessages chatMessages) {
        this.senderId = senderId;
        this.senderName = senderName;
        this.senderImage = senderImage;
        this.receiverId = receiverId;
        this.receiverName = receiverName;
        this.receiverImage = receiverImage;
        this.isMessageOwner = isMessageOwner;
        this.chatMessages = chatMessages;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderImage() {
        return senderImage;
    }

    public void setSenderImage(String senderImage) {
        this.senderImage = senderImage;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverImage() {
        return receiverImage;
    }

    public void setReceiverImage(String receiverImage) {
        this.receiverImage = receiverImage;
    }

    public boolean isMessageOwner() {
        return isMessageOwner;
    }

    public void setMessageOwner(boolean messageOwner) {
        isMessageOwner = messageOwner;
    }

    public ChatMessages getChatMessages() {
        return chatMessages;
    }

    public void setChatMessages(ChatMessages chatMessages) {
        this.chatMessages = chatMessages;
    }


}
