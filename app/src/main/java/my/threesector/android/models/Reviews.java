package my.threesector.android.models;

public class Reviews {

    String reviewId;
    String userProfileImage;
    String userProfileName;
    String title;
    String content;
    String date;
    double quality;
    double overAll;

    public Reviews(String reviewId, String userProfileImage, String userProfileName, String title, String content, String date, double quality, double overAll) {
        this.reviewId = reviewId;
        this.userProfileImage = userProfileImage;
        this.userProfileName = userProfileName;
        this.title = title;
        this.content = content;
        this.date = date;
        this.quality = quality;
        this.overAll = overAll;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public void setUserProfileImage(String userProfileImage) {
        this.userProfileImage = userProfileImage;
    }

    public String getUserProfileName() {
        return userProfileName;
    }

    public void setUserProfileName(String userProfileName) {
        this.userProfileName = userProfileName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getQuality() {
        return quality;
    }

    public void setQuality(double quality) {
        this.quality = quality;
    }

    public double getOverAll() {
        return overAll;
    }

    public void setOverAll(double overAll) {
        this.overAll = overAll;
    }


}
