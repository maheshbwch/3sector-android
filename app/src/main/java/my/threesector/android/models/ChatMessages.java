package my.threesector.android.models;

public class ChatMessages {

    String message_text;
    String message_key;
    String sender_id;
    String type;
    String message_date;

    public ChatMessages() {
    }

    public ChatMessages(String message_text, String message_key, String sender_id, String type, String message_date) {
        this.message_text = message_text;
        this.message_key = message_key;
        this.sender_id = sender_id;
        this.type = type;
        this.message_date = message_date;
    }

    public String getMessage_text() {
        return message_text;
    }

    public void setMessage_text(String message_text) {
        this.message_text = message_text;
    }

    public String getMessage_key() {
        return message_key;
    }

    public void setMessage_key(String message_key) {
        this.message_key = message_key;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage_date() {
        return message_date;
    }

    public void setMessage_date(String message_date) {
        this.message_date = message_date;
    }


}
