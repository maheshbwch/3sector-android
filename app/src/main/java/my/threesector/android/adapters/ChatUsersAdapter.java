package my.threesector.android.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.interfaces.OnItemClicked;
import my.threesector.android.models.ChatUsers;
import my.threesector.android.utils.MyUtils;

public class ChatUsersAdapter extends RecyclerView.Adapter<ChatUsersAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ChatUsers> arrayList = new ArrayList<>();
    private OnItemClicked onItemClicked = null;

    public void setOnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }

    public ChatUsersAdapter(Context context, ArrayList<ChatUsers> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_chat_users, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String chatUserPicture = arrayList.get(position).getReceiver_image();
        String chatUserName = arrayList.get(position).getReceiver_name();
        String timeStampDate = arrayList.get(position).getDate();

        String formattedDate = MyUtils.checkStringValue(timeStampDate) ? MyUtils.getDateFromTimeStamp(Long.parseLong(timeStampDate)) : "";

        holder.chatUserNameTxt.setText(MyUtils.checkStringValue(chatUserName) ? chatUserName : "-");
        holder.chatDateTxt.setText(formattedDate);

        Glide.with(context).load(chatUserPicture).placeholder(R.drawable.user_pic).into(holder.profileImageView);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public CircleImageView profileImageView;
        public TextView chatUserNameTxt,chatDateTxt/*, chatMessageTxt*/;

        public ViewHolder(View itemView) {
            super(itemView);

            this.profileImageView = itemView.findViewById(R.id.icu_profileImageView);
            this.chatUserNameTxt = itemView.findViewById(R.id.icu_chatUserNameTxt);
            //this.chatMessageTxt = itemView.findViewById(R.id.icu_chatMessageTxt);
            this.chatDateTxt = itemView.findViewById(R.id.icu_chatDateTxt);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClicked.onItemClicked(getAdapterPosition());
                }
            });




        }


    }
}