package my.threesector.android.adapters;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.models.CategoryListing;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;

public class ListingAdapter extends RecyclerView.Adapter<ListingAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CategoryListing> arrayList;
    private ListingClickListeners listingClickListeners = null;


    public ListingAdapter(Context context, ArrayList<CategoryListing> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setListingClickListeners(ListingClickListeners listingClickListeners) {
        this.listingClickListeners = listingClickListeners;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_category_listing, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String profileUrl = arrayList.get(position).getLogo();
        String categoryListingName = arrayList.get(position).getPostTitle();
        String phoneNumber = arrayList.get(position).getPhoneNumber();
        int mode = arrayList.get(position).getMode();
        int average = arrayList.get(position).getAverage();
        String categoryName = arrayList.get(position).getCategoryName();
        String description = arrayList.get(position).getTagLine();
        String address = arrayList.get(position).getAddress();
        //boolean isMyFavorite = arrayList.get(position).getOFavorite().getIsMyFavorite();


        MyUtils.viewHtmlTxt(categoryListingName,holder.listingNameTxt);


        holder.ratingBar.setRating((float) average);
        holder.ratingNumberTxt.setText("" + average + "/" + mode);
        holder.categoryNameTxt.setText(MyUtils.checkStringValue(categoryName) ? categoryName : "-");
        holder.descriptionTxt.setText(MyUtils.checkStringValue(description) ? description : "-");
        holder.addressTxt.setText(MyUtils.checkStringValue(address) ? address : "-");
        holder.phoneNumberTxt.setText(MyUtils.checkStringValue(phoneNumber) ? phoneNumber : "-");

        if (MyUtils.checkStringValue(profileUrl)) {
            Glide.with(context).load(profileUrl).placeholder(R.mipmap.app_logo).into(holder.profileImageView);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView listingNameTxt, ratingNumberTxt, categoryNameTxt, descriptionTxt, addressTxt, phoneNumberTxt;
        public RatingBar ratingBar;
        public PorterShapeImageView profileImageView;
        public LinearLayout phoneLinear;
        public SparkButton wishListButton;

        public ViewHolder(View itemView) {
            super(itemView);
            this.profileImageView = itemView.findViewById(R.id.icl_listingProfileImage);
            this.listingNameTxt = itemView.findViewById(R.id.icl_listingNameTxt);
            this.ratingBar = itemView.findViewById(R.id.icl_ratingBar);
            this.ratingNumberTxt = itemView.findViewById(R.id.icl_ratingNumberTxt);
            this.categoryNameTxt = itemView.findViewById(R.id.icl_categoryNameTxt);
            this.descriptionTxt = itemView.findViewById(R.id.icl_descriptionTxt);
            this.addressTxt = itemView.findViewById(R.id.icl_addressTxt);
            this.phoneLinear = itemView.findViewById(R.id.icl_phoneLinear);
            this.phoneNumberTxt = itemView.findViewById(R.id.icl_phoneNumberTxt);
            this.wishListButton = itemView.findViewById(R.id.icl_wishListButton);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listingClickListeners != null) {
                        listingClickListeners.onItemClicked(getAdapterPosition());
                    }
                }
            });

            wishListButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listingClickListeners != null) {
                        listingClickListeners.onFavouritesClicked(getAdapterPosition(), wishListButton);
                    }
                }
            });

            phoneLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String phoneNumber = arrayList.get(getAdapterPosition()).getPhoneNumber();
                    if (MyUtils.checkStringValue(phoneNumber)) {
                        listingClickListeners.onCallClicked(phoneNumber);
                    }
                }
            });


        }
    }
}