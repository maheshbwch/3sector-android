package my.threesector.android.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.models.Reviews;
import my.threesector.android.utils.MyUtils;

public class ReviewsListAdapter extends RecyclerView.Adapter<ReviewsListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Reviews> arrayList;

    public ReviewsListAdapter(Context context, ArrayList<Reviews> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_reviews_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String profileName = arrayList.get(position).getUserProfileName();
        String profileImageView = arrayList.get(position).getUserProfileImage();
        String date = arrayList.get(position).getDate();
        String title = arrayList.get(position).getTitle();
        String content = arrayList.get(position).getContent();

        double quality = arrayList.get(position).getQuality();
        double overAll = arrayList.get(position).getOverAll();

        holder.nameTxt.setText(MyUtils.checkStringValue(profileName) ? profileName : "-");
        holder.dateTxt.setText(MyUtils.checkStringValue(date) ? date : "-");
        holder.reviewTitleTxt.setText(MyUtils.checkStringValue(title) ? title : "-");
        holder.reviewDescriptionTxt.setText(MyUtils.checkStringValue(content) ? content : "-");

        holder.qualityRatingNumberTxt.setText("" + quality);
        holder.qualityRatingBar.setRating((float) quality);

        holder.overallRatingNumberTxt.setText("" + overAll);
        holder.overallRatingBar.setRating((float) overAll);

        Glide.with(context).load(profileImageView).placeholder(R.mipmap.app_logo).into(holder.profilePicImage);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public CircleImageView profilePicImage;
        public TextView nameTxt, dateTxt, reviewTitleTxt, reviewDescriptionTxt, qualityRatingNumberTxt, overallRatingNumberTxt;
        private RatingBar qualityRatingBar, overallRatingBar;

        public ViewHolder(View itemView) {
            super(itemView);
            this.profilePicImage = itemView.findViewById(R.id.irl_profilePicImage);
            this.nameTxt = itemView.findViewById(R.id.irl_nameTxt);
            this.dateTxt = itemView.findViewById(R.id.irl_dateTxt);
            this.reviewTitleTxt = itemView.findViewById(R.id.irl_reviewTitleTxt);
            this.reviewDescriptionTxt = itemView.findViewById(R.id.irl_reviewDescriptionTxt);

            this.qualityRatingBar = itemView.findViewById(R.id.irl_qualityRatingBar);
            this.qualityRatingNumberTxt = itemView.findViewById(R.id.irl_qualityRatingNumberTxt);

            this.overallRatingBar = itemView.findViewById(R.id.irl_overallRatingBar);
            this.overallRatingNumberTxt = itemView.findViewById(R.id.irl_overallRatingNumberTxt);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}