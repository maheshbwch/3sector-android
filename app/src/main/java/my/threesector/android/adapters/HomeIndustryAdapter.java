package my.threesector.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.interfaces.OnItemClicked;
import my.threesector.android.models.CategoriesHome;
import my.threesector.android.utils.MyUtils;

public class HomeIndustryAdapter extends RecyclerView.Adapter<HomeIndustryAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CategoriesHome> arrayList;
    private OnItemClicked onItemClicked = null;


    public HomeIndustryAdapter(Context context, ArrayList<CategoriesHome> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setOnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_home_industry, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String categoryName = arrayList.get(position).getName();
        String urlType = arrayList.get(position).getType();
        String iconUrl = arrayList.get(position).getUrl();

        holder.categoryName.setText(MyUtils.checkStringValue(categoryName) ? categoryName : "");

        //if (MyUtils.checkStringValue(urlType) && urlType.equalsIgnoreCase("image")) {
        Glide.with(context).load(iconUrl).placeholder(R.mipmap.app_logo).into(holder.categoryIcon);
        //}

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView categoryName;
        public ImageView categoryIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            this.categoryName = itemView.findViewById(R.id.ihi_categoryTxt);
            this.categoryIcon = itemView.findViewById(R.id.ihi_iconImage);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClicked != null) {
                        onItemClicked.onItemClicked(getAdapterPosition());
                    }
                }
            });
        }


    }
}