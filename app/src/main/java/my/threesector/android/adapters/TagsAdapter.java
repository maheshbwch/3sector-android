package my.threesector.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.interfaces.OnItemClicked;
import my.threesector.android.models.SearchFieldListing;
import my.threesector.android.utils.MyUtils;

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SearchFieldListing> arrayList;
    private OnItemClicked onItemClicked = null;


    public TagsAdapter(Context context, ArrayList<SearchFieldListing> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setOnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_search_tags, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String name = arrayList.get(position).getName();
        holder.tagTxt.setText(MyUtils.checkStringValue(name) ? name : "");

        MyUtils.viewHtmlTxt(name, holder.tagTxt);

        boolean isSelected = arrayList.get(position).isSelected();
        holder.itemView.setSelected(isSelected);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tagTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tagTxt = itemView.findViewById(R.id.ist_tagTxt);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClicked != null) {
                        int index = getAdapterPosition();
                        selectItem(index);
                        onItemClicked.onItemClicked(index);
                    }
                }
            });
        }

        private void selectItem(int position) {
            for (int i = 0; i < arrayList.size(); i++) {
                arrayList.get(i).setSelected(position == i);
            }
            notifyDataSetChanged();
        }

    }
}