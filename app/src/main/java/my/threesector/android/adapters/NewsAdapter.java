package my.threesector.android.adapters;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.models.News;
import my.threesector.android.utils.MyUtils;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<News> arrayList;


    public NewsAdapter(Context context, ArrayList<News> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_news, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String authorImage = arrayList.get(position).getAuthorImage();
        String name = arrayList.get(position).getAuthorName();
        String newsTitle = arrayList.get(position).getNewsTitle();
        String imageUrl = arrayList.get(position).getNewsImageUrl();
        String date = arrayList.get(position).getPostDate();

        Glide.with(context).load(authorImage).placeholder(R.drawable.user_pic).into(holder.authorProfileImage);
        holder.authorNameTxt.setText(MyUtils.checkStringValue(name) ? name : "-");

        if (MyUtils.checkStringValue(newsTitle)) {
            holder.postTitleTxt.setVisibility(View.VISIBLE);

            MyUtils.viewHtmlTxt(newsTitle,holder.postTitleTxt);

        }else {
            holder.postTitleTxt.setVisibility(View.GONE);
        }

        if (MyUtils.checkStringValue(imageUrl)) {
            holder.postImage.setVisibility(View.VISIBLE);
            Glide.with(context).load(imageUrl).into(holder.postImage);
        }else {
            holder.postImage.setVisibility(View.GONE);
        }

        holder.postDateTxt.setText(MyUtils.checkStringValue(date) ? date : "-");


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public CircleImageView authorProfileImage;
        public ImageView postImage;
        public TextView authorNameTxt, postTitleTxt, postDateTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            this.authorProfileImage = itemView.findViewById(R.id.in_authorProfileImage);
            this.authorNameTxt = itemView.findViewById(R.id.in_authorNameTxt);
            this.postTitleTxt = itemView.findViewById(R.id.in_postTitleTxt);
            this.postImage = itemView.findViewById(R.id.in_postImage);
            this.postDateTxt = itemView.findViewById(R.id.in_postDateTxt);
        }
    }
}