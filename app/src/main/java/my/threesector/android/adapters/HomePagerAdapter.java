package my.threesector.android.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import my.threesector.android.fragments.home.ChatFragment;
import my.threesector.android.fragments.home.HomeFragment;
import my.threesector.android.fragments.home.MyProfileFragment;
import my.threesector.android.fragments.home.ProjectFragment;
import my.threesector.android.interfaces.CallListener;
import my.threesector.android.interfaces.HomeListener;

public class HomePagerAdapter extends FragmentPagerAdapter {

    private HomeListener homeListener;
    private CallListener callListener;

    public HomePagerAdapter(FragmentManager fm, HomeListener homeListener, CallListener callListener) {
        super(fm);
        this.homeListener = homeListener;
        this.callListener = callListener;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HomeFragment(homeListener,callListener); //ChildFragment1 at position 0
            case 1:
                return new ChatFragment(homeListener); //ChildFragment2 at position 1
           /*case 2:
                return new ContactsFragment(homeListener);*/ //ChildFragment3 at position 3
            case 2:
                return new ProjectFragment(homeListener); //ChildFragment3 at position 2
            case 3:
                return new MyProfileFragment(homeListener); //ChildFragment4 at position 4
        }
        return null; //does not happen
    }


    @Override
    public int getCount() {
        return 4; //four fragments
    }
}
