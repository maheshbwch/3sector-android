package my.threesector.android.adapters;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.interfaces.OnItemClicked;
import my.threesector.android.interfaces.SectionClickListener;
import my.threesector.android.models.CategoriesHome;
import my.threesector.android.models.CategoryListing;
import my.threesector.android.utils.Constants;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.ParseListing;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;
import my.threesector.android.webservice.ApiInterface;
import my.threesector.android.webservice.HttpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SectionHeadAdapter extends RecyclerView.Adapter<SectionHeadAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CategoriesHome> arrayList;
    private OnItemClicked onItemClicked = null;
    private SectionClickListener sectionClickListener = null;
    private static final int SECTION_LIMIT = 5;


    public SectionHeadAdapter(Context context, ArrayList<CategoriesHome> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setOnItemClicked(OnItemClicked onItemClicked) {
        this.onItemClicked = onItemClicked;
    }

    public void setSectionClickListener(SectionClickListener sectionClickListener) {
        this.sectionClickListener = sectionClickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_section_header, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String categoryId = arrayList.get(position).getTermId();
        String sectionTitle = arrayList.get(position).getSectionTitle();
        holder.headerTxt.setText(MyUtils.checkStringValue(sectionTitle) ? sectionTitle : "");

        holder.getCategoryListing(categoryId);
    }

    @Override
    public int getItemCount() {
        int size = arrayList.size();
        return (size > SECTION_LIMIT) ? SECTION_LIMIT : size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView headerTxt;
        public RecyclerView listingRecyclerView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.headerTxt = itemView.findViewById(R.id.ish_headerTxt);
            this.listingRecyclerView = itemView.findViewById(R.id.ish_listingRecyclerView);

            listingRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClicked != null) {
                        onItemClicked.onItemClicked(getAdapterPosition());
                    }
                }
            });
        }

        private void getCategoryListing(String termId) {
            try {
                ApiInterface apiService = HttpRequest.getScalarsInstance().create(ApiInterface.class);
                Call<String> call = apiService.getCategoryListing(termId);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String jsonResponse = response.body();
                        Log.e("jsonResponse", ":" + jsonResponse);
                        if (jsonResponse != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(jsonResponse);

                                String status = jsonObject.optString("status");

                                if (MyUtils.checkStringValue(status) && status.equalsIgnoreCase(Constants.API_SUCCESS)) {

                                    JSONArray jsonArray = jsonObject.optJSONArray("oResults");
                                    ArrayList<CategoryListing> listingArrayList = new ArrayList<>();
                                    listingArrayList.addAll(ParseListing.getCategoryListingArrayList(jsonArray));

                                    adaptCategoriesListings(listingArrayList);

                                } else {
                                    Log.e("error", "error response");
                                    hideItemView();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideItemView();
                            }
                        } else {
                            Log.e("error", "null response");
                            hideItemView();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.e("error", "message:" + t.getMessage());
                        hideItemView();
                    }
                });
            } catch (Exception e) {
                Log.e("exp", ":" + e.getMessage());
                hideItemView();
            }
        }


        private void adaptCategoriesListings(ArrayList<CategoryListing> listingArrayList) {
            if (listingArrayList.size() > 0) {
                SectionItemAdapter sectionItemAdapter = new SectionItemAdapter(context, listingArrayList);
                sectionItemAdapter.setListingClickListeners(new ListingClickListeners() {
                    @Override
                    public void onItemClicked(int position) {

                        String listingId = "" + listingArrayList.get(position).getListingId();
                        String listingTitle = "" + listingArrayList.get(position).getPostTitle();
                        String logo = "" + listingArrayList.get(position).getLogo();
                        String coverImage = "" + listingArrayList.get(position).getCoverImg();

                        Bundle bundle = new Bundle();
                        bundle.putString("listingId", listingId);
                        bundle.putString("listingTitle", listingTitle);
                        bundle.putString("logo", logo);
                        bundle.putString("coverImage", coverImage);

                        if (sectionClickListener != null) {
                            sectionClickListener.onItemClicked(bundle);
                        }
                    }

                    @Override
                    public void onFavouritesClicked(int position, SparkButton wishListButton) {
                        String postId = "" + listingArrayList.get(position).getListingId();
                        if (sectionClickListener != null) {
                            sectionClickListener.onFavouritesClicked(postId,wishListButton);
                        }

                    }

                    @Override
                    public void onCallClicked(String phoneNumber) {
                        if (sectionClickListener != null) {
                            sectionClickListener.onCallClicked(phoneNumber);
                        }
                    }
                });

                listingRecyclerView.setAdapter(sectionItemAdapter);
            } else {
                hideItemView();
            }
        }


        private void hideItemView() {
            if (itemView.getVisibility() == View.VISIBLE) {
                itemView.setVisibility(View.GONE);
            }
        }
    }


}