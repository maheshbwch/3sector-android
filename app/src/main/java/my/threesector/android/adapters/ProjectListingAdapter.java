package my.threesector.android.adapters;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.interfaces.OnItemClicked;
import my.threesector.android.models.ProjectListing;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;

public class ProjectListingAdapter extends RecyclerView.Adapter<ProjectListingAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ProjectListing> arrayList;
    private ListingClickListeners listingClickListeners = null;


    public ProjectListingAdapter(Context context, ArrayList<ProjectListing> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setListingClickListeners(ListingClickListeners listingClickListeners) {
        this.listingClickListeners = listingClickListeners;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_project_listing, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String coverImg = arrayList.get(position).getCoverImg();
        String profileUrl = arrayList.get(position).getLogo();
        String categoryListingName = arrayList.get(position).getPostTitle();

        String categoryName = arrayList.get(position).getCategoryName();
        String tagLine = arrayList.get(position).getTagLine();
        String iconUrl = arrayList.get(position).getIconUrl();
        //boolean isMyFavorite = arrayList.get(position).getOFavorite().getIsMyFavorite();


        MyUtils.viewHtmlTxt(categoryListingName,holder.listingNameTxt);

        holder.tagLineTxt.setText(MyUtils.checkStringValue(tagLine) ? tagLine : "-");
        holder.categoryNameTxt.setText(MyUtils.checkStringValue(categoryName) ? categoryName : "-");

        if (MyUtils.checkStringValue(coverImg)) {
            Glide.with(context).load(coverImg).into(holder.coverImageView);
        }

        Glide.with(context).load(profileUrl).placeholder(R.drawable.user_pic).into(holder.profileImageView);
        Glide.with(context).load(iconUrl).placeholder(R.mipmap.app_logo).into(holder.categoryImageView);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView coverImageView;
        public TextView listingNameTxt, tagLineTxt, categoryNameTxt;
        public CircleImageView profileImageView, categoryImageView;
        public SparkButton wishListButton;

        public ViewHolder(View itemView) {
            super(itemView);
            this.coverImageView = itemView.findViewById(R.id.ipl_coverImageView);
            this.profileImageView = itemView.findViewById(R.id.ipl_listingProfileImage);
            this.listingNameTxt = itemView.findViewById(R.id.ipl_listingNameTxt);
            this.tagLineTxt = itemView.findViewById(R.id.ipl_listingTagTxt);

            this.categoryImageView = itemView.findViewById(R.id.ipl_categoryIconImage);
            this.categoryNameTxt = itemView.findViewById(R.id.ipl_categoryNameTxt);
            this.wishListButton = itemView.findViewById(R.id.ipl_wishListButton);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listingClickListeners != null) {
                        listingClickListeners.onItemClicked(getAdapterPosition());
                    }
                }
            });

            wishListButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listingClickListeners != null) {
                        listingClickListeners.onFavouritesClicked(getAdapterPosition(), wishListButton);
                    }
                }
            });
        }


    }
}