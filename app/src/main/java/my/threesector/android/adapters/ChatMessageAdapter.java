package my.threesector.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.models.ChatMessages;
import my.threesector.android.models.ConvMessages;
import my.threesector.android.utils.MyUtils;


public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.MessageViewHolder> {

    private Context context;
    private ArrayList<ConvMessages> arrayList = new ArrayList<>();

    public ChatMessageAdapter(Context context, ArrayList<ConvMessages> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_message, parent, false);
        return new MessageViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final MessageViewHolder holder, int position) {

        ConvMessages convMessages = arrayList.get(position);
        boolean isMessageOwner = convMessages.isMessageOwner();
        ChatMessages chatMessages = convMessages.getChatMessages();

        String senderImage = convMessages.getSenderImage();
        String receiverImage = convMessages.getReceiverImage();
        String messageText = chatMessages.getMessage_text();
        String timeStampDate = chatMessages.getMessage_date();

        String formattedDate = MyUtils.checkStringValue(timeStampDate) ? MyUtils.getDateFromTimeStamp(Long.parseLong(timeStampDate)) : "";


        if (isMessageOwner) {
            holder.senderChatLayout.setVisibility(View.VISIBLE);

            Glide.with(context).load(senderImage).placeholder(R.drawable.user_pic).into(holder.senderProfileImageView);

            holder.senderMessageTxt.setVisibility(View.VISIBLE);
            holder.senderMessageTxt.setText(MyUtils.checkStringValue(messageText) ? messageText : "");
            holder.senderAttachImage.setVisibility(View.GONE);

            holder.senderMsgTimeTxt.setText(formattedDate);
            holder.receiverChatLayout.setVisibility(View.GONE);
        } else {
            holder.receiverChatLayout.setVisibility(View.VISIBLE);

            Glide.with(context).load(receiverImage).placeholder(R.drawable.user_pic).into(holder.receiverProfileImageView);

            holder.receiverMessageTxt.setVisibility(View.VISIBLE);
            holder.receiverMessageTxt.setText(MyUtils.checkStringValue(messageText) ? messageText : "");
            holder.receiverAttachImage.setVisibility(View.GONE);

            holder.receiverMsgTimeTxt.setText(formattedDate);
            holder.senderChatLayout.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout receiverChatLayout, senderChatLayout;
        public CircleImageView receiverProfileImageView, senderProfileImageView;
        public TextView receiverMessageTxt, receiverMsgTimeTxt, senderMessageTxt, senderMsgTimeTxt;
        public ImageView receiverAttachImage, senderAttachImage;


        public MessageViewHolder(View itemView) {
            super(itemView);

            receiverChatLayout = itemView.findViewById(R.id.icm_receiverChatLayout);
            receiverProfileImageView = itemView.findViewById(R.id.icm_receiverProfileImageView);
            receiverMessageTxt = itemView.findViewById(R.id.icm_receiverMessageTxt);
            receiverAttachImage = itemView.findViewById(R.id.icm_receiverAttachImage);
            receiverMsgTimeTxt = itemView.findViewById(R.id.icm_receiverMsgTimeTxt);

            senderChatLayout = itemView.findViewById(R.id.icm_senderChatLayout);
            senderProfileImageView = itemView.findViewById(R.id.icm_senderProfileImageView);
            senderMessageTxt = itemView.findViewById(R.id.icm_senderMessageTxt);
            senderAttachImage = itemView.findViewById(R.id.icm_senderAttachImage);
            senderMsgTimeTxt = itemView.findViewById(R.id.icm_senderMsgTimeTxt);

        }

    }
}