package my.threesector.android.adapters;


import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import my.threesector.android.R;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.models.CategoryListing;
import my.threesector.android.utils.MyUtils;
import my.threesector.android.utils.custom_view.spark_button.SparkButton;

public class SectionItemAdapter extends RecyclerView.Adapter<SectionItemAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CategoryListing> arrayList;
    private ListingClickListeners listingClickListeners = null;
    private static final int SECTION_LIMIT = 5;


    public SectionItemAdapter(Context context, ArrayList<CategoryListing> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setListingClickListeners(ListingClickListeners listingClickListeners) {
        this.listingClickListeners = listingClickListeners;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_home_section, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String coverImg = arrayList.get(position).getCoverImg();
        String categoryListingName = arrayList.get(position).getPostTitle();

        String categoryName = arrayList.get(position).getCategoryName();
        String tagLine = arrayList.get(position).getTagLine();
        String phoneNumber = arrayList.get(position).getPhoneNumber();
        String iconUrl = arrayList.get(position).getIconUrl();
        String businessStatus = arrayList.get(position).getBusinessStatus();
        //boolean isMyFavorite = arrayList.get(position).getOFavorite().getIsMyFavorite();


        MyUtils.viewHtmlTxt(categoryListingName,holder.listingNameTxt);

        holder.tagLineTxt.setText(MyUtils.checkStringValue(tagLine) ? tagLine : "-");
        holder.phoneNumberTxt.setText(MyUtils.checkStringValue(phoneNumber) ? phoneNumber : "-");

        holder.categoryNameTxt.setText(MyUtils.checkStringValue(categoryName) ? categoryName : "-");

        if (MyUtils.checkStringValue(businessStatus)) {
            holder.businessStatusTxt.setVisibility(View.VISIBLE);
            holder.businessStatusTxt.setText(businessStatus);
        } else {
            holder.businessStatusTxt.setVisibility(View.GONE);
        }

        if (MyUtils.checkStringValue(coverImg)) {
            Glide.with(context).load(coverImg).into(holder.coverImageView);
        }

        Glide.with(context).load(iconUrl).placeholder(R.mipmap.app_logo).into(holder.categoryImageView);

    }

    @Override
    public int getItemCount() {
        //int size = arrayList.size();
        //return (size > SECTION_LIMIT) ? SECTION_LIMIT : size;
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView coverImageView;
        public TextView listingNameTxt, tagLineTxt, phoneNumberTxt, categoryNameTxt, businessStatusTxt;
        public LinearLayout phoneLinear;
        public CircleImageView categoryImageView;
        public SparkButton wishListButton;

        public ViewHolder(View itemView) {
            super(itemView);
            this.coverImageView = itemView.findViewById(R.id.ihs_coverImageView);
            this.listingNameTxt = itemView.findViewById(R.id.ihs_listingNameTxt);
            this.tagLineTxt = itemView.findViewById(R.id.ihs_listingTagTxt);

            this.phoneLinear = itemView.findViewById(R.id.ihs_phoneLinear);
            this.phoneNumberTxt = itemView.findViewById(R.id.ihs_phoneNumberTxt);

            this.categoryImageView = itemView.findViewById(R.id.ihs_categoryIconImage);
            this.categoryNameTxt = itemView.findViewById(R.id.ihs_categoryNameTxt);
            this.businessStatusTxt = itemView.findViewById(R.id.ihs_businessStatusTxt);
            this.wishListButton = itemView.findViewById(R.id.ihs_wishListButton);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listingClickListeners != null) {
                        listingClickListeners.onItemClicked(getAdapterPosition());
                    }
                }
            });

            wishListButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listingClickListeners != null) {
                        listingClickListeners.onFavouritesClicked(getAdapterPosition(), wishListButton);
                    }
                }
            });

            phoneLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String phoneNumber = arrayList.get(getAdapterPosition()).getPhoneNumber();
                    if (MyUtils.checkStringValue(phoneNumber)) {
                        listingClickListeners.onCallClicked(phoneNumber);
                    }
                }
            });
        }
    }
}