package my.threesector.android.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

import my.threesector.android.R;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private ArrayList<String> arrayList;


    public RecyclerAdapter(ArrayList<String> arrayList) {
        this.arrayList = arrayList;
    }


    /*public void setOnRemoveClicked(MedicationNameListener medicationNameListener) {
        this.medicationNameListener = medicationNameListener;
    }*/


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        //String name = arrayList.get(position);
        //holder.medicationNameTxt.setText(""+(position+1)+"." + name);

    }

    @Override
    public int getItemCount() {
        //return arrayList.size();
        return 8;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView medicationNameTxt, removeTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            //this.medicationNameTxt = itemView.findViewById(R.id.imn_medicationNameTxt);
            //this.removeTxt = itemView.findViewById(R.id.imn_removeTxt);


        }
    }
}