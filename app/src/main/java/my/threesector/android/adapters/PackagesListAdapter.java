package my.threesector.android.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.models.PackageModel;
import my.threesector.android.utils.MyUtils;

public class PackagesListAdapter extends RecyclerView.Adapter<PackagesListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<PackageModel> arrayList;

    public PackagesListAdapter(Context context, ArrayList<PackageModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_package, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String serialNumber = arrayList.get(position).getPackageSerialNumber();
        String name = arrayList.get(position).getPackageName();
        boolean isSelected = arrayList.get(position).isSelected();

        holder.packageNumberTxt.setText((MyUtils.checkStringValue(serialNumber)) ? serialNumber : "-");
        holder.packageNameTxt.setText((MyUtils.checkStringValue(name)) ? name : "-");
        holder.selectedRadio.setChecked(isSelected);
        holder.itemView.setSelected(isSelected);

        if (isSelected) {
            holder.packageNameTxt.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.packageNameTxt.setTextColor(context.getResources().getColor(R.color.gray_light));
        }


        Log.e("pos:", ":" + position);
        Log.e("isSelected:", ":" + isSelected);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public TextView packageNumberTxt, packageNameTxt;
        public RadioButton selectedRadio;

        public ViewHolder(View itemView) {
            super(itemView);
            this.packageNumberTxt = itemView.findViewById(R.id.ip_packageNumberTxt);
            this.packageNameTxt = itemView.findViewById(R.id.ip_packageNameTxt);
            this.selectedRadio = itemView.findViewById(R.id.ip_selectedRadio);


           /* selectedRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checkRadioButton(getAdapterPosition());
                }
            });*/

            selectedRadio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkRadioButton(getAdapterPosition());
                }
            });
        }

        private void checkRadioButton(int position) {
            for (int i = 0; i < arrayList.size(); i++) {
                arrayList.get(i).setSelected(position == i);
            }
            notifyDataSetChanged();
        }

    }
}
