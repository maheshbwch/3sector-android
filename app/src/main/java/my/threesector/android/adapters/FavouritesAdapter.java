package my.threesector.android.adapters;


import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;

import java.util.ArrayList;

import my.threesector.android.R;
import my.threesector.android.interfaces.FavouritesInterface;
import my.threesector.android.interfaces.ListingClickListeners;
import my.threesector.android.interfaces.OnItemClicked;
import my.threesector.android.models.Favourites;
import my.threesector.android.utils.MyUtils;

public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Favourites> arrayList;
    private ListingClickListeners listingClickListeners = null;

    private OnItemClicked onItemClicked = null;
    private FavouritesInterface favouritesInterface = null;



    public FavouritesAdapter(Context context, ArrayList<Favourites> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setListingClickListeners(ListingClickListeners listingClickListeners) {
        this.listingClickListeners = listingClickListeners;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.item_favourites, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String listingName = arrayList.get(position).getPostTitle();
        String tagLine = arrayList.get(position).getTagLine();
        String iconUrl = arrayList.get(position).getIconUrl();

        MyUtils.viewHtmlTxt(listingName,holder.listingNameTxt);

        holder.tagLineTxt.setText(MyUtils.checkStringValue(tagLine) ? tagLine : "-");

        Glide.with(context).load(iconUrl).placeholder(R.mipmap.app_logo).into(holder.profileImageView);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView listingNameTxt, tagLineTxt;
        public PorterShapeImageView profileImageView;
        private LinearLayout removeLinear;

        public ViewHolder(View itemView) {
            super(itemView);
            this.profileImageView = itemView.findViewById(R.id.if_listingProfileImage);
            this.listingNameTxt = itemView.findViewById(R.id.if_listingNameTxt);
            this.tagLineTxt = itemView.findViewById(R.id.if_listingTagTxt);
            this.removeLinear = itemView.findViewById(R.id.if_removeLinear);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listingClickListeners != null) {
                        listingClickListeners.onItemClicked(getAdapterPosition());
                    }
                }
            });

            removeLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listingClickListeners != null) {
                        listingClickListeners.onFavouritesClicked(getAdapterPosition(), null);
                    }
                }
            });

        }


    }
}