package my.threesector.android.interfaces;

public interface FavouritesListener {
    void onFavouritesSuccess(String status);
    void onFavouritesFailed(String errorType,String error);
}
