package my.threesector.android.interfaces;

public interface ConnectivityInterface {
    void onConnected();
    void onNotConnected();
}
