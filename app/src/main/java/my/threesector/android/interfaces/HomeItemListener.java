package my.threesector.android.interfaces;

public interface HomeItemListener {
    void onChatClicked();
    void onBookNowClicked();
}
