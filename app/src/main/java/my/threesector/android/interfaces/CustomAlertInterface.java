package my.threesector.android.interfaces;

public interface CustomAlertInterface {

    void onPositiveButtonClicked();

    void onNegativeButtonClicked();

}
