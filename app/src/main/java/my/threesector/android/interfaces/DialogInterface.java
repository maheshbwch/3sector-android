package my.threesector.android.interfaces;

public interface DialogInterface {
    void onButtonClicked();
    void onDismissedClicked();
}
