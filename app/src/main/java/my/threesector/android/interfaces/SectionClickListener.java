package my.threesector.android.interfaces;

import android.os.Bundle;

import my.threesector.android.utils.custom_view.spark_button.SparkButton;

public interface SectionClickListener {
    void onItemClicked(Bundle bundle);
    void onFavouritesClicked(String postId, SparkButton wishListButton);
    void onCallClicked(String phoneNumber);
}
