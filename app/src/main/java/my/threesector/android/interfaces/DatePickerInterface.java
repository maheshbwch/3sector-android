package my.threesector.android.interfaces;

public interface DatePickerInterface {

    void onDateSelected(int day, int month, int year);
    void onDialogDismiss();

}
