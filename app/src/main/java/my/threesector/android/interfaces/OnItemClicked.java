package my.threesector.android.interfaces;

public interface OnItemClicked {
    void onItemClicked(int position);
}
