package my.threesector.android.interfaces;

public interface HomeListener {
    void onApiCallSuccess();
    void onApiCallFailed(String errorType,String error);
}
