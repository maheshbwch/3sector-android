package my.threesector.android.interfaces;

public interface CountDownInterface {
    void onTicking(long millisUntilFinished);
    void onFinished();

}
