package my.threesector.android.interfaces;

public interface ChatUserListener {
    void onUserCreated();
    void onFailed();
}
