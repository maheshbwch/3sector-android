package my.threesector.android.interfaces;

import my.threesector.android.utils.custom_view.spark_button.SparkButton;

public interface ListingClickListeners {
    void onItemClicked(int position);
    void onFavouritesClicked(int position,SparkButton wishListButton);
    void onCallClicked(String phoneNumber);
}
