package my.threesector.android.interfaces;

public interface UploadAlertListener {
    void onChooseFileClicked();
    void onCaptureCameraClicked();
}
