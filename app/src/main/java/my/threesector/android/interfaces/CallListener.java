package my.threesector.android.interfaces;

public interface CallListener {
    void onCallClicked(String phoneNumber);
}
