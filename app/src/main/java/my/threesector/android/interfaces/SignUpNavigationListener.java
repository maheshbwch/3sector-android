package my.threesector.android.interfaces;

public interface SignUpNavigationListener {
    void onBackButtonClicked();
    void onNextButtonClicked();
}
