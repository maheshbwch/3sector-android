package my.threesector.android.interfaces;

public interface SignUpModeListener {
    void onMerchantRegistrationClicked();
    void onFreeSignUpClicked();
    void onBackButtonClicked();
    void onNextButtonClicked(boolean isFreeSignUpProcess);
}
