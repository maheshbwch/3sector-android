package my.threesector.android.webservice;

public class ApiConstants {

    public static final String BASE_URL = "https://bestweb.my/3sectornew/";
    public static final String AUTO_LOGIN_WEB_VIEW_URL = BASE_URL+"login/";
    public static final String QUESTIONS_WEB_VIEW_URL = BASE_URL+"questions/";
    public static final String ADD_LISTING_WEB_VIEW_URL = BASE_URL+"add-listing-plan/";
    public static final String BECOME_MERCHANT_WEB_VIEW_URL = BASE_URL+"become-a-merchant/";



    public static final String API_BASE_URL = BASE_URL + "wp-json/wiloke/v2/";
    public static final String LOGIN = API_BASE_URL + "auth";
    public static final String SIGN_UP = API_BASE_URL + "signup";
    public static final String CATEGORY_LISTING_HOME = API_BASE_URL + "taxonomies/listing-categories";
    public static final String CATEGORY_LISTING = API_BASE_URL + "listing-category";
    public static final String CATEGORY_LISTING_DETAILS = API_BASE_URL + "listing-detail";
    public static final String BECOME_A_AUTHOR = API_BASE_URL + "bw_become_author";
    public static final String SEARCH_FIELD_LISTING = API_BASE_URL + "search-fields/listing";
    public static final String SEARCH_BY_TAGS = API_BASE_URL + "listing_tag";
    public static final String SEARCH_BY_REGION = API_BASE_URL + "listing-location";
    public static final String NEWS_LIST = API_BASE_URL + "posts";
    public static final String PROJECT_LISTING = API_BASE_URL + "get-my-listings";
    public static final String GET_PROFILE_DETAILS = API_BASE_URL + "get-profile";
    public static final String GET_MY_FAVOURITES = API_BASE_URL + "get-my-favorites";
    public static final String ADD_TO_FAVORITES = API_BASE_URL + "add-to-my-favorites";
    public static final String REMOVE_FAVORITES = API_BASE_URL + "remove-from-my-favorites";
    public static final String UPDATE_PASSWORD = API_BASE_URL + "update-password";
    public static final String GET_LISTING_REVIEW = API_BASE_URL + "posts";
    public static final String POST_REVIEW = API_BASE_URL + "post-review";
    public static final String GET_NEAR_BY_ME = API_BASE_URL + "nearbyme";

    public static final String GET_CHAT_USERS = API_BASE_URL + "get-authors-chatted";
    public static final String GET_CHAT_MESSAGES = API_BASE_URL + "get-author-messages";
    public static final String SEND_CHAT_MESSAGES = API_BASE_URL + "send-message";


}
