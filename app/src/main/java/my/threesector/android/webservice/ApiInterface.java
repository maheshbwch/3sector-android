package my.threesector.android.webservice;


import java.util.Map;

import my.threesector.android.models.Login;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @Multipart
    @POST(ApiConstants.LOGIN)
    Call<Login> loginUser(@PartMap() Map<String, RequestBody> partMap);

    @Multipart
    @POST(ApiConstants.SIGN_UP)
    Call<Login> registerUser(@PartMap() Map<String, RequestBody> partMap);

    @GET(ApiConstants.CATEGORY_LISTING_HOME)
    Call<String> getCategories();

    @GET(ApiConstants.CATEGORY_LISTING + "/{input}")
    Call<String> getCategoryListing(@Path("input") String input);

    @GET(ApiConstants.CATEGORY_LISTING_DETAILS + "/{input}")
    Call<String> getCategoryListingDetails(@Path("input") String input);

    @POST(ApiConstants.BECOME_A_AUTHOR)
    Call<String> becomeAnAuthor(@Header("Authorization") String authHeader);

    @GET(ApiConstants.SEARCH_FIELD_LISTING)
    Call<String> getSearchFieldListing();

    @GET(ApiConstants.SEARCH_BY_TAGS + "/{input}")
    Call<String> getTagsListing(@Path("input") String input);

    @GET(ApiConstants.SEARCH_BY_REGION + "/{input}")
    Call<String> getRegionListing(@Path("input") String input);

    @GET(ApiConstants.NEWS_LIST)
    Call<String> getNewsListing();

    @GET(ApiConstants.PROJECT_LISTING)
    Call<String> getProjectListing(@Query("postType") String postType, @Query("page") String page, @Header("Authorization") String authHeader);

    @GET(ApiConstants.PROJECT_LISTING)
    Call<String> getMyListing(@Query("postType") String postType, @Query("page") String page, @Header("Authorization") String authHeader);

    @GET(ApiConstants.GET_PROFILE_DETAILS)
    Call<String> getProfileDetails(@Header("Authorization") String authHeader);

    @GET(ApiConstants.GET_MY_FAVOURITES)
    Call<String> getMyFavourites(@Query("page") String page, @Header("Authorization") String authHeader);

    @Multipart
    @POST(ApiConstants.UPDATE_PASSWORD)
    Call<String> updatePassword(@PartMap() Map<String, RequestBody> partMap, @Header("Authorization") String authHeader);

    @Multipart
    @POST(ApiConstants.ADD_TO_FAVORITES)
    Call<String> addToFavorites(@PartMap() Map<String, RequestBody> partMap, @Header("Authorization") String authHeader);

    @Multipart
    @POST(ApiConstants.REMOVE_FAVORITES)
    Call<String> removeFromFavorites(@PartMap() Map<String, RequestBody> partMap, @Header("Authorization") String authHeader);

    @GET(ApiConstants.GET_LISTING_REVIEW + "/{input}/" + "reviews")
    Call<String> getListingReview(@Path("input") String input);

    @Multipart
    @POST(ApiConstants.POST_REVIEW)
    Call<String> addReview(@PartMap() Map<String, RequestBody> partMap, @Header("Authorization") String authHeader);

    @GET(ApiConstants.GET_NEAR_BY_ME)
    Call<String> getNearByMe(@Query("lat") String lat, @Query("lng") String lng, @Query("radius") String radius, @Query("unit") String unit);

    @GET(ApiConstants.GET_CHAT_USERS)
    Call<String> getChatUsers(@Header("Authorization") String authHeader);

    @GET(ApiConstants.GET_CHAT_MESSAGES)
    Call<String> getChatMessages(@Query("chatFriendID") String chatUserId, @Header("Authorization") String authHeader);

    @Multipart
    @POST(ApiConstants.SEND_CHAT_MESSAGES)
    Call<String> sendChatMessages(@PartMap() Map<String, RequestBody> partMap, @Header("Authorization") String authHeader);

}
