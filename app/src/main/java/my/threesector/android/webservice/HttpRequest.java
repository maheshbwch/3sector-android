package my.threesector.android.webservice;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class HttpRequest {

    public static String BASE_URL = ApiConstants.API_BASE_URL;
    private static Retrofit retrofit = null;
    private static Retrofit retrofitScalars = null;
    private static Retrofit retrofitAccessToken = null;

    public static Retrofit getInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHttpClient())
                    .build();
        }
        return retrofit;
    }





    public static Retrofit getScalarsInstance() {
        if (retrofitScalars == null) {
            retrofitScalars = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return retrofitScalars;
    }


    private static OkHttpClient getHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build();
    }


    /*public static Retrofit getTokenInstance() {
        if (retrofitAccessToken == null) {
            retrofitAccessToken = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getHttpClientWithInterceptor())
                    .build();
        }
        return retrofitAccessToken;
    }*/


   /* private static OkHttpClient getHttpClientWithInterceptor() {
        return new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest  = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + "")
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build();
    }*/



    /**/

}